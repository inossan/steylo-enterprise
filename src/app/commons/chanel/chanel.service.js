(function () {
    'use strict';

    angular.module('app.commons')
        .factory('ChanelService', ChanelService);

    ChanelService.$inject = ['$http'];
    function ChanelService($http) {
        const chanelService = {};
        chanelService.createChanel = createChanel;
        chanelService.updateChanel = updateChanel;
        chanelService.deleteChanel = deleteChanel;

        return chanelService;

        function createChanel(chanel) {
            let request = {
                datasChanel: [chanel]
            };
            console.log('ok');
            let URL = BASE_URL + '/chanel/create';
            return $http.post(URL, request, {cache: true});
        }

        function updateChanel() {
            //todo
        }

        function deleteChanel() {
            //todo
        }
    }
})();