(function () {
    'use strict';

    angular.module('app.commons')
        .factory('$sweetAlert', modalService);

    modalService.$inject = ['toastr'];
    function modalService (toastr) {
        let modalService = {};

        modalService.supprimer = supprimer;
        modalService.sauvegarder = sauvegarder;

        return modalService;

        function supprimer(fonction, data) {
            return new Promise(function (resolve, reject) {
                swal({
                    title: 'Voulez-vous vraiment supprimer cette donnée ?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: brandDanger,
                    confirmButtonText: 'Oui, supprimer !',
                    cancelButtonText: 'Annuler'
                }).then(function () {
                    fonction(data).then(function (res) {
                        resolve('success');
                        if (res.data.hasError === false) {
                            swal('La donnée a bien été supprimée.', '', 'success');
                        }else {
                            toastr.error(res.data.status.message);
                        }
                    }, function (err) {
                        console.log('err', err);
                        reject('error');
                    });
                }, function (erreur) {
                    reject('error');
                    console.log('erreur', erreur);
                });
            });
        }

        function sauvegarder(element, fonction) {
            swal({
                title: 'Voulez-vous vraiment enregistrer ' + element + ' ?',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: brandSuccess,
                confirmButtonText: 'Oui, Enregistrer',
                cancelButtonText: 'Non, Annuler',
                closeOnConfirm: false
            }).then((result) => {
                if (result.value) {
                    fonction();
                    // result.dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                } else if (result.dismiss === 'cancel') {

                }
            });
        }
    }
})();
