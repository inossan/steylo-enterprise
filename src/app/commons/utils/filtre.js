(function () {
    'use strict';

    angular
        .module('app.commons')
        .filter('range', range)
        .filter('search', search)
        .filter('dureeJour', dureeJour)
        .filter('heure', heure)
        .filter('age', age)
        .filter('currency', currency)
        .filter('ordinal', ordinal)
        .filter('longue_date', longue_date)
        .filter('courte_date', courte_date)
        .filter('date_relative', date_relative);

    range.$inject = [];
    function range() {
        return function(input, total) {
            total = parseInt(total);

            for (let i=0; i<total; i++) {
                input.push(i);
            }

            return input;
        };
    }

    search.$inject = [];
    function search(){
        return function(arr, searchString){
            if(!searchString){
                return arr;
            }
            let result = [];
            searchString = searchString.toLowerCase();
            _.forEach(arr, function(item){
                if(item.title.toLowerCase().indexOf(searchString) !== -1){
                    result.push(item);
                }
            });
            return result;
        };
    }

    dureeJour.$inject = [];
    function dureeJour(){
        return function(de){
            let dateEnreg = de.split(' ');
            let date = dateEnreg[0];
            return moment('01/10/2017', 'DD/MM/YYYY').diff(moment(date, 'DD/MM/YYYY'), 'days', false);
        }
    }

    heure.$inject = [];
    function heure(){
        return function(heureArrivee){
            let heurear=heureArrivee.split(' ')[1];
            let heur=heurear.split(':')[0];
            let min=heurear.split(':')[1];
            let se=heurear.split(':')[2];
            return heur +'h' +' '+ min + 'm'+ ' ' + se +'s';
        }
    }

    age.$inject = [];
    function age(){
        return function(dateNaissance){
            if(!dateNaissance)return 'nd';
            let unites= "an(s)";
            let age=moment().diff(moment(dateNaissance,'DD/MM/YYYY'),'years',false);
            if(age===0){
                age=moment().diff(moment(dateNaissance,'DD/MM/YYYY'),'months',false);
                unites="mois";
            }
            if(age===0){
                age=moment().diff(moment(dateNaissance,'DD/MM/YYYY'),'weeks',false);
                unites="semaine(s)";
            }
            if(age===0){
                age=moment().diff(moment(dateNaissance,'DD/MM/YYYY'),'days',false);
                unites="jour(s)";
            }
            return age +' ' + unites;
        }
    }

    currency.$inject = [];
    function currency(){
        // Create the return function and set the required parameter name to **input**
        // setup optional parameters for the currency symbol and location (left or right of the amount)
        return function(input, symbol, place) {
            // Ensure that we are working with a number
            if(isNaN(input)) {
                return input;
            } else {

                // Check if optional parameters are passed, if not, use the defaults
                let symbol = symbol || '€';
                let place = place === undefined ? true : place;

                // Perform the operation to set the symbol in the right location
                if( place === true) {
                    return symbol + input;
                } else {
                    return input + symbol;
                }

            }
        }
    }

    ordinal.$inject = [];
    function ordinal(){
// Create the return function
        // set the required parameter name to **number**
        return function(number) {

            // Ensure that the passed in data is a number
            if(isNaN(number) || number < 1) {

                // If the data is not a number or is less than one (thus not having a cardinal value) return it unmodified.
                return number;

            } else {

                // If the data we are applying the filter to is a number, perform the actions to check it's ordinal suffix and apply it.

                //Pour les français
                if (number === 1){
                    return number + 'er';
                }else {
                    return number + 'ième';
                }

                //Pour les anglais
                /*var lastDigit = number % 10;

                if(lastDigit === 1) {
                    return number + 'st'
                } else if(lastDigit === 2) {
                    return number + 'nd'
                } else if (lastDigit === 3) {
                    return number + 'rd'
                } else if (lastDigit > 3) {
                    return number + 'th'
                }*/

            }
        }
    }

    longue_date.$inject = [];
    function longue_date(){
        return function(de){
            let dateEnreg = de.split(' ');
            let dateASpliter = dateEnreg[0];
            let splitDate = _.split(dateASpliter, '/');
            let date = splitDate[1] + '/' + splitDate[0] + '/' + splitDate[2];
            let heure = dateEnreg[1];
            let d = moment(date, 'DD-MM-YYYY').format('DD MMM YYYY');
            return d + ' à ' + heure;
        }
    }

    courte_date.$inject = [];
    function courte_date(){
        return function(de){
            let splitDate = _.split(de, '/');
            let date = splitDate[1] + '/' + splitDate[0] + '/' + splitDate[2];
            return moment(date, 'DD-MM-YYYY').format('DD MMM YYYY');
        }
    }

    date_relative.$inject = [];
    function date_relative(){
        return function(de){
            return moment(de, "DD-MM-YYYY h:mm:ss").fromNow();
        }
    }
    /*
        .filter('currency', function(){

    });*/
})();
