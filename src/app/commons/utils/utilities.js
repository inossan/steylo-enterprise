"use strict";

angular
    .module('app.commons')
    .service("commonUtilities", ["$filter", "$rootScope", function ($filter, $rootScope) {
    var mainSelf = this;

    mainSelf.formatMillier = function (nombre) {
      nombre += "";
      var sep = " ";
      var reg = /(\d+)(\d{3})/;

      while (reg.test(nombre)) {
        nombre = nombre.replace(reg, "$1" + sep + "$2");
      }

      return nombre;
    };

    mainSelf.formatMillierRec = function (nombre) {
      nombre += "";
      var reg = new RegExp("\\ ", "g");
      nombre = nombre.trim().replace(reg, "");

      return nombre;
    };

    mainSelf.dateJsonFormat = function (date) {
      if (mainSelf.isNullOrEmptyValue(date))
      return null;

      return "\/Date(" + (mainSelf.formatUTCDate(date)) + ")\/";
    };

    mainSelf.BoucleFuncAsyncClass = function () {
      var self = this;

      var results = new Array();
      self.response = null;
      self.callback = null;
      self.searchFunc = null;
      var listeOfParameters = null;

      self.BouclerFuncAsync = function () {
        var request = listeOfParameters == null ? null : (listeOfParameters.length > 0 ? listeOfParameters.shift() : null);

        if (request != null) {
          self.searchFunc(request, self.BouclerFuncAsyncRappel);
        } else {

          self.callback(new mainSelf.ResponseSw(null, 0, 0, 0, 0, "", true));
        }
      };

      self.BouclerFuncAsyncRappel = function (response) {
        if (response.HasError) {
          console.log(response.Message);
          self.response = response;

          self.callback(self.response);
        } else {
          if (!mainSelf.isNullOrEmptyValue(response.Response)) {
            for (var i = 0; i < response.Response.length; i++) {
              var rp = response.Response[i];
              results.push(rp);
            }
          }
          if (listeOfParameters.length > 0)
          self.BouclerFuncAsync();
          else {
            self.response.Response = results;
            self.callback(self.response);
          }
        }
      };

      self.start = function (searchFunc, callback, parameters) {

        results = new Array();

        self.response = new mainSelf.ResponseSw(null, 0, 0, 0, 0, "", false);
        self.callback = callback;
        listeOfParameters = parameters;
        self.searchFunc = searchFunc;

        self.BouclerFuncAsync();
      };
    };

    mainSelf.dateJsonFormat = function (date) {
      if (mainSelf.isNullOrEmptyValue(date))
      return null;

      return "\/Date(" + (mainSelf.formatUTCDate(date)) + ")\/";
    };

    mainSelf.getNbJours = function (date) {
      return new Date(date.getFullYear(), date.getMonth() + 1, -1).getDate() + 1;
    };

    mainSelf.getAge = function (birthDate, now) {

      var days = angular.copy(now.getDate() - birthDate.getDate());

      if (days < 0) {
        var newNow = angular.copy(new Date(now.getFullYear(), now.getMonth() - 1, now.getDate()));
        if (now < newNow)
        days += angular.copy(parseInt(mainSelf.dateDiff(now, newNow).day));
        else
        days += angular.copy(parseInt(mainSelf.dateDiff(newNow, now).day));

        now = angular.copy(newNow);
      }
      var months = angular.copy(now.getMonth() - birthDate.getMonth());
      if (months < 0) {
        months += angular.copy(12);
        now = angular.copy(new Date(now.getFullYear() - 1, now.getMonth(), now.getDate()));
      }
      var years = angular.copy(now.getFullYear() - birthDate.getFullYear());
      if (years === 0) {
        if (months === 0) {
          if (days === 0) {
            const hours = parseInt(Math.abs(now - birthDate) / (1000 * 60 * 60) % 24);
            const minutes = parseInt(Math.abs(now.getTime() - birthDate.getTime()) / (1000 * 60) % 60);
            const seconds = parseInt(Math.abs(now.getTime() - birthDate.getTime()) / (1000) % 60);
            if (hours === 0) {
              return minutes + " min " + seconds + " s";
            }
            if (minutes === 0) {
              return seconds + " s";
            }
            return hours + " h " + minutes + " min ";
          } else {
            if (days === 1)
            return days + " jour";
            else
            return days + " jours";
          }
        } else {
          return months + " mois";
        }
      }
      return years + " ans";
    };

    mainSelf.toDate = function (value) {
      var dateRegExp = /^\/Date\((.*?)\)\/$/;
      var date = dateRegExp.exec(value);
      return new Date(parseInt(date[1]));
    };

    mainSelf.formatUTCDate = function (d1) {
      if (mainSelf.isNullOrEmptyValue(d1))
      return null;

      var d = new Date(d1);
      var str = Date.UTC(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), 0);
      return str;
    };

    mainSelf.currentDate = function () {
      return new Date();
    };

    mainSelf.currentDateJson = function () {
      return $filter("dateJson")(mainSelf.currentDate());
    };

    mainSelf.dateDiffFormatee = function (date1, date2) {
      var diff = mainSelf.dateDiff(date1, date2);
      var dateFormatee = null;

      if (diff.day == 0 && diff.hour == 0 && diff.min == 0) {
        dateFormatee = "A l'instant";
      } else {
        if (diff.day == 0 && diff.hour == 0) {
          dateFormatee = diff.min + " mins";
        } else {
          if (diff.day == 0) {
            dateFormatee = diff.hour + " hrs " + diff.min + " mins";
          } else {
            dateFormatee = diff.day + " jrs " + diff.hour + " hrs " + diff.min + " mins";
          }
        }
      }

      return dateFormatee;
    };

    mainSelf.dateJsonDiff = function (dateInf, dateSup) {
      var date1 = $filter("jsonDateUtc")(dateInf);
      var date2 = $filter("jsonDateUtc")(dateSup);

      var diff = mainSelf.dateDiff(date1, date2);

      return diff;
    };

    mainSelf.dateDiff = function (date1, date2) {
      var diff = {}; // Initialisation du retour
      var tmp = date2 - date1;

      tmp = Math.floor(tmp / 1000); // Nombre de secondes entre les 2 dates
      diff.sec = tmp % 60; // Extraction du nombre de secondes

      tmp = Math.floor((tmp - diff.sec) / 60); // Nombre de minutes (partie entière)
      diff.min = tmp % 60; // Extraction du nombre de minutes

      tmp = Math.floor((tmp - diff.min) / 60); // Nombre d"heures (entières)
      diff.hour = tmp % 24; // Extraction du nombre d"heures

      tmp = Math.floor((tmp - diff.hour) / 24); // Nombre de jours restants
      diff.day = tmp;

      return diff;
    };

    // entrée date (Coordinated Universal Time) - resultat en format yyyy/mm/dd
    // à utiliser pour les formats de date en html5
    mainSelf.dateToFormatYYYYMMDD = function (value) {
      if (mainSelf.isNullOrEmptyValue(value))
      return null;

      var valueInDate = (new Date(value));
      var d = valueInDate.getDate();
      var m = (valueInDate.getMonth() + 1).toString();
      if (m.length == 1)
      m = "0" + m;
      var y = valueInDate.getFullYear();
      return y + "/" + m + "/" + d;

      //return (new Date(value)).getDate();
    };

    // entrée date (Coordinated Universal Time) - resultat en format yyyy-mm-dd
    mainSelf.dateToFormatYYYYMMDD2 = function (value) {
      if (mainSelf.isNullOrEmptyValue(value))
      return null;

      var valueInDate = (new Date(value));
      var d = valueInDate.getDate();
      var m = (valueInDate.getMonth() + 1).toString();
      if (m.length == 1)
      m = "0" + m;
      var y = valueInDate.getFullYear();
      return y + "-" + m + "-" + d;
    };

    // resultat en format yyyy/mm/dd - entrée date (Coordinated Universal Time)
    // à utiliser pour les formats de date en html5
    mainSelf.formatYYYYMMDDToDate = function (date) {
      if (!mainSelf.isNullOrEmptyValue(date)) {

        return new Date(Date.parse(date));
      } else {
        return null;
      }
    };

    // resultat en format yyyy-mm-dd - entrée date (Coordinated Universal Time)
    mainSelf.formatYYYYMMDDToDate2 = function (date) {
      if (!mainSelf.isNullOrEmptyValue(date)) {
        return new Date(Date.parse(date));
      } else {
        return null;
      }
    };
    // #endregion

    mainSelf.transRequest = function (data) {

      if (mainSelf.isNullOrEmptyValue(data))
      return null;

      if (mainSelf.isNullOrEmptyValue(data.isw)) {
        data.request.process = urlSw.process;
        var sessionCookie = $cookies.get('session');
        if (!mainSelf.isNullOrEmptyValue(sessionCookie)) {
          var session = JSON.parse(sessionCookie);
          data.request.session = session.session;
        }
        return data.request;
      } else {
        return data;
      }
    };

    mainSelf.transResponse = function (data) {

      if (mainSelf.isNullOrEmptyValue(data))
      return null;

      if (!mainSelf.IsJsonString(data))
      return data;

      var response = JSON.parse(data);

      return response;
    };

    mainSelf.ResponseSw = function (message, hasCustomError, hasError) {
      var self = this;

      self.message = message;
      self.hasCustomError = hasCustomError || false;
      self.hasError = hasError || false;
    };

    mainSelf.RequestSw = function (startIndex, lastIndex, takeAll, operatorToUse, orderBy, isNotificationToShow, titleNotificationToShow, notificationToShow, showLoader) {
      var self = this;

      self.orderBy = orderBy || "ASC";
      self.takeAll = takeAll || false;
      self.lastIndex = lastIndex || 8;
      self.startIndex = startIndex || 1;
      self.operatorToUse = operatorToUse || "NONE";
      self.isNotificationToShow = isNotificationToShow || false;
      self.titleNotificationToShow = titleNotificationToShow || "";
      self.notificationToShow = notificationToShow || "";
      self.showLoader = mainSelf.isNullOrEmptyValue(showLoader) ? true : showLoader;
    };

    mainSelf.IsUndefinedOrNull = function (object) {
      return object === null || typeof object === "undefined";
    };

    mainSelf.isNullOrEmptyValue = function (value) {
      var valueBool = false;
      if (typeof value === "boolean" || typeof value === "number") {
        valueBool = mainSelf.IsUndefinedOrNull(value);
      } else {
        valueBool = (mainSelf.IsUndefinedOrNull(value) || value === "" || !value || 0 === value.length);
      }
      return valueBool;
    };

    mainSelf.objectIsNull = function (object) {
      var valueBool = false;
      if (!mainSelf.isNullOrEmptyValue(object)) {
        for (var i in object) {
          if (typeof object[i] === "object") {
            if (object[i] instanceof Array) {
              if (object[i].length > 0) {
                valueBool = true;
                break;
              }
            } else {
              valueBool = mainSelf.objectIsNull(object[i]);
              if (valueBool) break;
            }
          } else {
            if (!mainSelf.isNullOrEmptyValue(object[i])) {
              valueBool = true;
              break;
            }
          }
        }
      }
      return valueBool;
    };

    mainSelf.RemoveItem = function (array, key, value) {

      mainSelf.writeInConsole("remove item");

      var cloneArray = typeof (array) === "function" ? array() : array;

      for (var j = 0; j < cloneArray.length; j++) {
        if (cloneArray[j][key] === value) {
          array.splice(j, 1);
          break;
        }
      }
    };

    mainSelf.arrayContainItems = function (array) {
      return (!mainSelf.isNullOrEmptyValue(array) && array.length > 0);
    };

    mainSelf.GetItem = function (array, key, value) {

      var cloneArray = typeof (array) === "function" ? array() : array;

      if (mainSelf.isNullOrEmptyValue(cloneArray))
      return null;

      for (var i = 0; i < cloneArray.length; i++) {
        if (cloneArray[i][key] === value) {
          return cloneArray[i];
        }
      }

      return null;
    };

    mainSelf.GetItems = function (array, key, value) {

      var cloneArray = typeof (array) === "function" ? array() : array;

      if (mainSelf.isNullOrEmptyValue(cloneArray))
      return null;

      var liste = new Array();
      for (var i = 0; i < cloneArray.length; i++) {
        if (cloneArray[i][key] === value) {
          liste.push(cloneArray[i]);
        }
      }

      return liste;
    };

    mainSelf.TakeItem = function (array, key, value) {

      var item = mainSelf.GetItem(array, key, value);
      mainSelf.RemoveItem(array, key, value);

      return item;
    };

    mainSelf.ExtensionProperties = function (obj, dto, className) {
      for (var j in dto) {
        for (var i in obj) {
          var propExt = j + className + "Ext";
          if (propExt === i) {
            dto[j] = obj[i];
          } else {
            if (j === i)
            dto[j] = obj[i];
          }
        }
      }

      return dto;
    };

    mainSelf.customFilterable = function () {
      var obj = {
        messages: {
          and: "et",
          extra: false,
          clear: "Effacer",
          filter: "Appliquer",
          info: "Filtrer par:",
          isFalse: "False",
          or: "ou",
          selectValue: "Sélectionner la catégorie",
          cancel: "Annuler",
          operator: "Sélectionner l'opérateur",
          value: "Sélectionner la valeur"
        },
        operators: {
          string: {
            eq: "Egal à",
            neq: "Différent de",
            startswith: "Commence avec",
            contains: "Contient",
            doesnotcontain: "Ne contient pas",
            endswith: "Se termine avec"
          },
          number: {
            gte: "Supérieur ou égal à",
            gt: "Plus grand que",
            lte: "Inférieur ou égal à",
            lt: "Plus petit que"
          }
        }
      };

      return obj;
    };

    mainSelf.ClearCache = function () {
      var cookies = $cookies.getAll();
      angular.forEach(cookies, function (v, k) {
        $cookies.remove(k);
      });
    };


    mainSelf.SaveContainsCorbeille = function (item, step) {
      if (mainSelf.objectIsNull(item) && !mainSelf.isNullOrEmptyValue(step)) {
        let generatedKeyFromStep = step.replace(' ', '_');
        $cookies.put(generatedKeyFromStep, JSON.stringify(item));
      }
    };

    mainSelf.resetContainsCorbeille = function (item, step) {
      let generatedKeyFromStep = step.replace(' ', '_');
      var cookieItem = $cookies.get(generatedKeyFromStep);
      $cookies.remove(cookieItem);
    };

    mainSelf.ResetQuickSidebar = function () {
      $('body').removeClass('page-quick-sidebar-open');
      $rootScope.$commentaires = [];
      $rootScope.$documents = [];
    };

    // TreeView config
    mainSelf.TreeViewConfig = function (elts, nameFieldParent, nameChildsArray, nameFieldChild, idFieldChild) {
      var eltsTreeView = [];

      // parent
      angular.forEach(elts, function (item, key) {
        var elt = {};

        elt.text = item[nameFieldParent];
        elt.icon = "fa fa-folder icon-state-success";
        elt.state = {
          opened: true,
          selected: false
        }
        elt.children = [];

        // child
        angular.forEach(item[nameChildsArray], function (child, key) {
          var eltChild = {};

          eltChild.text = child[nameFieldChild];
          eltChild.customId = child[idFieldChild];
          eltChild.icon = "fa fa-folder icon-state-default";
          eltChild.state = {
            selected: false
          }

          elt.children.push(eltChild);
        });

        eltsTreeView.push(elt);
      });

      return eltsTreeView;
    };

    // début définition
    mainSelf.writeInConsole = function (text) {
      if (typeof console !== "undefined") {
        console.log(text);
      }
    };

    mainSelf.utf8_encode = function (argString) {

      if (argString === null || typeof argString === "undefined") {
        return "";
      }

      var string = (argString + ""); // .replace(/\r\n/g, "\n").replace(/\r/g, "\n");
      var utftext = "",
      start,
      end,
      stringl = 0;

      start = end = 0;
      stringl = string.length;
      for (var n = 0; n < stringl; n++) {
        var c1 = string.charCodeAt(n);
        var enc = null;

        if (c1 < 128) {
          end++;
        } else if (c1 > 127 && c1 < 2048) {
          enc = String.fromCharCode(
            (c1 >> 6) | 192, (c1 & 63) | 128
          );
        } else if ((c1 & 0xF800) != 0xD800) {
          enc = String.fromCharCode(
            (c1 >> 12) | 224, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
          );
        } else { // surrogate pairs
          if ((c1 & 0xFC00) != 0xD800) {
            throw new RangeError("Unmatched trail surrogate at " + n);
          }
          var c2 = string.charCodeAt(++n);
          if ((c2 & 0xFC00) != 0xDC00) {
            throw new RangeError("Unmatched lead surrogate at " + (n - 1));
          }
          c1 = ((c1 & 0x3FF) << 10) + (c2 & 0x3FF) + 0x10000;
          enc = String.fromCharCode(
            (c1 >> 18) | 240, ((c1 >> 12) & 63) | 128, ((c1 >> 6) & 63) | 128, (c1 & 63) | 128
          );
        }
        if (enc !== null) {
          if (end > start) {
            utftext += string.slice(start, end);
          }
          utftext += enc;
          start = end = n + 1;
        }
      }

      if (end > start) {
        utftext += string.slice(start, stringl);
      }

      return utftext;
    };
    // #region encode
    mainSelf.base64Encode = function (str) {
      return $.base64.encode(str);
    };

    mainSelf.base64Decode = function (str) {
      return $.base64.decode(str);
    };

    // custom
    mainSelf.encode = function (str, iteration) {
      var i = 0, response = "";
      while (i < iteration) {
        response = mainSelf.base64Encode(str);
        str = angular.copy(response);
        i++;
      }
      return response;
    };

    mainSelf.decode = function (str) {
      var response = mainSelf.base64Decode(str);
      return mainSelf.base64Decode(mainSelf.base64Decode(response));
    };

    mainSelf.encrypt = function (msg, iteration) {
      var keySize = 128;
      var iterations = 100;

      var salt = CryptoJS.lib.WordArray.random(128 / 8);
      var iv = CryptoJS.lib.WordArray.random(128 / 8);
      var secret = CryptoJS.lib.WordArray.random(128 / 8);

      var key = CryptoJS.PBKDF2(secret.toString(), salt, {
        keySize: keySize / 32,
        iterations: iterations
      });

      var encrypted = CryptoJS.AES.encrypt(msg, key, {
        iv: iv,
        padding: CryptoJS.pad.Pkcs7,
        mode: CryptoJS.mode.CBC
      });

      var transitmessage = salt.toString() + iv.toString() + secret.toString() + iteration.toString() + encrypted.toString();
      return transitmessage;
    }

    mainSelf.randomString = function (length) {
      var result = "";
      var chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      for (var i = length; i > 0; --i)
      result += chars[Math.floor(Math.random() * chars.length)];
      return result;
    }

    mainSelf.decrypt = function (transitmessage, pass) {
      var keySize = 128;
      var iterations = 100;

      var salt = CryptoJS.enc.Hex.parse(transitmessage.substr(0, 32));
      var iv = CryptoJS.enc.Hex.parse(transitmessage.substr(32, 32))
      var encrypted = transitmessage.substring(64);

      var key = CryptoJS.PBKDF2(pass, salt, {
        keySize: keySize / 32,
        iterations: iterations
      });

      var decrypted = CryptoJS.AES.decrypt(encrypted, key, {
        iv: iv,
        padding: CryptoJS.pad.Pkcs7,
        mode: CryptoJS.mode.CBC

      })
      return decrypted;
    }

    mainSelf.ConvertArrayToObject = function (array) {
      var object = {};
      angular.forEach(array, function (item, key) {
        object[item["key"]] = item["value"];
      });
      return object;
    };

    mainSelf.IsJsonString = function (str) {
      try {
        JSON.parse(str);
      } catch (e) {
        return false;
      }
      return true;
    };

    function uniteFunc(nombre) {
      var unite;
      switch (nombre) {
        case 0:
        unite = "zéro";
        break;
        case 1:
        unite = "un";
        break;
        case 2:
        unite = "deux";
        break;
        case 3:
        unite = "trois";
        break;
        case 4:
        unite = "quatre";
        break;
        case 5:
        unite = "cinq";
        break;
        case 6:
        unite = "six";
        break;
        case 7:
        unite = "sept";
        break;
        case 8:
        unite = "huit";
        break;
        case 9:
        unite = "neuf";
        break;
      } //fin switch
      return unite;
    }

    function dizaineFunc(nombre) {
      var dizaine;
      switch (nombre) {
        case 10:
        dizaine = "dix";
        break;
        case 11:
        dizaine = "onze";
        break;
        case 12:
        dizaine = "douze";
        break;
        case 13:
        dizaine = "treize";
        break;
        case 14:
        dizaine = "quatorze";
        break;
        case 15:
        dizaine = "quinze";
        break;
        case 16:
        dizaine = "seize";
        break;
        case 17:
        dizaine = "dix-sept";
        break;
        case 18:
        dizaine = "dix-huit";
        break;
        case 19:
        dizaine = "dix-neuf";
        break;
        case 20:
        dizaine = "vingt";
        break;
        case 30:
        dizaine = "trente";
        break;
        case 40:
        dizaine = "quarante";
        break;
        case 50:
        dizaine = "cinquante";
        break;
        case 60:
        dizaine = "soixante";
        break;
        case 70:
        dizaine = "soixante-dix";
        break;
        case 80:
        dizaine = "quatre-vingt";
        break;
        case 90:
        dizaine = "quatre-vingt-dix";
        break;
      } //fin switch
      return dizaine;
    }

    function numberToLetterFunction(nombre) {
      var i, j, n, quotient, reste, nb;
      var ch;
      var numberToLetter = "";
      //__________________________________

      if (nombre.toString().replace(/ /gi, "").length > 15)
      return "dépassement de capacité";
      if (isNaN(nombre.toString().replace(/ /gi, "")))
      return "Nombre non valide";

      nb = parseFloat(nombre.toString().replace(/ /gi, ""));
      if (Math.ceil(nb) != nb)
      return "Nombre avec virgule non géré.";

      n = nb.toString().length;
      switch (n) {
        case 1:
        numberToLetter = uniteFunc(nb);
        break;
        case 2:
        if (nb > 19) {
          quotient = Math.floor(nb / 10);
          reste = nb % 10;
          if (nb < 71 || (nb > 79 && nb < 91)) {
            if (reste == 0)
            numberToLetter = dizaineFunc(quotient * 10);
            if (reste == 1)
            numberToLetter = dizaineFunc(quotient * 10) + "-et-" + uniteFunc(reste);
            if (reste > 1)
            numberToLetter = dizaineFunc(quotient * 10) + "-" + uniteFunc(reste);
          } else
          numberToLetter = dizaineFunc((quotient - 1) * 10) + "-" + dizaineFunc(10 + reste);
        } else
        numberToLetter = dizaineFunc(nb);
        break;
        case 3:
        quotient = Math.floor(nb / 100);
        reste = nb % 100;
        if (quotient == 1 && reste == 0)
        numberToLetter = "cent";
        if (quotient == 1 && reste != 0)
        numberToLetter = "cent" + " " + numberToLetterFunction(reste);
        if (quotient > 1 && reste == 0)
        numberToLetter = uniteFunc(quotient) + " cents";
        if (quotient > 1 && reste != 0)
        numberToLetter = uniteFunc(quotient) + " cent " + numberToLetterFunction(reste);
        break;
        case 4:
        quotient = Math.floor(nb / 1000);
        reste = nb - quotient * 1000;
        if (quotient == 1 && reste == 0)
        numberToLetter = "mille";
        if (quotient == 1 && reste != 0)
        numberToLetter = "mille" + " " + numberToLetterFunction(reste);
        if (quotient > 1 && reste == 0)
        numberToLetter = numberToLetterFunction(quotient) + " mille";
        if (quotient > 1 && reste != 0)
        numberToLetter = numberToLetterFunction(quotient) + " mille " + numberToLetterFunction(reste);
        break;
        case 5:
        quotient = Math.floor(nb / 1000);
        reste = nb - quotient * 1000;
        if (quotient == 1 && reste == 0)
        numberToLetter = "mille";
        if (quotient == 1 && reste != 0)
        numberToLetter = "mille" + " " + numberToLetterFunction(reste);
        if (quotient > 1 && reste == 0)
        numberToLetter = numberToLetterFunction(quotient) + " mille";
        if (quotient > 1 && reste != 0)
        numberToLetter = numberToLetterFunction(quotient) + " mille " + numberToLetterFunction(reste);
        break;
        case 6:
        quotient = Math.floor(nb / 1000);
        reste = nb - quotient * 1000;
        if (quotient == 1 && reste == 0)
        numberToLetter = "mille";
        if (quotient == 1 && reste != 0)
        numberToLetter = "mille" + " " + numberToLetterFunction(reste);
        if (quotient > 1 && reste == 0)
        numberToLetter = numberToLetterFunction(quotient) + " mille";
        if (quotient > 1 && reste != 0)
        numberToLetter = numberToLetterFunction(quotient) + " mille " + numberToLetterFunction(reste);
        break;
        case 7:
        quotient = Math.floor(nb / 1000000);
        reste = nb % 1000000;
        if (quotient == 1 && reste == 0)
        numberToLetter = "un million";
        if (quotient == 1 && reste != 0)
        numberToLetter = "un million" + " " + numberToLetterFunction(reste);
        if (quotient > 1 && reste == 0)
        numberToLetter = numberToLetterFunction(quotient) + " millions";
        if (quotient > 1 && reste != 0)
        numberToLetter = numberToLetterFunction(quotient) + " millions " + numberToLetterFunction(reste);
        break;
        case 8:
        quotient = Math.floor(nb / 1000000);
        reste = nb % 1000000;
        if (quotient == 1 && reste == 0)
        numberToLetter = "un million";
        if (quotient == 1 && reste != 0)
        numberToLetter = "un million" + " " + numberToLetterFunction(reste);
        if (quotient > 1 && reste == 0)
        numberToLetter = numberToLetterFunction(quotient) + " millions";
        if (quotient > 1 && reste != 0)
        numberToLetter = numberToLetterFunction(quotient) + " millions " + numberToLetterFunction(reste);
        break;
        case 9:
        quotient = Math.floor(nb / 1000000);
        reste = nb % 1000000;
        if (quotient == 1 && reste == 0)
        numberToLetter = "un million";
        if (quotient == 1 && reste != 0)
        numberToLetter = "un million" + " " + numberToLetterFunction(reste);
        if (quotient > 1 && reste == 0)
        numberToLetter = numberToLetterFunction(quotient) + " millions";
        if (quotient > 1 && reste != 0)
        numberToLetter = numberToLetterFunction(quotient) + " millions " + numberToLetterFunction(reste);
        break;
        case 10:
        quotient = Math.floor(nb / 1000000000);
        reste = nb - quotient * 1000000000;
        if (quotient == 1 && reste == 0)
        numberToLetter = "un milliard";
        if (quotient == 1 && reste != 0)
        numberToLetter = "un milliard" + " " + numberToLetterFunction(reste);
        if (quotient > 1 && reste == 0)
        numberToLetter = numberToLetterFunction(quotient) + " milliards";
        if (quotient > 1 && reste != 0)
        numberToLetter = numberToLetterFunction(quotient) + " milliards " + numberToLetterFunction(reste);
        break;
        case 11:
        quotient = Math.floor(nb / 1000000000);
        reste = nb - quotient * 1000000000;
        if (quotient == 1 && reste == 0)
        numberToLetter = "un milliard";
        if (quotient == 1 && reste != 0)
        numberToLetter = "un milliard" + " " + numberToLetterFunction(reste);
        if (quotient > 1 && reste == 0)
        numberToLetter = numberToLetterFunction(quotient) + " milliards";
        if (quotient > 1 && reste != 0)
        numberToLetter = numberToLetterFunction(quotient) + " milliards " + numberToLetterFunction(reste);
        break;
        case 12:
        quotient = Math.floor(nb / 1000000000);
        reste = nb - quotient * 1000000000;
        if (quotient == 1 && reste == 0)
        numberToLetter = "un milliard";
        if (quotient == 1 && reste != 0)
        numberToLetter = "un milliard" + " " + numberToLetterFunction(reste);
        if (quotient > 1 && reste == 0)
        numberToLetter = numberToLetterFunction(quotient) + " milliards";
        if (quotient > 1 && reste != 0)
        numberToLetter = numberToLetterFunction(quotient) + " milliards " + numberToLetterFunction(reste);
        break;
        case 13:
        quotient = Math.floor(nb / 1000000000000);
        reste = nb - quotient * 1000000000000;
        if (quotient == 1 && reste == 0)
        numberToLetter = "un billion";
        if (quotient == 1 && reste != 0)
        numberToLetter = "un billion" + " " + numberToLetterFunction(reste);
        if (quotient > 1 && reste == 0)
        numberToLetter = numberToLetterFunction(quotient) + " billions";
        if (quotient > 1 && reste != 0)
        numberToLetter = numberToLetterFunction(quotient) + " billions " + numberToLetterFunction(reste);
        break;
        case 14:
        quotient = Math.floor(nb / 1000000000000);
        reste = nb - quotient * 1000000000000;
        if (quotient == 1 && reste == 0)
        numberToLetter = "un billion";
        if (quotient == 1 && reste != 0)
        numberToLetter = "un billion" + " " + numberToLetterFunction(reste);
        if (quotient > 1 && reste == 0)
        numberToLetter = numberToLetterFunction(quotient) + " billions";
        if (quotient > 1 && reste != 0)
        numberToLetter = numberToLetterFunction(quotient) + " billions " + numberToLetterFunction(reste);
        break;
        case 15:
        quotient = Math.floor(nb / 1000000000000);
        reste = nb - quotient * 1000000000000;
        if (quotient == 1 && reste == 0)
        numberToLetter = "un billion";
        if (quotient == 1 && reste != 0)
        numberToLetter = "un billion" + " " + numberToLetterFunction(reste);
        if (quotient > 1 && reste == 0)
        numberToLetter = numberToLetterFunction(quotient) + " billions";
        if (quotient > 1 && reste != 0)
        numberToLetter = numberToLetterFunction(quotient) + " billions " + numberToLetterFunction(reste);
        break;
      } //fin switch
      /*respect de l"accord de quatre-vingt*/
      if (numberToLetter.substr(numberToLetter.length - "quatre-vingt".length, "quatre-vingt".length) == "quatre-vingt")
      numberToLetter = numberToLetter + "s";

      return numberToLetter;
    }

    mainSelf.toLetter = function (nombre) {
      return numberToLetterFunction(nombre);
    };

    mainSelf.b64toBlob = function (base64Data, contentType, sliceSize) {
      contentType = contentType || '';
      var sliceSize = 1024;
      var byteCharacters = atob(base64Data);
      var bytesLength = byteCharacters.length;
      var slicesCount = Math.ceil(bytesLength / sliceSize);
      var byteArrays = new Array(slicesCount);

      for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
        var begin = sliceIndex * sliceSize;
        var end = Math.min(begin + sliceSize, bytesLength);

        var bytes = new Array(end - begin);
        for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
          bytes[i] = byteCharacters[offset].charCodeAt(0);
        }
        byteArrays[sliceIndex] = new Uint8Array(bytes);
      }
      return new Blob(byteArrays, { type: contentType });
    };

    mainSelf.downloadFile = function (blobUrl, fileName) {
      var a = document.createElement("a");
      document.body.appendChild(a);
      a.style = "display: none";
      a.href = blobUrl;
      a.download = fileName;
      a.click();
      a.target = "_blank";
      window.URL.revokeObjectURL(blobUrl);
    };

    // Changes XML to JSON
    mainSelf.xmlToJson = function (xml) {

      // Create the return object
      var obj = {};

      if (xml.nodeType == 1) { // element
        // do attributes
        if (xml.attributes.length > 0) {
          obj["@attributes"] = {};
          for (var j = 0; j < xml.attributes.length; j++) {
            var attribute = xml.attributes.item(j);
            obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
          }
        }
      } else if (xml.nodeType == 3) { // text
        obj = xml.nodeValue;
      }

      // do children
      if (xml.hasChildNodes()) {
        for (var i = 0; i < xml.childNodes.length; i++) {
          var item = xml.childNodes.item(i);
          var nodeName = item.nodeName;
          if (typeof (obj[nodeName]) == "undefined") {
            obj[nodeName] = xmlToJson(item);
          } else {
            if (typeof (obj[nodeName].push) == "undefined") {
              var old = obj[nodeName];
              obj[nodeName] = [];
              obj[nodeName].push(old);
            }
            obj[nodeName].push(xmlToJson(item));
          }
        }
      }
      return obj;
    };

    mainSelf.stringToDate = function (_date, _format, _delimiter) {
      var formatLowerCase = _format.toLowerCase();
      var formatItems = formatLowerCase.split(_delimiter);
      var dateItems = _date.split(_delimiter);
      var monthIndex = formatItems.indexOf("mm");
      var dayIndex = formatItems.indexOf("dd");
      var yearIndex = formatItems.indexOf("yyyy");
      var month = parseInt(dateItems[monthIndex]);
      month -= 1;
      var formatedDate = new Date(dateItems[yearIndex], month, dateItems[dayIndex]);
      return formatedDate;
    };

    //*/ Fonction pour spécifier md-max-date
    mainSelf.getDatesToNow = function () {
      return new Date(
        (new Date()).getFullYear(),
        (new Date()).getMonth(),
        (new Date()).getDate());
      };

    mainSelf.getImageFromBase64 = function (img) {
          switch (img.extPhotoAbonne){
              case 'png':
                  return 'data:image/png;base64,' + img.photoAbonne;
              case 'jpeg':
                  return 'data:image/jpeg;base64,' + img.photoAbonne;
              case 'jpg':
                  return 'data:image/jpg;base64,' + img.photoAbonne;
              case 'gif':
                  return 'data:image/gif;base64,' + img.photoAbonne;
          }
      };

    mainSelf.base64 = function () {
          // Base64 encoding service used by AuthenticationService
          return {

              keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

              encode: function (input) {
                  let output = "";
                  let chr1, chr2, chr3 = "";
                  let enc1, enc2, enc3, enc4 = "";
                  let i = 0;

                  do {
                      chr1 = input.charCodeAt(i++);
                      chr2 = input.charCodeAt(i++);
                      chr3 = input.charCodeAt(i++);

                      enc1 = chr1 >> 2;
                      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                      enc4 = chr3 & 63;

                      if (isNaN(chr2)) {
                          enc3 = enc4 = 64;
                      } else if (isNaN(chr3)) {
                          enc4 = 64;
                      }

                      output = output +
                          this.keyStr.charAt(enc1) +
                          this.keyStr.charAt(enc2) +
                          this.keyStr.charAt(enc3) +
                          this.keyStr.charAt(enc4);
                      chr1 = chr2 = chr3 = "";
                      enc1 = enc2 = enc3 = enc4 = "";
                  } while (i < input.length);

                  return output;
              },

              decode: function (input) {
                  let output = "";
                  let chr1, chr2, chr3 = "";
                  let enc1, enc2, enc3, enc4 = "";
                  let i = 0;

                  // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
                  let base64test = /[^A-Za-z0-9\+\/\=]/g;
                  if (base64test.exec(input)) {
                      window.alert("There were invalid base64 characters in the input text.\n" +
                          "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                          "Expect errors in decoding.");
                  }
                  input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

                  do {
                      enc1 = this.keyStr.indexOf(input.charAt(i++));
                      enc2 = this.keyStr.indexOf(input.charAt(i++));
                      enc3 = this.keyStr.indexOf(input.charAt(i++));
                      enc4 = this.keyStr.indexOf(input.charAt(i++));

                      chr1 = (enc1 << 2) | (enc2 >> 4);
                      chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                      chr3 = ((enc3 & 3) << 6) | enc4;

                      output = output + String.fromCharCode(chr1);

                      if (enc3 !== 64) {
                          output = output + String.fromCharCode(chr2);
                      }
                      if (enc4 !== 64) {
                          output = output + String.fromCharCode(chr3);
                      }

                      chr1 = chr2 = chr3 = "";
                      enc1 = enc2 = enc3 = enc4 = "";

                  } while (i < input.length);

                  return output;
              }
          };
      };

    mainSelf.validateEmail = function (email) {
        console.log('email value', email);
        return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
    };
    }
  ]);
