(function () {
    'use strict';

    angular.module('app.commons', [
        //*/
        'oc.lazyLoad',
        'ncy-angular-breadcrumb',
        'angular-loading-bar',
        'ngMap',
        'ui.bootstrap',
        'ui.router',
        'vTabs',
        'dynamicNumber',
        'ngCookies',
        'LocalStorageModule',
        'vButton',
        'vModal',
        'ngFileUpload',
        'mm.acl',
        'permission',
        'permission.ui',
        'angular.filter'
        //'ngMaterial',
        //'mdSteppers',

        //*/
    ])
})();