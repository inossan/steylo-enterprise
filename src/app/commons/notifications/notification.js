(function () {
    'use strict';

    angular.module('app.commons')
        .controller('notificationCtrl', notificationCtrl);

    notificationCtrl.$inject = ['$rootScope', '$scope', '$interval', 'demandeAbonneService', 'CommandeService'];
    function notificationCtrl($rootScope, $scope, $interval, demandeAbonneService, CommandeService) {

        $scope.getBadgeByNombreDemande = getBadgeByNombreDemande;
        $rootScope.callDemandeurNotificationAtInterval = $interval(callDemandeurNotificationAtInterval, 3000);
        $rootScope.animateNombreDemande = $interval(animateNombreDemande, 3000);

        $scope.getBadgeByNombreCommande = getBadgeByNombreCommande;
        $rootScope.callCommandesNotificationAtInterval = $interval(callCommandesNotificationAtInterval, 3000);
        $rootScope.animateNombreCommande = $interval(animateNombreCommande, 3000);


        function animateNombreDemande() {
            if ($scope.notifDemandeurs > 0 ){
                let animationName = 'animated bounce';
                let animationend = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                $('#notif').addClass(animationName).one(animationend, function() {
                    $(this).removeClass(animationName);
                });
            }
        }

        function callDemandeurNotificationAtInterval() {
            demandeAbonneService.getAbonneDemande(0, 0, 3, 'no-loading-bar')  // 0: en attente; 0: page; 3: nombre de demande
                .then(function (res) {
                    if (!res.data.hasError){
                        if (res.data.count === 0){
                            console.log('res.data.count', res.data.count);
                            $scope.demandesAbonne = [];
                        }else {
                            $scope.demandesAbonne = []; //Recoit la liste de toutes les demandes
                            $scope.demandesAbonneTemp = res.data.itemsAbonneDemande;
                            _.forEach($scope.demandesAbonneTemp, function (ad) {  //On reconverti les photos des demandeurs
                                if (ad.photoAbonne){
                                    ad.photoAbonne = getImageFromBase64(ad)
                                }
                                $scope.demandesAbonne.push(ad);
                                console.log('demandesAbonne', $scope.demandesAbonne);
                            });
                        }
                        $scope.notifDemandeurs = res.data.count;
                        console.log('notifDemandeurs', $scope.notifDemandeurs);
                    }
                });
        }

        function getBadgeByNombreDemande(nombreDemande) {
            if (nombreDemande === 0 || nombreDemande === undefined){
                return 'badge-danger';
            }
            if (nombreDemande > 0){
                return 'badge-success';
            }
        }

        function getImageFromBase64(img) {
            switch (img.extPhotoAbonne){
                case 'png':
                    return 'data:image/png;base64,' + img.photoAbonne;
                case 'jpeg':
                    return 'data:image/jpeg;base64,' + img.photoAbonne;
                case 'jpg':
                    return 'data:image/jpg;base64,' + img.photoAbonne;
                case 'gif':
                    return 'data:image/gif;base64,' + img.photoAbonne;
            }
        }

        function animateNombreCommande() {
            if ($scope.notifCommandes > 0 ){
                let animationName = 'animated bounce';
                let animationend = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                $('#comd').addClass(animationName).one(animationend, function() {
                    $(this).removeClass(animationName);
                });
            }
        }

        function callCommandesNotificationAtInterval() {
            CommandeService.getCommandes(statut_commande.EN_ATTENTE, 0, 3, 'no-loading-bar')  // 0: page; 3: nombre de commandes
                .then(function (res) {
                    if (!res.data.hasError){
                        if (res.data.count === 0){
                            console.log('res.data.count', res.data.count);
                            $scope.commandes = [];
                        }else {
                            $scope.commandes = res.data.itemsAbonneCommand;

                            _.forEach($scope.commandes, function (c) {
                                let tot = 0;
                                _.forEach(c.kitDto.kitProduitDtos, function (kp) {
                                    tot += kp.quantite * kp.prix;
                                    console.log('tot', tot);
                                });
                                c.total = tot;
                            });

                            console.log('commandesNotif', $scope.commandes);
                        }
                        $scope.notifCommandes = res.data.count;
                        console.log('notifCommandes', $scope.notifCommandes);
                    }
                });
        }

        function getBadgeByNombreCommande(nombreCommande) {
            if (nombreCommande === 0 || nombreCommande === undefined){
                return 'badge-danger';
            }
            if (nombreCommande > 0){
                return 'badge-success';
            }
        }
    }
})();