(function () {
    'use strict';

    angular.module('app.commons')
        .controller('notificationCommandesCtrl', notificationCommandesCtrl);

    notificationCommandesCtrl.$inject = ['$rootScope', '$scope', '$interval', 'CommandeService'];
    function notificationCommandesCtrl($rootScope, $scope, $interval, CommandeService) {

        $scope.getBadgeByNombreCommande = getBadgeByNombreCommande;
        $rootScope.callCommandesNotificationAtInterval = $interval(callCommandesNotificationAtInterval, 3000);
        $rootScope.animateNombreCommande = $interval(animateNombreCommande, 3000);

        function animateNombreCommande() {
            if ($scope.notifCommandes > 0 ){
                let animationName = 'animated bounce';
                let animationend = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
                $('#comd').addClass(animationName).one(animationend, function() {
                    $(this).removeClass(animationName);
                });
            }
        }

        function callCommandesNotificationAtInterval() {
            CommandeService.getCommandes(statut_commande.EN_ATTENTE, 0, 3, 'no-loading-bar')  // 0: page; 3: nombre de commandes
                .then(function (res) {
                    if (!res.data.hasError){
                        if (res.data.count === 0){
                            console.log('res.data.count', res.data.count);
                            $scope.commandes = [];
                        }else {
                            $scope.commandes = res.data.itemsAbonneCommand;
                        }
                        $scope.notifCommandes = res.data.count;
                        console.log('notifCommandes', $scope.notifCommandes);
                    }
                });
        }

        function getBadgeByNombreCommande(nombreCommande) {
            if (nombreCommande === 0 || nombreCommande === undefined){
                return 'badge-danger';
            }
            if (nombreCommande > 0){
                return 'badge-success';
            }
        }
    }
})();