(function () {
    'use strict';

    angular.module('app.commons')
        .factory('UtilisateurService', UtilisateurService);

    UtilisateurService.$inject = ['$rootScope', '$http'];
    function UtilisateurService($rootScope, $http) {

        const utilisateurService = {};

        utilisateurService.createUser = createUser;
        utilisateurService.updateUser = updateUser;
        utilisateurService.deleteUser = deleteUser;
        utilisateurService.getUsers = getUsers;
        utilisateurService.searchUsers = searchUsers;
        utilisateurService.enableDisableUser = enableDisableUser;

        return utilisateurService;

        function createUser(u) {
            let request = {
                datasChanelUsers: [u]
            };
            let URL = BASE_URL + '/chanelUsers/create';
            return $http.post(URL, request, {cache: true});
        }

        function updateUser(u) {
            let request = {
                datasChanelUsers: [u]
            };
            let URL = BASE_URL + '/chanelUsers/update';
            return $http.post(URL, request, {cache: true});
        }

        function getUsers(index, size) {
            let request = {
                dataChanelUsers:{
                    idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel,
                    trash: false
                    //isRoot: true,
                    //statut: 1
                },
                index: index || 0,
                size: size || 5
            };
            let URL = BASE_URL + '/chanelUsers/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }

        function searchUsers(criteria, index, size) {
            let request = {
                dataChanelUsers: criteria,
                index: index || 0,
                size: size || 5
            };
            let URL = BASE_URL + '/chanelUsers/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }

        function deleteUser(u) {
            let request = {
                datasChanelUsers: [u]
            };
            let URL = BASE_URL + '/chanelUsers/delete';
            return $http.post(URL, request, {cache: true});
        }

        function enableDisableUser(u) {
            let request = {
                datasChanelUsers: [u]
            };
            let URL = BASE_URL + '/chanelUsers/update';
            return $http.post(URL, request, {cache: true});
        }
    }
})();