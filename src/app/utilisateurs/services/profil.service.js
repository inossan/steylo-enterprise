(function () {
    'use strict';

    angular.module('app.utilisateurs')
        .factory('ProfilService', ProfilService);

    ProfilService.$inject = ['$rootScope', '$http'];
    function ProfilService($rootScope, $http) {

        const profilService = {};

        profilService.createProfil = createProfil;
        profilService.updateProfil = updateProfil;
        profilService.deleteProfil = deleteProfil;
        profilService.getProfils = getProfils;

        return profilService;

        function createProfil(p) {
            let request = {
                datasPermissionProfil: [p]
            };
            let URL = BASE_URL + '/permissionProfil/create';
            return $http.post(URL, request, {cache: true});
        }

        function updateProfil(p) {
            let request = {
                datasPermissionProfil: [p]
            };
            let URL = BASE_URL + '/permissionProfil/update';
            return $http.post(URL, request, {cache: true});
        }

        function deleteProfil(p) {
            let request = {
                datasPermissionProfil: [p]
            };
            let URL = BASE_URL + '/permissionProfil/delete';
            return $http.post(URL, request, {cache: true});
        }

        function getProfils(index, size) {
            let request = {
                dataPermissionProfil:{
                    idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel,
                },
                index: index || 0,
                size: size || 5
            };
            let URL = BASE_URL + '/permissionProfil/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }
    }
})();