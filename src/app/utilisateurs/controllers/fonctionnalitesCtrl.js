(function () {
    'use strict';

    angular.module('app.utilisateurs')
        .controller('fonctionnalitesCtrl', fonctionnalitesCtrl);

    fonctionnalitesCtrl.$inject = ['$rootScope', '$scope', 'FonctionnaliteService', 'SousFonctionnaliteService', 'toastr'];
    function fonctionnalitesCtrl($rootScope, $scope, FonctionnaliteService, SousFonctionnaliteService, toastr) {

        $scope.getFonctionnaliteActions = getFonctionnaliteActions;
        $scope.createSousFonctionnalite = createSousFonctionnalite;
        $rootScope.updateSousFonctionnalite = updateSousFonctionnalite;
        $scope.deleteSousFonctionnalite = deleteSousFonctionnalite;
        $scope.getSousFonctionnalite = getSousFonctionnalite;
        $scope.showSousFonctionnalite = showSousFonctionnalite;

        getFonctionnaliteActions();
        getSousFonctionnalite();

        $scope.currentPage = 1;
        $scope.sizeSF = 5;
        $scope.max_size = 3;

        let modalEditSF = $('#edit-sf');

        function getFonctionnaliteActions(index, size) {
            FonctionnaliteService.getFonctionnalite(index, size)
                .then(function (res) {
                    if (!res.data.hasError){
                        $rootScope.fonctionnalitesActions = res.data.itemsChanelPermissions;
                        $scope.totalFonctionnalitesActions = res.data.count || 0;
                    }
                }, function (err) {
                    console.log('fonctionnalites err', err);
                })
        }

        function createSousFonctionnalite(sf) {

            if (!sf){
                sf = {};
            }
            if (!sf.hasOwnProperty('libelle') || sf.libelle === ''){
                toastr.error('Veuillez renseigner le libellé !', 'Erreur');
                return;
            }
            if (!sf.hasOwnProperty('idPermission') || sf.idPermission === ''){
                toastr.error('Veuillez renseigner la fonctionnalité parente !', 'Erreur');
                return;
            }

            SousFonctionnaliteService.createSousFonctionnalite(sf)
                .then(function (res) {
                    console.log('create sous fonctionnalites res', res);
                    if (!res.data.hasError){
                        getSousFonctionnalite();
                        toastr.success('Enregistrement effectué avec succès', 'Succès');
                    }else {
                        toastr.success(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('create sous fonctionnalites err', err);
                })
        }

        function updateSousFonctionnalite(sf) {
            SousFonctionnaliteService.updateSousFonctionnalite(sf)
                .then(function (res) {
                    console.log('update sous fonctionnalites res', res);
                    if (!res.data.hasError){
                        getSousFonctionnalite();
                        modalEditSF.modal('hide');
                        toastr.success('Modification effectuée avec succès', 'Succès');
                    }else {
                        toastr.success(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('update sous fonctionnalites err', err);
                })
        }

        function deleteSousFonctionnalite(sf) {
            SousFonctionnaliteService.deleteSousFonctionnalite(sf)
                .then(function (res) {
                    console.log('delete sous fonctionnalites res', res);
                    if (!res.data.hasError){
                        getSousFonctionnalite();
                        toastr.success('Suppression effectuée avec succès', 'Succès');
                    }else {
                        toastr.success(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('delete sous fonctionnalites err', err);
                })
        }

        function getSousFonctionnalite(index, size) {
            SousFonctionnaliteService.getSousFonctionnalites(index, size)
                .then(function (res) {
                    console.log('get sous fonctionnalites res', res);
                    if (!res.data.hasError){
                        $scope.sousFonctionnalites = res.data.itemsPermissionActions;
                        $scope.totaSousFonctionnalites = res.data.count || 0;
                    }else {
                        toastr.success(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('get sous fonctionnalites err', err);
                })
        }

        function showSousFonctionnalite(sf) {
            $rootScope.editingSF = angular.copy(sf);
            $rootScope.editingSFTitle = sf.libelle;
            modalEditSF.modal('show');
        }

    }
})();