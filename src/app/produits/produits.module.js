(function () {
    'use strict';

    angular.module('app.produits', [
        'app.commons',
        'ui.router',
        //*/
        'oc.lazyLoad',
        'ncy-angular-breadcrumb',
        'angular-loading-bar',
        'textAngular',
        'rzModule',
        'ckeditor',
        'vTabs',
        //'mdSteppers',

        //*/
    ])
        .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {

            $ocLazyLoadProvider.config({
                // Set to true if you want to see what and when is dynamically loaded
                debug: true
            });

            $urlRouterProvider.otherwise('/login');
            $stateProvider
                .state('app.produits', {
                    url: '/produits',
                    templateUrl: 'app/produits/views/produits.html',
                    ncyBreadcrumb: {
                        label: 'Gestion des produits'
                    },
                    controller: 'prodCtrl',
                    resolve: {
                        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                            // you can lazy load controllers
                            return $ocLazyLoad.load({
                                files: [
                                    'app/produits/controllers/produitsCtrl.js',
                                    'app/produits/controllers/mesProduitsCtrl.js',
                                    'app/produits/services/produit.service.js'
                                ]
                            });
                        }]
                    }
                })
        }])
})();