(function () {
    'use strict';

    angular.module('app.produits')
        .controller('prodCtrl', prodCtrl);

    prodCtrl.$inject = ['$rootScope', '$scope', '$state', 'ProduitService', 'CategoriesService', 'SousCategoriesService', 'MarqueService', 'notificationService', 'toastr', '$sweetAlert', 'AclService'];
    function prodCtrl($rootScope, $scope, $state, ProduitService, CategoriesService, SousCategoriesService, MarqueService, notificationService, toastr, $sweetAlert, AclService) {

        $scope.can = AclService.can;
        $scope.saveProduit = saveProduit;
        $scope.updateProduit = updateProduit;
        $scope.updateProduitImport = updateProduitImport;
        $scope.reinitProduit = reinitProduit;
        $scope.showEditProduitModal = showEditProduitModal;
        $rootScope.getCategories = getCategories;
        $rootScope.getSousCategories = getSousCategories;
        //$rootScope.getCategories = getCategories;
        $scope.getMarques = getMarques;
        $scope.changeMinSlider = changeMinSlider;
        $scope.changeMaxSlider = changeMaxSlider;
        $rootScope.getImportProduits = getImportProduits;
        $scope.importerCatalogProduit = importerCatalogProduit;

        getImportProduits();
        getCategories();
        getMarques();

        //Configuration du tabs-----------------------------
        //*/
        $scope.tabs = {
            active: 0
        };
        $scope.pages = {
            active: 0
        };
        $scope.horizontalTabs = {
            active: 0
        };
        $scope.pages = [
            {
                id: 1,
                title: 'Saisir un nouveau produit',
                badge: true //Pour afficher le nombre de demande  dans un badge uniquement pour cet onglet
            },
            {
                id: 2,
                title: 'Importer votre catalogue produit',
                content: ''
            }
        ];
        //*/
        //Fin de la configuration du tabs-------------------

        //$scope.produit = {};
        $scope.minNum = 10;
        $scope.slider = {
            minValue: 1,
            maxValue: 1000000000,
            options: {
                floor: 1,
                ceil: 1000000000,
                step: 1,
                noSwitching: true,
                hideLimitLabels: true,
                hidePointerLabels: true,
                selectionBarGradient: {
                    from: 'white',
                    to: brandInfo
                },
                id: 'slider-id',
                onStart: function() {
                    console.log('on start ' + id); // logs 'on start slider-id'
                },
                onChange: function(id) {
                    console.log('on change ' + id); // logs 'on change slider-id'
                },
                onEnd: function(id) {
                    console.log('on end ' + id); // logs 'on end slider-id'
                }
            }
        };
        $scope.min = 1;
        $scope.max = 1000000000;

        $scope.produit = {};

        $scope.currentPage = 1;
        $scope.currentPageS = 1;
        $scope.sizeProduit = 5;
        $scope.max_size = 3;

        $scope.subview = 'ajouter-produit'; //On choisit l'onglet 'ajout-produit' par défaut
        $scope.documents = [];
        $scope.dropzone = {
            id: "dropzone",
            maxFiles: 5,
            addFile: function (document) {
                let toCheckExistingDocument = {
                    fileName: document.file.name,
                    type: document.file.type,
                    data: document.data
                };
                if (_.find($scope.documents, toCheckExistingDocument)){
                    console.log('trouvé');
                    //Supprimer le doublon d'image
                    this.removeFile(document);
                    return;
                }
                $scope.documents.push({
                    fileName: document.file.name,
                    type: document.file.type,
                    data: document.data
                });

                console.log('$scope.documents', $scope.documents);
                console.log('document', document);
            },
            removeFile: function (file) {
                $scope.item.documents = angular.copy(Enumerable.From($scope.documents).Where(function (doc) {
                    return doc.fileName !== file.name;
                }).Select(function (doc) {
                    return doc;
                }).ToArray());
            }
        };

        $scope.documentsImport = [];
        $scope.dropzoneImport = {
            id: "dropzoneImport",
            maxFiles: 5,
            addFile: function (document) {
                let toCheckExistingDocument = {
                    fileName: document.file.name,
                    type: document.file.type,
                    data: document.data
                };
                if (_.find($scope.documentsImport, toCheckExistingDocument)){
                    console.log('trouvé');
                    //Supprimer le doublon d'image
                    this.removeFile(document);
                    return;
                }
                $scope.documentsImport.push({
                    fileName: document.file.name,
                    type: document.file.type,
                    data: document.data
                });

                console.log('$scope.documents', $scope.documentsImport);
                console.log('document', document);
            },
            removeFile: function (file) {
                $scope.item.documents = angular.copy(Enumerable.From($scope.documentsImport).Where(function (doc) {
                    return doc.fileName !== file.name;
                }).Select(function (doc) {
                    return doc;
                }).ToArray());
            }
        };

        // Editor options pour ckeditor
        $scope.options = {
            language: 'fr',
            allowedContent: true,
            entities: false
        };

        let modalEditProduit = $('#edit-produit');
        $scope.windowHeightP = $(window).innerHeight() - 260;
/*/

        $scope.setHeight = function() {

            let wwindowHeight = $(window).innerHeight();
            console.log("$rootScope.windowHeight ="+$rootScope.windowHeight);
            console.log("wwindowHeight ="+wwindowHeight);
            //windowHeight= windowHeight-270;
            //$rootScope.windowHeight = windowHeight;

            //$rootScope.Height="height:"+$rootScope.windowHeight+"px;";
            //console.log("$rootScope.Height ="+windowHeight);
        };

        $scope.setHeight();
//*/

        function changeMinSlider(prixMin) {
            if (prixMin < $scope.slider.maxValue){
                if (prixMin > $scope.min) {
                    $scope.slider.minValue = prixMin
                }
            }else {
                $scope.slider.minValue = $scope.slider.maxValue;
                $scope.minNum = $scope.slider.maxValue.toString().length;
                console.log('minNum', $scope.minNum);
            }
            /*
            if ($scope.slider.minValue < $scope.min) {
                $scope.produit.prixMin = $scope.min
            }
            else{
                $scope.produit.prixMin = $scope.slider.minValue
            }*/
        }

        function changeMaxSlider(prixMax) {
            if (prixMax < $scope.max) {
                $scope.slider.maxValue = prixMax
            }
            /*if ($scope.slider.maxValue > $scope.max) {
                $scope.produit.prixMax = $scope.max
            }
            else{
                $scope.produit.prixMax = $scope.slider.maxValue
            }*/
        }

        function getCategories(idMarque, fromImportingIHM) {
            CategoriesService.getCategories(idMarque, fromImportingIHM)
                .then(function (res) {
                    if (!res.data.hasError){
                        $scope.categories = res.data.itemsCategorieProduit;
                        console.log('res categories', res);
                    }
                });
        }

        function getSousCategories(idCategorie) {
            $('#categorie').removeClass('col-md-12'); // Nous permet de redimensionner le champ catégorie lorsque celui-ci est selectionné
            SousCategoriesService.getSousCategories(idCategorie)
                .then(function (res) {
                    if (!res.data.hasError){
                        $rootScope.sousCategories = res.data.itemsSousCategorieProduit;
                        console.log('res sousCategories', res);
                    }
                });
        }

        function getMarques() {
            MarqueService.getMarque()
                .then(function (res) {
                    if (!res.data.hasError){
                        $scope.marques = res.data.itemsMarque;
                        console.log('res marques', res);
                    }
                });
        }

        function saveProduit(produit) {
            console.log('produit', produit);
            //Vérification des champs obligatoires
            if (!produit){
                produit = {};
                $scope.produit = {};
            }
            //console.log('produit', produit);
            if (!produit.hasOwnProperty('referenceProduit') || produit.referenceProduit === ''){
                toastr.error('Veuillez renseigner la référence du produit !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('libelle') || produit.libelle === ''){
                toastr.error('Veuillez renseigner le libellé du produit !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('quantite') || produit.quantite === '' || produit.quantite < 1){
                toastr.error('Veuillez renseigner la quantité ou elle est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('idMarque') || produit.idMarque <= 0){
                toastr.error('Veuillez renseigner la marque ou elle est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('idCategorie') || produit.idCategorie <= 0){
                toastr.error('Veuillez renseigner la catégorie ou elle est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('idSousCategorie') || produit.idSousCategorie <= 0){
                toastr.error('Veuillez renseigner la sous catégorie ou elle est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('prix') || produit.prix === '' || produit.prix <= 0){
                toastr.error('Veuillez renseigner le prix ou il est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('prixMin') || produit.prixMin === '' || produit.prixMin <= 0){
                toastr.error('Veuillez renseigner le prix minimum ou il est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('prixMax') || produit.prixMax === '' || produit.prixMax <= 0){
                toastr.error('Veuillez renseigner le prix maximum ou il est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('size') || produit.size === '' || produit.size <= 0){
                toastr.error('Veuillez renseigner la taille !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('poids') || produit.poids === '' || produit.poids <= 0){
                toastr.error('Veuillez renseigner le poids ou il est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('couleur') || produit.couleur === ''){
                toastr.error('Veuillez renseigner la couleur !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('description') || produit.description === ''){
                toastr.error('Veuillez renseigner la description du produit !', 'Erreur');
                return;
            }
            if (!$scope.documents || _.size($scope.documents) === 0){
                toastr.error('Veuillez renseigner au moins une image !', 'Erreur');
                return;
            }
            produit.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;
            produit.listImageProduit = _.map($scope.documents, function (img) {
                return {
                    image: _.split(img.data, ',')[1],
                    type: _.split(img.type, '/')[1],
                    extImg: _.split(img.type, '/')[1]
                }
            });
            console.log('produit', produit);
            console.log('$scope.documents', $scope.documents);
            //return;

            swal({
                title: 'Voulez-vous vraiment enregistrer ' + produit.libelle + ' ?',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: brandSuccess,
                confirmButtonText: 'Oui, Enregistrer',
                cancelButtonText: 'Non, Annuler',
                closeOnConfirm: false
            }).then((result) => {
                if (result.value) {
                    ProduitService.createProduit(produit)
                        .then(function (res) {
                            if (!res.data.hasError){
                                $scope.documents = [];
                                $('#categorie').addClass('col-md-12');
                                $scope.produit = {};
                                $scope.documents = undefined;
                                $state.go('app.produits');
                                notificationService.flatty.success('<b><i class="fa fa-check"></i>&nbsp;Votre produit a bien été enregistré !</b>');
                                console.log('res create produit', res);
                            }else {
                                toastr.error(res.data.status.message, 'Erreur');
                            }
                        }, function (err) {
                            console.log('err create produit', err);
                        });
                    // result.dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                } else if (result.dismiss === 'cancel') {

                }
            });
        }

        function showEditProduitModal(produit) {
            //toastr.info('En cours de maintenance !', 'Information');
            //return; // A commenter pour la modification d'un produit
            $rootScope.editingProduit = angular.copy(produit);
            console.log('editingProduit', $rootScope.editingProduit);
            $rootScope.getSousCategories($rootScope.editingProduit.idCategorie);    // Appel de la fonction depuis prodCtrl
            modalEditProduit.modal('show');
        }

        function updateProduitImport(produit) {
            console.log('produit', produit);
            //Vérification des champs obligatoires
            if (!produit){
                produit = {};
                $scope.produit = {};
            }
            //console.log('produit', produit);
            if (!produit.hasOwnProperty('referenceProduit') || produit.referenceProduit === ''){
                toastr.error('Veuillez renseigner la référence du produit !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('libelle') || produit.libelle === ''){
                toastr.error('Veuillez renseigner le libellé du produit !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('quantite') || produit.quantite === '' || produit.quantite < 1){
                toastr.error('Veuillez renseigner la quantité ou elle est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('idMarque') || produit.idMarque <= 0){
                toastr.error('Veuillez renseigner la marque ou elle est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('idCategorie') || produit.idCategorie <= 0){
                toastr.error('Veuillez renseigner la catégorie ou elle est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('idSousCategorie') || produit.idSousCategorie <= 0){
                toastr.error('Veuillez renseigner la sous catégorie ou elle est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('prix') || produit.prix === '' || produit.prix <= 0){
                toastr.error('Veuillez renseigner le prix ou il est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('prixMin') || produit.prixMin === '' || produit.prixMin <= 0){
                toastr.error('Veuillez renseigner le prix minimum ou il est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('prixMax') || produit.prixMax === '' || produit.prixMax <= 0){
                toastr.error('Veuillez renseigner le prix maximum ou il est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('size') || produit.size === '' || produit.size <= 0){
                toastr.error('Veuillez renseigner la taille !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('poids') || produit.poids === '' || produit.poids <= 0){
                toastr.error('Veuillez renseigner le poids ou il est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('couleur') || produit.couleur === ''){
                toastr.error('Veuillez renseigner la couleur !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('description') || produit.description === ''){
                toastr.error('Veuillez renseigner la description du produit !', 'Erreur');
                return;
            }
            //*/
            if (!$scope.documentsImport || _.size($scope.documentsImport) === 0){
                toastr.error('Veuillez renseigner au moins une image !', 'Erreur');
                return;
            }
            produit.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;
            produit.listImageProduit = _.map($scope.documentsImport, function (img) {
                return {
                    image: _.split(img.data, ',')[1],
                    type: _.split(img.type, '/')[1],
                    extImg: _.split(img.type, '/')[1]
                }
            });
            console.log('produit', produit);
            console.log('$scope.documents', $scope.documentsImport);
            //*/
            //return;

            ProduitService.updateProduit(produit)
                .then(function (res) {
                    if (!res.data.hasError){
                        //$scope.documents = [];
                        //$('#categorie').addClass('col-md-12');
                        //$scope.editingProduit = {};
                        //$scope.documents = undefined;
                        //$state.go('app.produits');
                        getImportProduits();
                        notificationService.flatty.success('<b><i class="fa fa-check"></i>&nbsp;Votre produit a bien été mis à jour !</b>');
                        console.log('res update produit', res);
                        modalEditProduit.modal('hide');
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err update produit', err);
                });
        }

        function updateProduit(produit) {
            console.log('produit', produit);
            //Vérification des champs obligatoires
            if (!produit){
                produit = {};
                $scope.produit = {};
            }
            //console.log('produit', produit);
            if (!produit.hasOwnProperty('referenceProduit') || produit.referenceProduit === ''){
                toastr.error('Veuillez renseigner la référence du produit !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('libelle') || produit.libelle === ''){
                toastr.error('Veuillez renseigner le libellé du produit !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('quantite') || produit.quantite === '' || produit.quantite < 1){
                toastr.error('Veuillez renseigner la quantité ou elle est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('idMarque') || produit.idMarque <= 0){
                toastr.error('Veuillez renseigner la marque ou elle est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('idCategorie') || produit.idCategorie <= 0){
                toastr.error('Veuillez renseigner la catégorie ou elle est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('idSousCategorie') || produit.idSousCategorie <= 0){
                toastr.error('Veuillez renseigner la sous catégorie ou elle est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('prix') || produit.prix === '' || produit.prix <= 0){
                toastr.error('Veuillez renseigner le prix ou il est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('prixMin') || produit.prixMin === '' || produit.prixMin <= 0){
                toastr.error('Veuillez renseigner le prix minimum ou il est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('prixMax') || produit.prixMax === '' || produit.prixMax <= 0){
                toastr.error('Veuillez renseigner le prix maximum ou il est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('size') || produit.size === '' || produit.size <= 0){
                toastr.error('Veuillez renseigner la taille !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('poids') || produit.poids === '' || produit.poids <= 0){
                toastr.error('Veuillez renseigner le poids ou il est invalide !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('couleur') || produit.couleur === ''){
                toastr.error('Veuillez renseigner la couleur !', 'Erreur');
                return;
            }
            if (!produit.hasOwnProperty('description') || produit.description === ''){
                toastr.error('Veuillez renseigner la description du produit !', 'Erreur');
                return;
            }
            //*/
            if (!$scope.documents || _.size($scope.documents) === 0){
                toastr.error('Veuillez renseigner au moins une image !', 'Erreur');
                return;
            }
            produit.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;
            produit.listImageProduit = _.map($scope.documents, function (img) {
                return {
                    image: _.split(img.data, ',')[1],
                    type: _.split(img.type, '/')[1],
                    extImg: _.split(img.type, '/')[1]
                }
            });
            console.log('produit', produit);
            console.log('$scope.documents', $scope.documents);
            //*/
            //return;

            ProduitService.updateProduit(produit)
                .then(function (res) {
                    if (!res.data.hasError){
                        //$scope.documents = [];
                        //$('#categorie').addClass('col-md-12');
                        //$scope.editingProduit = {};
                        //$scope.documents = undefined;
                        //$state.go('app.produits');
                        $rootScope.getProduits();
                        notificationService.flatty.success('<b><i class="fa fa-check"></i>&nbsp;Votre produit a bien été mis à jour !</b>');
                        console.log('res update produit', res);
                        modalEditProduit.modal('hide');
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err update produit', err);
                });
        }

        function reinitProduit(produit) {
            swal({
                title: 'Voulez-vous vraiment réinitialser les informations saisies ?',
                type: 'info',
                showCancelButton: true,
                confirmButtonColor: brandSuccess,
                confirmButtonText: 'Oui, Réinitialser',
                cancelButtonText: 'Non, Annuler',
                closeOnConfirm: false
            }).then((result) => {
                if (result.value) {
                    console.log('produit', produit);
                    $scope.produit = {};
                    $scope.documents = [];
                    $('#categorie').addClass('col-md-12');
                    // result.dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                } else if (result.dismiss === 'cancel') {

                }
            });
        }

        function importerCatalogProduit(file, errFiles, criteria) {

            console.log('file', file);
            console.log('errFiles', errFiles);
            console.log('criteria', criteria);

            if (!criteria){
                criteria = {};
            }

            if (!criteria.hasOwnProperty('idMarque') || criteria.idMarque === ''){
                toastr.error('Veuillez renseigner la marque du produit !', 'Erreur');
                return;
            }
            if (!criteria.hasOwnProperty('idCategorie') || criteria.idCategorie === ''){
                toastr.error('Veuillez renseigner la catégorie du produit !', 'Erreur');
                return;
            }
            if (!criteria.hasOwnProperty('idSousCategorie') || criteria.idSousCategorie === ''){
                toastr.error('Veuillez renseigner la sous catégorie du produit !', 'Erreur');
                return;
            }

            if (file && !file.name.endsWith('.xlsx')){
                toastr.error('Erreur fichier - Fichier excel non valide !', 'Erreur');
                return;
            }

            if (!file){
                toastr.error('Veuillez renseigner un fichier excel !', 'Erreur');
                return;
            }

            if (errFiles !== null){
                toastr.error('Taille du fichier trop grand - Autorisée: 1MB', 'Erreur');
                return;
            }

            if (errFiles && errFiles.length !== 0){
                toastr.error('Taille du fichier trop grand - Autorisée: 1MB', 'Erreur');
                return;
            }

            //$scope.uploading = true;

            ProduitService.importProduits(file, criteria)
                .then(function (res) {
                    console.log('upload res', res);
                    if (!res.data.hasError){
                        $scope.fileToImport = undefined;
                        $scope.criteria = undefined;
                        toastr.success("Importation effectuée avec succès !", 'Succès');
                        $scope.getImportProduits();
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('upload err', err);
                }, function (evt) {
                    // Math.min is to fix IE which reports 200% sometimes
                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                    $scope.progress = file.progress;
                    console.log('upload progress', $scope.progress);
                });
        }
        
        function getImportProduits(index, size) {
            ProduitService.getImportProduits(index, size)
                .then(function (res) {
                    console.log('import produits res', res);
                    if (!res.data.hasError){
                        $scope.produits = res.data.itemsProduit;
                        $scope.totalProduit = res.data.count;
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('import produits err', err);
                });
        }
    }
})();