(function () {
    'use strict';

    angular.module('app.espace-publicitaire')
        .factory('EspacePubService', EspacePubService);

    EspacePubService.$inject = ['$rootScope', '$http', 'Upload'];
    function EspacePubService($rootScope, $http, Upload) {
        const espacePubService = {};

        espacePubService.exporterCSV = exporterCSV;
        espacePubService.importPublication = importPublication;
        espacePubService.getFullPublications = getFullPublications;
        espacePubService.getLessPublications = getLessPublications;
        espacePubService.searchPublications = searchPublications;
        espacePubService.updateTypePublications = updateTypePublications;
        espacePubService.getTypePublication = getTypePublication;
        espacePubService.updatePublication = updatePublication;
        espacePubService.createPaypalPaiement = createPaypalPaiement;
        espacePubService.completePaypalPaiement = completePaypalPaiement;
        espacePubService.getCompteInfo = getCompteInfo;
        espacePubService.getTransactions = getTransactions;
        espacePubService.searchTransactions = searchTransactions;
        espacePubService.convertCurrency = convertCurrency;

        return espacePubService;

        function exporterCSV() {

        }

        function searchPublications(criteria, index, size) {
            let request = {
                dataPublication: criteria,
                index: index || 0,
                size: size || 5
            };
            let URL = BASE_URL + '/publication/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }

        function getLessPublications(index, size) {
            let request = {
                dataPublication:{
                    idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel,
                    isValid: 0      //On recupère que les publications en attente

                },
                index: index || 0,
                size: size || 5
            };
            let URL = BASE_URL + '/publication/getLessByCriteria';
            return $http.post(URL, request, {cache: true});
        }

        function getFullPublications(index, size, isReady) {
            let request = {
                dataPublication:{
                    idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel,
                    isReady: isReady || 0

                },
                index: index || 0,
                size: size || 5
            };
            let URL = BASE_URL + '/publication/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }

        function getTypePublication(id, index, size) {
            let request = {
                dataTypePublication:{
                    idPublication: id
                },
                index: index || 0,
                size: size || 6
            };
            let URL = BASE_URL + '/typePublication/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }

        function importPublication(files) {
            console.log('filesToSend', files);
            let URL = BASE_URL + '/publication/importPublication';

            return Upload.upload({
                url: URL,
                data: {
                    video: files[0] || new Blob([""]),
                    image: files[1] || new Blob([""]),
                    audio: files[2] || new Blob([""]),
                    texte: files[3] || '',
                    texteSize: files[4] || '',
                    arrayKey: '',
                    chanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel
                }
            });
        }

        function updatePublication(pub) {
            let request = {
                datasPublication: [pub]
            };
            let URL = BASE_URL + '/publication/update';
            return $http.post(URL, request, {cache: true});
        }

        function updateTypePublications(typePub) {
            let request = {
                datasTypePublication: [typePub]
            };
            let URL = BASE_URL + '/typePublication/update';
            return $http.post(URL, request, {cache: true});
        }

        function createPaypalPaiement(amount) {
            let request = {
                dataPayment: {
                    sum: amount
                }
            };
            let URL = BASE_URL + '/paypal/make/payment';
            return $http.post(URL, request, {cache: true});
        }

        function completePaypalPaiement(payment) {
            let request = {
                dataPayment: {
                    paymentId: payment.paymentId || '',
                    payerID: payment.payerID || '',
                    idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel
                }
            };
            let URL = BASE_URL + '/paypal/complete/payment';
            return $http.post(URL, request, {cache: true});
        }

        function getTransactions(index, size) {
            let request = {
                dataTransactionEspacePub:{
                    idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel
                },
                index: index || 0,
                size: size || 5
            };
            let URL = BASE_URL + '/transactionEspacePub/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }


        function searchTransactions(criteria, index, size) {
            let request = {
                dataTransactionEspacePub: criteria,
                index: index || 0,
                size: size || 5
            };
            let URL = BASE_URL + '/transactionEspacePub/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }

        function getCompteInfo() {
            let request = {
                dataCompteEspacePub:{
                    idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel
                }
            };
            let URL = BASE_URL + '/compteEspacePub/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }

        function convertCurrency(amount) {

            let URL = 'https://www.xe.com/fr/currencyconverter/convert/?Amount='+amount+'&From=XOF&To=EUR';
            return $http.get(URL, {cache: true});
        }
    }
})();