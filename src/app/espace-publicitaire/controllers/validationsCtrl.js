(function () {
    'use strict';

    angular.module('app.espace-publicitaire')
        .controller('validationsCtrl', validationsCtrl);

    validationsCtrl.$inject = ['$rootScope', '$scope', 'EspacePubService', 'toastr', 'notificationService'];
    function validationsCtrl($rootScope, $scope, EspacePubService, toastr, notificationService) {

        $scope.getFullPublications = getFullPublications;
        getFullPublications();
        $scope.searchPublications = searchPublications;
        $rootScope.decisionTypePublication = decisionTypePublication;
        $scope.showModalRejectPub = showModalRejectPub;
        $scope.loadTypePublication = loadTypePublication;


        $rootScope.typePublication = {};
        $scope.disableButton = undefined;

        $scope.currentPage = 1;
        $scope.currentPageS = 1;
        $scope.sizePublication = 5;
        $scope.max_size = 3;

        // Configurations date-range-picker-----------------------
        $scope.datePicker = {
            startDate: moment().subtract(1, "days"),
            endDate: moment()
        };

        $scope.options = {
            applyClass: 'btn-success',
            cancelClass: 'btn-secondary',
            opens: 'left',
            //timePicker: true,
            //timePicker24Hour: true,
            autoApply: false,
            //timePickerSeconds: true,
            locale: {
                applyLabel: "Choisir",
                fromLabel: "De",
                //format: "YYYY-MM-DD", //will give you 2017-01-06
                //format: "D-MMM-YY", //will give you 6-Jan-17
                format: "DD MMMM YYYY", //will give you 6 January 2017
                toLabel: "à",
                cancelLabel: 'Annuler',
                customRangeLabel: 'Définir intervalle'
            },
            ranges: {
                'Aujourd\'hui': [moment(), moment()],
                'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 derniers jours': [moment().subtract(6, 'days'), moment()],
                '30 derniers jours': [moment().subtract(29, 'days'), moment()],
                'Ce mois': [moment().startOf('month'), moment().endOf('month')],
                'Le mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };
        // Fin de la configuration--------------------------------


        $scope.searching = false;

        let modalMotifRejet = $('#motifRejet');

        function searchPublications(criteria, index, size) {
            console.log('startDate', $scope.datePicker.startDate.format('DD-MM-YYYY'));
            console.log('endDate', $scope.datePicker.endDate.format('DD-MM-YYYY'));
            $scope.searching = true;

            let dataCriteria = {};

            dataCriteria.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;
            //dataCriteria.isValid = 0;
            dataCriteria.startDate = $scope.datePicker.startDate.format('DD-MM-YYYY');
            dataCriteria.endDate = $scope.datePicker.endDate.format('DD-MM-YYYY');
            dataCriteria.isReady = 0;

            EspacePubService.searchPublications(dataCriteria, index, size)
                .then(function (res) {
                    console.log('res search publications', res);
                    if (!res.data.hasError){
                        $scope.publications = res.data.itemsPublication;
                        $scope.totalPublications = res.data.count;
                        if (res.data.count === 0 && !criteria.libelle){
                            toastr.info('Aucune publication trouvée pour cette recherche !', 'Information');
                        }
                        console.log('totalPublication', res.data.count);
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err search publications', err);
                });
        }

        function getFullPublications(index, size) {

            console.log('getPublications index', index);
            console.log('getPublications size', size);

            EspacePubService.getFullPublications(index, size)
                .then(function (res) {
                    console.log('getPublications res', res);
                    if (!res.data.hasError){
                        $scope.publications = res.data.itemsPublication;
                        $scope.totalPublications = res.data.count;
                    }else {
                        console.log('Erreur', res.data.status.message);
                    }
                }, function (err) {
                    console.log('getPublications err', err);
                });
        }

        function loadTypePublication(idPub, index, size) {

            EspacePubService.getTypePublication(idPub, index, size)
                .then(function (res) {
                    console.log('getTypePublication res', res);
                    if (!res.data.hasError){
                        $scope.typePublications = res.data.itemsTypePublication;
                        $scope.totalTypePublications = res.data.count;
                    }else {
                        console.log('Erreur', res.data.status.message);
                    }
                }, function (err) {
                    console.log('getTypePublication err', err);
                });
        }

        function decisionTypePublication(id, typePub, decision) {

            console.log('typePub', typePub);
            console.log('decision', decision);

            let tp = {};

            switch (decision) {

                case 'accept':
                    tp.isValid = 1;   //Validé
                    tp.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;
                    tp.idPublication = typePub.idPublication;
                    tp.id = typePub.id;
                    break;

                case 'reject':
                    if (!typePub.hasOwnProperty('motifRejet')){
                        toastr.error('Veuillez indiquer un motif pour le rejet svp !');
                        return;
                    }
                    if (typePub.motifRejet === ''){
                        toastr.error('Le motif n\'est pas renseigné !', 'Erreur', {
                            positionClass: 'toast-top-right'
                        });
                        return;
                    }
                    tp.isValid = 0;
                    tp.motifRejet = typePub.motifRejet;
                    tp.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;
                    tp.idPublication = typePub.tp.idPublication;
                    tp.id = typePub.tp.id;
                    break;
            }

            console.log('typePub', tp);

            //return;
            EspacePubService.updateTypePublications(tp)
                .then(function (res) {
                    console.log('res typePub update', res);
                    if (!res.data.hasError){
                        if (res.data.itemsTypePublication[0]){
                            if (res.data.itemsTypePublication[0].isValid === true){
                                notificationService.flatty.success('Le type de publication a été validé !');
                                $scope.disableButton = true;
                            }
                            if (res.data.itemsTypePublication[0].isValid === false){
                                notificationService.flatty.error('Le type de publication a été rejeté !');
                                $scope.disableButton = false;
                            }
                        }
                        $('[data-toggle="tooltip"]').tooltip('hide');
                        loadTypePublication(id);
                        //getFullPublications();
                        modalMotifRejet.modal('hide');
                        //$scope.disableButton = !$scope.disableButton;

                    }else {
                        notificationService.flatty.error('Erreur interne\nLa validation a échoué !');
                    }
                }, function (err) {
                    console.log('err typePub update', err);
                });
            
        }

        function showModalRejectPub(id, tp) {
            $rootScope.typePublication.tp = tp;
            $rootScope.typePublication.id = id;
            modalMotifRejet.modal('show');
        }
    }
})();