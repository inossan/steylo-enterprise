(function () {
    'use strict';

    angular.module('app.espace-publicitaire')
        .controller('compteCtrl', compteCtrl);

    compteCtrl.$inject = ['$rootScope', '$scope', 'EspacePubService', '$state', '$window', 'toastr', '$location'];
    function compteCtrl($rootScope, $scope, EspacePubService, $state, $window, toastr, $location) {

        $scope.exporterCsv = exporterCsv;
        $scope.getHistoAchatCredit = getHistoAchatCredit;
        $scope.searchTransactions = searchTransactions;
        $scope.getSolde = getSolde;
        $scope.showModalAchatCredit = showModalAchatCredit;
        $rootScope.initPayPalPayment = initPayPalPayment;
        $rootScope.completePayPalPayment = completePayPalPayment;

        getHistoAchatCredit();
        getSolde();

        $scope.currentPage = 1;
        $scope.currentPageS = 1;
        $scope.sizeTransaction = 5;
        $scope.max_size = 3;

        const achatCredit = $('#achat-credit');

        console.log('$location.url', $location.search());
        if ($location.search().token) { //Si au moins un champ est renseigné
            let payment = {
                paymentId: $location.search().paymentId,
                payerID: $location.search().PayerID
            };
            completePayPalPayment(payment);
        }

        // Configurations date-range-picker-----------------------
        $scope.datePicker = {
            startDate: moment().subtract(1, "days"),
            endDate: moment()
        };

        $scope.options = {
            applyClass: 'btn-success',
            cancelClass: 'btn-secondary',
            opens: 'center',
            //timePicker: true,
            //timePicker24Hour: true,
            autoApply: true,
            //timePickerSeconds: true,
            locale: {
                applyLabel: "Choisir",
                fromLabel: "De",
                //format: "YYYY-MM-DD", //will give you 2017-01-06
                //format: "D-MMM-YY", //will give you 6-Jan-17
                format: "DD MMMM YYYY", //will give you 6 January 2017
                toLabel: "à",
                cancelLabel: 'Annuler',
                customRangeLabel: 'Définir intervalle'
            },
            ranges: {
                'Aujourd\'hui': [moment(), moment()],
                'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 derniers jours': [moment().subtract(6, 'days'), moment()],
                '30 derniers jours': [moment().subtract(29, 'days'), moment()],
                'Ce mois': [moment().startOf('month'), moment().endOf('month')],
                'Le mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };
        // Fin de la configuration--------------------------------



        // ------------------------------------Les fonctions------------------------------------

        function exporterCsv() {

            console.log('exporterCsv');

            return;

            EspacePubService.exporterCSV()
                .then(function (res) {
                    console.log('res achat export csv', res);
                    if (!res.data.hasError){
                        let file = new Blob([res.data], { type: 'text/csv' });
                        if (file.size === 0){
                            toastr.info('Aucune donnée à exporter !', 'Information');
                            return;
                        }
                        saveAs(file, 'Historique-Achat');
                        toastr.success('Exportation effectuée avec succès!', 'Succès');
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err achat export csv', err);
                });
        }

        $scope.searching = false;

        function searchTransactions(criteria, index, size) {
            console.log('startDate', criteria.startDate.format('DD-MM-YYYY'));
            console.log('endDate', criteria.endDate.format('DD-MM-YYYY'));
            $scope.searching = true;

            let dataCriteria = {};

            dataCriteria.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;
            dataCriteria.startDate = criteria.startDate.format('DD-MM-YYYY');
            dataCriteria.endDate = criteria.endDate.format('DD-MM-YYYY');

            EspacePubService.searchTransactions(dataCriteria, index, size)
                .then(function (res) {
                    console.log('res searchTransactions', res);
                    if (!res.data.hasError){
                        $scope.transactions = res.data.itemsTransactionEspacePub;
                        $scope.totalTransaction = res.data.count || 0;
                        if (res.data.count === 0 && !criteria.libelle){
                            toastr.info('Aucune transaction trouvée pour cette recherche !', 'Information');
                        }
                        console.log('totalPublication', res.data.count);
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err searchTransactions', err);
                });
        }

        function getHistoAchatCredit(index, size) {
            EspacePubService.getTransactions(index, size)
                .then(function (res) {
                    console.log('res getHistoAchatCredit', res);
                    if (!res.data.hasError){
                        $scope.transactions = res.data.itemsTransactionEspacePub || [];
                        $scope.totalTransaction = res.data.count || 0;
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err getHistoAchatCredit', err);
                });
        }

        function getSolde() {
            EspacePubService.getCompteInfo()
                .then(function (res) {
                    console.log('res getSolde', res);
                    if (!res.data.hasError){
                        $scope.solde = res.data.itemsCompteEspacePub[0].solde || undefined;  //s'il n'a pas de compte, afficher un bouton pour en créer
                    }else {
                        console.log('Erreur getSolde', res.data.status.message);
                    }
                }, function (err) {
                    console.log('err getSolde', err);
                });
        }

        function showModalAchatCredit() {
            achatCredit.modal('show');
        }

        function doConversion(amount) {

            EspacePubService.convertCurrency(amount)
                .then(function (res) {
                    console.log('res doConversion', res);

                }, function (err) {
                    console.log('err doConversion', err);
                });
        }

        function initPayPalPayment(choix) {
            console.log("choix", choix);

            if (!choix) {
                toastr.error('Veuillez selectionner un forfait svp !', 'Erreur');
            }

            let amount;

            switch (choix) {
                case 'bronze':
                    amount = $rootScope.choice.BRONZE;
                    break;

                case 'argent':
                    amount = $rootScope.choice.ARGENT;
                    break;

                case 'or':
                    amount = $rootScope.choice.OR;
                    break;

                default:
                    amount = 0;
                    break;
            }



            console.log("amount", amount);
            //convertir avant initiation du paiement

            let amountConverted = Math.ceil(amount/TAUX_CHANGE);   //En euro
            console.log("amountConverted", amountConverted);

            EspacePubService.createPaypalPaiement(amountConverted)
                .then(function (res) {
                    console.log('res initPaPaylPayment', res);
                    if (!res.data.hasError){
                        $window.open(res.data.itemPayment.redirect_url, '_self');
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err initPaPaylPayment', err);
                });
        }

        function completePayPalPayment(payment) {
            EspacePubService.completePaypalPaiement(payment)
                .then(function (res) {
                    console.log('res completePaPaylPayment', res);
                    if (!res.data.hasError){
                        getHistoAchatCredit();
                        getSolde();
                        $state.reload();
                        toastr.success('Paiement effectué avec succès !', 'Succès');
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err completePaPaylPayment', err);
                });
        }
    }
})();