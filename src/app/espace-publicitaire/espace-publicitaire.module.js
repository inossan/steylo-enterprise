(function () {
    'use strict';

    angular.module('app.espace-publicitaire', [
        'daterangepicker',
        'ngFileUpload'
    ])
        .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {

            $ocLazyLoadProvider.config({
                // Set to true if you want to see what and when is dynamically loaded
                debug: true
            });

            $urlRouterProvider.otherwise('/login');
            $stateProvider
                .state('app.espace-publicitaire', {
                    url: '/espace-pub',
                    templateUrl: 'app/espace-publicitaire/views/espace-publicitaire.html',
                    ncyBreadcrumb: {
                        label: 'Espace publicitaire'
                    },
                    controller: 'espPubCtrl',
                    resolve: {
                        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                            // you can lazy load controllers
                            return $ocLazyLoad.load({
                                files: [
                                    'app/espace-publicitaire/controllers/espacePublicitaireCtrl.js',
                                    'app/espace-publicitaire/controllers/compteCtrl.js',
                                    'app/espace-publicitaire/controllers/historiqueCtrl.js',
                                    'app/espace-publicitaire/controllers/publicationsCtrl.js',
                                    'app/espace-publicitaire/controllers/validationsCtrl.js',
                                    'app/espace-publicitaire/controllers/statistiquesCtrl.js',
                                    'app/espace-publicitaire/services/espacePub.service.js'
                                ]
                            });
                        }]
                    },
                    redirectTo: 'app.espace-publicitaire.compte'
                })
                .state('app.espace-publicitaire.compte', {
                    url: '/compte',
                    ncyBreadcrumb: {
                        label: 'Compte'
                    },
                    views: {
                        'esp_pub_sub_menu': {
                            templateUrl: 'app/espace-publicitaire/views/compte/compte.html',
                            controller: 'compteCtrl'
                        },
                    },
                })
                .state('app.espace-publicitaire.publications', {
                    url: '/publications',
                    ncyBreadcrumb: {
                        label: 'Publications'
                    },
                    views: {
                        'esp_pub_sub_menu': {
                            templateUrl: 'app/espace-publicitaire/views/publications/publications.html',
                            controller: 'publicationsCtrl'
                        },
                    },
                })
                .state('app.espace-publicitaire.validations', {
                    url: '/validations',
                    ncyBreadcrumb: {
                        label: 'Validations'
                    },
                    views: {
                        'esp_pub_sub_menu': {
                            templateUrl: 'app/espace-publicitaire/views/validations/validations.html',
                            controller: 'validationsCtrl'
                        },
                    },
                })
                .state('app.espace-publicitaire.historique', {
                    url: '/historique',
                    ncyBreadcrumb: {
                        label: 'Historique'
                    },
                    views: {
                        'esp_pub_sub_menu': {
                            templateUrl: 'app/espace-publicitaire/views/historique/historique.html',
                            controller: 'historiqueCtrl'
                        },
                    },
                })
                .state('app.espace-publicitaire.statistiques', {
                    url: '/statistiques',
                    ncyBreadcrumb: {
                        label: 'Statistiques'
                    },
                    views: {
                        'esp_pub_sub_menu': {
                            templateUrl: 'app/espace-publicitaire/views/statistiques/statistiques.html',
                            controller: 'statistiquesCtrl'
                        },
                    },
                })
        }])
})();