(function () {
    'use strict';

    angular.module('app.ecommerciaux')
        .controller('ecomGeoLocaliteCtrl', ecomGeoLocaliteCtrl);

    ecomGeoLocaliteCtrl.$inject = ['$rootScope', '$scope', 'AclService'];
    function ecomGeoLocaliteCtrl($rootScope, $scope, AclService) {

        $scope.can = AclService.can;
    }
})();