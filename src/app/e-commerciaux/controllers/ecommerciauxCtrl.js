(function () {
    'use strict';

    angular.module('app.ecommerciaux')
        .controller('ecomCtrl', ecomCtrl);

    ecomCtrl.$inject = ['$rootScope', '$scope', 'demandeAbonneService', '$stateParams', 'AclService'];
    function ecomCtrl($rootScope, $scope, demandeAbonneService, $stateParams, AclService) {

        //Configuration du tabs-----------------------------
        /*/
        $scope.tabs = {
            active: 0
        };
        $scope.pages = {
            active: 0
        };
        $scope.horizontalTabs = {
            active: 0
        };
        $scope.pages = [
            {
                id: 1,
                title: 'Demandes',
                badge: true //Pour afficher le nombre de demande  dans un badge uniquement pour cet onglet
            },
            {
                id: 2,
                title: 'Géolocalisation',
                content: ''
            }
        ];
        //*/
        //Fin de la configuration du tabs-------------------

        $scope.can = AclService.can;

        $scope.windowHeightC = $(window).innerHeight() - 280;
        $scope.windowHeightCS = $(window).innerHeight() - 350;

        $scope.currentPageS = 1;
        $scope.currentPage = 1;
        $scope.sizeDemande = 5;
        $scope.max_size = 3;
        $scope.search = false;

        console.log('$stateParams EC', $stateParams);

        $scope.badgeTabDemande = 'badge-warning'; //On choisit le badge 'en_attente' par défaut
        $scope.statutTabDemande = 'en attente'; //On choisit le badge 'en_attente' par défaut
        $scope.subview = 'by-etat-demandeurs'; //On choisit l'onglet 'ajout-produit' par défaut
        $rootScope.statutDemande = statut_demande.EN_ATTENTE;   //Nous permet de masquer les boutons "valider" et "rejeter"
        $scope.criteria = {
            demandeState: statut_demande.EN_ATTENTE
        };

        $scope.getStatusAbonne = getStatusAbonne;
        $scope.getStatusBadge = getStatusBadge;
        $scope.getStatusInfo = getStatusInfo;
        $scope.getStatusAlert = getStatusAlert;
        $scope.voirInfoDemandeur = voirInfoDemandeur;
        $scope.searchDemandeur = searchDemandeur;
        $scope.selectLigneDemandeur = selectLigneDemandeur;

        //Les modaux
        $rootScope.modalInfoAbonne = $('#abonneInfo');

        function selectLigneDemandeur(selected) {
            $scope.selectedDemandeur = selected;
        }

        function getInfoAbonne(id) {
            demandeAbonneService.getAbonne(id)
                .then(function (res) {
                    if (!res.data.hasError){
                        $rootScope.abonneInfo = res.data.itemsAbonne[0];
                        if ($rootScope.abonneInfo.photoIdentite){
                            switch ($rootScope.abonneInfo.extPhotoIdentite){
                                case 'png':
                                    $rootScope.abonneInfo.photoIdentite =  'data:image/png;base64,' + $rootScope.abonneInfo.photoIdentite;
                                    break;
                                case 'jpeg':
                                    $rootScope.abonneInfo.photoIdentite =  'data:image/jpeg;base64,' + $rootScope.abonneInfo.photoIdentite;
                                    break;
                                case 'jpg':
                                    $rootScope.abonneInfo.photoIdentite =  'data:image/jpg;base64,' + $rootScope.abonneInfo.photoIdentite;
                                    break;
                                case 'gif':
                                    $rootScope.abonneInfo.photoIdentite =  'data:image/gif;base64,' + $rootScope.abonneInfo.photoIdentite;
                                    break;
                            }
                        }
                        console.log('$rootScope.abonneInfo', $rootScope.abonneInfo);

                        //Ouverture du popup du détails
                        $rootScope.modalInfoAbonne.modal('show')
                            .on('shown.bs.modal', function () {
                                let marker;
                                let center = new google.maps.LatLng(parseFloat($rootScope.abonneInfo.latitude), parseFloat($rootScope.abonneInfo.longitude));
                                let map = new google.maps.Map(document.getElementById('mapSize'), {
                                    zoom: 15,
                                    center: center,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                                    //styles: [{"stylers": [{ "saturation": 100 }]}],
                                    /*/
                                    zoomControl: boolean,
                                    mapTypeControl: boolean,
                                    scaleControl: boolean,
                                    rotateControl: boolean,//*/
                                    streetViewControl: false,
                                    fullscreenControl: false

                                });
                                let iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
                                /*let infowindow = new google.maps.InfoWindow({
                                    content: contentString
                                });*/

                                marker = new google.maps.Marker({
                                    map: map,
                                    //draggable: true,
                                    animation: google.maps.Animation.DROP,
                                    position: center,
                                    //label: $scope.abonneInfo.nom[0],
                                    icon: {
                                        //url: $scope.abonneInfo.photoIdentite ? $scope.abonneInfo.photoIdentite : undefined,
                                        scaledSize: new google.maps.Size(32, 32)
                                    }
                                    //position: {lat: parseFloat(latitude), lng: parseFloat(longitude)}
                                });
                                marker.addListener('click', toggleBounce);

                                function toggleBounce() {
                                    if (marker.getAnimation() !== null) {
                                        marker.setAnimation(null);
                                    } else {
                                        marker.setAnimation(google.maps.Animation.BOUNCE);
                                    }
                                    //infowindow.open(map, marker);
                                }

                                console.log('ok');
                                google.maps.event.trigger(map, "resize");
                                return map.setCenter(center);
                        });
                    }
                });
        }

        //Les fonctions---------------------------------------------

        $scope.getDemandeurByStatut = function (d) {
            // $scope.badgeTabDemande (pour connaitre la catégorie dans laquelle nous sommes - concernant les demandes)
            // $scope.statutTabDemande (pour spécifier le nom correct en fonction du statut - concernant les demandes)
            $scope.criteria = {};
            switch (d){
                case 'en_attente':
                    $scope.badgeTabDemande = 'badge-warning';
                    $scope.statutTabDemande = 'en attente';
                    $rootScope.statutDemande = statut_demande.EN_ATTENTE;
                    $scope.criteria.demandeState = statut_demande.EN_ATTENTE;
                    $scope.getDemandeAbonne($scope.statutDemande);
                    break;
                case 'valide':
                    $scope.badgeTabDemande = 'badge-success';
                    $scope.statutTabDemande = 'validée(s)';
                    $rootScope.statutDemande = statut_demande.VALIDE;
                    $scope.criteria.demandeState = statut_demande.VALIDE;
                    $scope.getDemandeAbonne($scope.statutDemande);
                    break;
                case 'rejete':
                    $scope.badgeTabDemande = 'badge-danger';
                    $scope.statutTabDemande = 'rejetée(s)';
                    $rootScope.statutDemande = statut_demande.REJETE;
                    $scope.criteria.demandeState = statut_demande.REJETE;
                    $scope.getDemandeAbonne($scope.statutDemande);
                    break;
            }
        };

        $rootScope.getDemandeAbonne = function (demandeState = 0, index = 0, size = 5) {
            console.log('demandeState', demandeState);
            console.log('index', index);
            console.log('size', size);
            demandeAbonneService.getAbonneDemande(demandeState, index, size)
                .then(function (res) {
                    console.log('response abonné', res);
                    if (!res.data.hasError){
                        if (res.data.count === 0){
                            console.log('res.data.count', res.data.count);
                            $scope.demandesAbonne = [];
                        }else {
                            $scope.demandesAbonne = []; //Recoit la liste de toutes les demandes
                            $scope.demandesAbonneTemp = res.data.itemsAbonneDemande;
                            _.forEach($scope.demandesAbonneTemp, function (ad) {  //On reconverti les photos des demandeurs
                                //ad.loadSpinAccept = false;
                                //ad.loadSpinReject = false;
                                if (ad.photoAbonne){
                                    ad.photoAbonne = getImageFromBase64(ad)
                                }
                                $scope.demandesAbonne.push(ad);
                                console.log('demandesAbonne', $scope.demandesAbonne);
                            });
                        }
                        $scope.totalDemandeAbonne = res.data.count;
                        console.log('totalDemandeAbonne', $scope.totalDemandeAbonne);
                    }
                });
        };
        $rootScope.getDemandeAbonne();
        
        function getStatusAbonne(demande) {
            switch (demande.demandeState){
                case 0:
                    return 'En attente';
                case 1:
                    return 'Validé';
                case 3:
                    return 'Rejeté';
            }
        }

        function getStatusAlert(demande) {
            if (!demande){
                return;
            }
            switch (demande.demandeState){
                case 0:
                    return {
                        status: 'alert-warning',
                        message: 'Sa demande est en attente',
                        icon: 'icon-user-follow'
                    };
                case 1:
                    return {
                        status: 'alert-success',
                        message: 'Sa demande a été validée',
                        icon: 'icon-user-following'
                    };
                case 3:
                    return {
                        status: 'alert-danger',
                        message: 'Sa demande a été rejetée',
                        icon: 'icon-user-unfollow'
                    };
            }
        }

        function getStatusBadge(demande) {
            if (!demande){
                return;
            }
            switch (demande.demandeState){
                case 0:
                    return 'badge-warning';
                case 1:
                    return 'badge-success';
                case 3:
                    return 'badge-danger';
            }
        }

        function getStatusInfo(demande) {
            if (!demande){
                return;
            }
            switch (demande.demandeState){
                case 0:
                    return 'border-warning';
                case 1:
                    return 'border-success';
                case 3:
                    return 'border-danger';
            }
        }

        function voirInfoDemandeur(a) {
            if (a){
                getInfoAbonne(a.idAbonne);
                $rootScope.demandeur = angular.copy(a);
                console.log('demandeur', $rootScope.demandeur);
            }
        }

        //Revoir comment cette function pourrait etre factorisée et la centraliser dans un fichier commun à tous les modules
        function getImageFromBase64(img) {
            switch (img.extPhotoAbonne){
                case 'png':
                    return 'data:image/png;base64,' + img.photoAbonne;
                case 'jpeg':
                    return 'data:image/jpeg;base64,' + img.photoAbonne;
                case 'jpg':
                    return 'data:image/jpg;base64,' + img.photoAbonne;
                case 'gif':
                    return 'data:image/gif;base64,' + img.photoAbonne;
            }
        }

        function searchDemandeur(criteria, index, size) {
            $scope.search = (!(criteria.nomAbonne === undefined || criteria.nomAbonne === ''));
            criteria.idChannel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;

            console.log('criteria', criteria);

            //Si le champ de recherche est vide on passe au mode normal
            if ($scope.search === false){
                switch ($rootScope.statutDemande){
                    case statut_demande.EN_ATTENTE:
                        $rootScope.getDemandeAbonne($rootScope.statutDemande);
                        break;
                    case statut_demande.VALIDE:
                        $rootScope.getDemandeAbonne($rootScope.statutDemande);
                        break;
                    case statut_demande.REJETE:
                        $rootScope.getDemandeAbonne($rootScope.statutDemande);
                        break;
                }
                return;
            }

            //Sinon c'est une recherche
            demandeAbonneService.searchDemandeur(criteria, index, size)
                .then(function (res) {
                    console.log('response abonné', res);
                    if (!res.data.hasError){
                        if (res.data.count === 0){
                            console.log('res.data.count', res.data.count);
                            $scope.demandesAbonne = [];
                        }else {
                            $scope.demandesAbonne = []; //Recoit la liste de toutes les demandes
                            $scope.demandesAbonneTemp = res.data.itemsAbonneDemande;
                            _.forEach($scope.demandesAbonneTemp, function (ad) {  //On reconverti les photos des demandeurs
                                //ad.loadSpinAccept = false;
                                //ad.loadSpinReject = false;
                                if (ad.photoAbonne){
                                    ad.photoAbonne = getImageFromBase64(ad)
                                }
                                $scope.demandesAbonne.push(ad);
                                console.log('demandesAbonne', $scope.demandesAbonne);
                            });
                        }
                        $scope.totalDemandeAbonne = res.data.count;
                        console.log('totalDemandeAbonne', $scope.totalDemandeAbonne);
                    }
                });
        }

        /*/
        function getLocalisation(latitude, longitude) {
            console.log('latitude', latitude);
            console.log('longitude', longitude);
            let marker;
            let center = new google.maps.LatLng(59.76522, 18.35002);
            function initialize() {
                $scope.map = new google.maps.Map(document.getElementById('mapSize'), {
                    zoom: 15,
                    center: center,
                    //center: new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude)),
                    //center: {lat: parseFloat(latitude), lng: parseFloat(longitude)},
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                marker = new google.maps.Marker({
                    map: $scope.map,
                    //draggable: true,
                    animation: google.maps.Animation.DROP,
                    position: center
                    //position: {lat: parseFloat(latitude), lng: parseFloat(longitude)}
                });
                marker.addListener('click', toggleBounce);
            }

            function toggleBounce() {
                if (marker.getAnimation() !== null) {
                    marker.setAnimation(null);
                } else {
                    marker.setAnimation(google.maps.Animation.BOUNCE);
                }
            }

            google.maps.event.addDomListener(window, 'load', initialize);
        }
        //*/
    }
})();