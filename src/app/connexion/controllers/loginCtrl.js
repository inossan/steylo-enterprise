(function () {
    'use strict';

    angular.module('app')
        .controller('loginCtrl', loginCtrl);

    loginCtrl.$inject = ['$scope', '$state', 'ConnexionService', 'toastr', 'notificationService'];
    function loginCtrl($scope, $state, ConnexionService, toastr, notificationService) {

        // Set the default value of inputType
        $scope.inputType = 'password';
        $scope.hideShowPassword = hideShowPassword;
        $scope.connexion = connexion;

        // Hide & show password function
        function hideShowPassword(){
            if ($scope.inputType === 'password')
                $scope.inputType = 'text';
            else
                $scope.inputType = 'password';
        }

        function connexion(user) {
            if (!user){ //user non defini
                return;
            }
            if (!user.login || !user.password){ //les champs ne sont pas renseignés
                return;
            }

            $scope.loadSpin = true;
            ConnexionService.login(user)
                .then(function (res) {
                    if (!res.data.hasError){
                        //On recupère les détails (roles, profils,...) du user connecté
                        //todo
                        console.log('res user', res.data.itemChanelUsers);
                        let chanelUser = res.data.itemChanelUsers;

                        if (chanelUser.logoChanel){
                            switch (chanelUser.extLogoChanel){
                                case 'png':
                                    chanelUser.logoChanel =  'data:image/png;base64,' + chanelUser.logoChanel;
                                    break;
                                case 'jpeg':
                                    chanelUser.logoChanel =  'data:image/jpeg;base64,' + chanelUser.logoChanel;
                                    break;
                                case 'jpg':
                                    chanelUser.logoChanel =  'data:image/jpg;base64,' + chanelUser.logoChanel;
                                    break;
                                case 'gif':
                                    chanelUser.logoChanel =  'data:image/gif;base64,' + chanelUser.logoChanel;
                                    break;
                            }
                        }

                        //on stocke ses infos et on lance le dashboard
                        ConnexionService.setCredentials(chanelUser);
                        $state.go('app.main');
                        $scope.loadSpin = false;
                    }else {
                        //toastr.error(res.data.status.message, 'Erreur')
                        notificationService.flatty.error('<b><i class="fa fa-ban"></i>&nbsp;'+res.data.status.message+'</b>');
                        $scope.loadSpin = false;
                    }
                }, function (err) {
                    console.log('err user', err.data);
                    notificationService.flatty.error('<b><i class="fa fa-ban"></i>&nbsp;Impossible d\'accéder au serveur</b>');
                    $scope.loadSpin = false;
                });
        }
    }
})();