(function () {
    'use strict';

    angular.module('app')
        .controller('logoutCtrl', logoutCtrl);

    logoutCtrl.$inject = ['$rootScope', '$scope', '$state', 'ConnexionService', 'notificationService', 'toastr', '$interval'];
    function logoutCtrl($rootScope, $scope, $state, ConnexionService, notificationService, toastr, $interval) {

        $scope.logout = logout;

        function logout(idUser) {
            swal({
                title: 'Vous êtes sur le point de vous déconnecter!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Oui, quitter',
                cancelButtonText: 'Non, rester',
                confirmButtonColor: brandDanger,
                buttonsStyling: true,
                allowOutsideClick: false,
            }).then((result) => {
                if (result.value) {
                    ConnexionService.clearCredentials();
                    toastr.info('Vous êtes maintenant déconnecté !', 'Information');

                    //On stoppe la notification
                    $interval.cancel($rootScope.animateNombreDemande);
                    $interval.cancel($rootScope.callDemandeurNotificationAtInterval);
                    //--------------------------------------------------------------

                    $state.go('connexion.login');

                    // A decommenter si on prend en compte la de/connexion coté serveur
                    /*ConnexionService.logout(idUser)
                        .then(function (res) {
                            if(!res.data.hasError){
                                ConnexionService.clearCredentials();
                                toastr.info('L\'utilisateur a bien été déconnecté', 'Information');
                                $state.go('connexion.login');
                            }
                        }, function (err) {
                            console.log('err user', err);
                            notificationService.flatty.error('<b><i class="fa fa-ban"></i>&nbsp;Une erreur interne est survenue !</b>');
                        });*/
                    // result.dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                } else if (result.dismiss === 'cancel') {

                }
            });
        }
    }
})();