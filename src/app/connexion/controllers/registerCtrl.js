(function () {
    'use strict';

    angular.module('app.commons')
        .controller('registerCtrl', registerCtrl);

    registerCtrl.$inject = ['$rootScope', '$scope', 'ChanelService', 'paysService', 'secteurActiviteService', 'commonUtilities', 'WizardHandler', 'lodash', 'toastr', 'ngNotify', '$state', '$location'];
    function registerCtrl($rootScope, $scope, ChanelService, paysService, secteurActiviteService, commonUtilities, WizardHandler, lodash, toastr, ngNotify, $state, $location) {

        //humane.log("Bienvenue !");

        getPays();
        getSecteurActivite();

        $scope.creerChanel = creerChanel;
        $scope.getEtat = getEtat;
        $rootScope.goToConnexion = goToConnexion;
        $scope.documents = [];

        function goToConnexion() {
            $('[data-toggle="tooltip"]').tooltip('dispose');    //Supprime le tooltip qui persiste
            $state.go('connexion.login');
        }

        //Les définitions des fonctions
        function getSecteurActivite() {
            secteurActiviteService.getSecteurActivite()
                .then(function (res) {
                    if(!res.data.hasError) {
                        $scope.secteursActivites = res.data.itemsSecteurActivite;
                        console.log('secteursActivites', $scope.secteursActivites);
                    }else {
                        ngNotify.set(res.data.status.message, {
                            type: 'error'
                        });
                    }
                });
        }

        function getPays() {
            paysService.getPays()
                .then(function (res) {
                    if(!res.data.hasError) {
                        $scope.pays = res.data.itemsCountry;
                        console.log('pays', $scope.pays);
                    }else {
                        ngNotify.set(res.data.status.message, {
                            type: 'error'
                        });
                    }
                });
        }

        function creerChanel(chanel) {
            $scope.entreprise.idPays = chanel.pays.id;
            $scope.entreprise.logo = lodash.split(chanel.logo, ',')[1];
            $scope.entreprise.extLogo = $rootScope.filename.type;
            console.log('chanel', chanel);
            console.log('$scope.entreprise', $scope.entreprise);
            //console.log('$rootScope.filename', $rootScope.filename);
            //return;
            ChanelService.createChanel($scope.entreprise)
                .then(function (res) {
                    console.log('res', res);
                    if (!res.data.hasError){
                        $scope.entreprise = undefined;
                        $scope.confPass = undefined;
                        WizardHandler.wizard('wz-register').reset();

                        toastr.success('Enregistrement effectué avec succès !', 'Succès');
                        //utiliser un timer avant de revenir à la connexion
                        $state.go('connexion.login');

                    }else {
                        //WizardHandler.wizard('wz-register').reset();
                        ngNotify.set(res.data.status.message, {
                            type: 'error'
                        });
                    }
                }, function (err) {
                    console.log('creerChanel err', err);
                });
        }

        //Permet d'auto-remplir le champ "etat" du formulaire lorsque le pays est selectionné
        function getEtat(pays) {
            $scope.entreprise.etat = pays.countryName;
        }

        //$("#file").filestyle({text: "Find file"});

        /*function validateFileType(){
            var fileName = document.getElementById("fileName").value;
            var idxDot = fileName.lastIndexOf(".") + 1;
            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
            if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
                //TO DO
            }else{
                alert("Only jpg/jpeg and png files are allowed!");
            }
        }*/
    }
})();