(function () {
    'use strict';

    angular.module('app.suivis')
        .controller('detailsRetourCtrl', detailsRetourCtrl);

    detailsRetourCtrl.$inject = ['$rootScope', '$scope', 'RetourService', 'EtatRetourService','$stateParams', 'toastr'];
    function detailsRetourCtrl($rootScope, $scope, RetourService, EtatRetourService, $stateParams, toastr) {

        $scope.showDetailsRetour = showDetailsRetour($stateParams.id);

        function showDetailsRetour(idRetour) {

            $('[data-toggle="tooltip"]').tooltip('dispose');

            RetourService.getDetailsRetours(idRetour)
                .then(function (res) {
                    console.log('res getDetailsRetour', res);
                    if (!res.data.hasError) {
                        $scope.retour = res.data.itemsRetourCommande[0];

                        console.log('retour', $scope.retour);
                    } else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err getDetailsRetour', err);
                });
        }

    }
})();