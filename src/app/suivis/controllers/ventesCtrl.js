(function () {
    'use strict';

    angular.module('app.suivis')
        .controller('ventesCtrl', ventesCtrl);

    ventesCtrl.$inject = ['$rootScope', '$scope', 'VenteService', 'toastr'];
    function ventesCtrl($rootScope, $scope, VenteService, toastr) {

        $scope.getVentes = getVentes;
        $scope.getTotalProduit = getTotalProduit;

        getVentes();

        function getVentes(index, size) {
            VenteService.getVentes(index, size)
                .then(function (res) {
                    if (res.data.count === 0){
                        console.log('res.data.count', res.data.count);
                        $scope.ventes = [];
                    }else {
                        $scope.ventes = []; //Recoit la liste de toutes les ventes
                        $scope.ventesTemp = res.data.itemsKitProduit;
                        _.forEach($scope.ventesTemp, function (ad) {  //On reconverti les photos des e-commerciaux
                            if (ad.photoAbonne){
                                ad.photoAbonne = getImageFromBase64(ad)
                            }
                            $scope.ventes.push(ad);
                            console.log('ventes', $scope.ventes);
                        });
                    }
                    $scope.totalVentes = res.data.count;
                }, function (err) {
                    console.log('err getVentes --> ' + err);
                });
        }

        function getTotalProduit(prix, qte) {
            return prix * qte;
        }

        //Revoir comment cette function pourrait etre factorisée et la centraliser dans un fichier commun à tous les modules
        function getImageFromBase64(img) {
            switch (img.extPhotoAbonne){
                case 'png':
                    return 'data:image/png;base64,' + img.photoAbonne;
                case 'jpeg':
                    return 'data:image/jpeg;base64,' + img.photoAbonne;
                case 'jpg':
                    return 'data:image/jpg;base64,' + img.photoAbonne;
                case 'gif':
                    return 'data:image/gif;base64,' + img.photoAbonne;
            }
        }
    }
})();