(function () {
    'use strict';

    angular.module('app.suivis')
        .factory('CommandeService', CommandeService);

    CommandeService.$inject = ['$rootScope', '$http'];
    function CommandeService($rootScope, $http) {
        const commandeService = {};

        commandeService.getCommandes = getCommandes;
        commandeService.getDetailsCommandes = getDetailsCommandes;
        commandeService.searchCommandes = searchCommandes;
        commandeService.updateCommande = updateCommande;

        return commandeService;

        function getCommandes(statut, index, size, showLoadingBar) {
            let slb = (showLoadingBar === 'no-loading-bar');
            let request = {
                dataAbonneCommand: {
                    idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel,
                    statutCommandCode: statut
                },
                index: index || 0,
                size: size || 6
            };

            //let URL = BASE_URL + '/abonneCommand/getLessByCriteria';
            let URL = BASE_URL + '/abonneCommand/getFullByCriteria';
            return $http.post(URL, request, {
                ignoreLoadingBar: slb,
                cache: true
            });
        }
        function getDetailsCommandes(idCommande) {
            let request = {
                dataAbonneCommand: {
                    id: idCommande,
                    idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel,
                }
            };

            let URL = BASE_URL + '/abonneCommand/getFullByCriteria';
            return $http.post(URL, request, {cache: true});
        }

        function searchCommandes(criteria, index, size) {
            let request = {
                dataAbonneCommand: criteria,
                index: index || 0,
                size: size || 6
            };
            let URL = BASE_URL + '/abonneCommand/getByCriteria';
            return $http.post(URL, request, { cache: true });
        }

        function updateCommande(id, statut, commandeInfo) {
            let request = {
                datasAbonneCommand: [{
                    id: id,
                    statutCommandCode: statut,
                    historiqueCommande: {
                        commentaire: commandeInfo.commentaire || "",
                        notifyCustomer: commandeInfo.notifyCustomer || 0
                    }
                }]
            };
            let URL = BASE_URL + '/abonneCommand/update';
            return $http.post(URL, request, { cache: true });
        }

    }
})();