(function () {
    'use strict';

    angular.module('app.suivis')
        .factory('RetourService', RetourService);

    RetourService.$inject = ['$rootScope', '$http'];
    function RetourService($rootScope, $http) {
        const retourService = {};

        retourService.getRetours = getRetours;
        retourService.searchRetour = searchRetour;
        retourService.updateRetour = updateRetour;
        retourService.getDetailsRetours = getDetailsRetours;

        return retourService;

        function getRetours(index, size) {
            let request = {
                dataRetourCommande: {
                    idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel,
                },
                index: index || 0,
                size: size || 6
            };

            let URL = BASE_URL + '/retourCommande/getByCriteria';
            return $http.post(URL, request, { cache: true});
        }

        function getDetailsRetours(idRetour) {
            let request = {
                dataRetourCommande: {
                    id: idRetour,
                    idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel,
                }
            };

            let URL = BASE_URL + '/retourCommande/getByCriteria';
            return $http.post(URL, request, { cache: true});
        }

        function searchRetour(criteria, index, size) {
            let request = {
                dataRetourCommande: criteria,
                index: index || 0,
                size: size || 6
            };
            let URL = BASE_URL + '/retourCommande/getByCriteria';
            return $http.post(URL, request, { cache: true });
        }

        function updateRetour(r) {
            let request = {
                datasRetourCommande: [r]
            };
            let URL = BASE_URL + '/retourCommande/update';
            return $http.post(URL, request, { cache: true });
        }

    }
})();