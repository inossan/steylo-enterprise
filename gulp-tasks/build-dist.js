// 'use strict'
//
// var gulp = require('gulp');
// var uglify = require('gulp-uglify');
// var cssmin = require('gulp-cssmin')
// var rename = require('gulp-rename');
// var del = require('del');
// var runSequence = require('run-sequence');
// var replace = require('gulp-replace');
//
// var paths = gulp.paths;
//
// gulp.vendors = require('./../vendors.json');
// var vendors = gulp.vendors;
//
// gulp.task('copy:vendorsCSS', function() {
//   return gulp.src(vendorsCSS)
//   .pipe(gulp.dest(paths.vendors + '/css/'));
// });
//
// gulp.task('minify:vendorsCSS', function() {
//   return gulp.src([paths.vendors + '/css/*.css', '!' + paths.vendors + '/css/*.min.css'])
//   .pipe(cssmin())
//         .pipe(rename({suffix: '.min'}))
//         .pipe(gulp.dest(paths.vendors + '/css/'));
// });
//
// gulp.task('clean:vendorsCSS', function () {
//     return del([paths.vendors + '/css/*.css', '!' + paths.vendors + '/css/*.min.css']);
// });
//
// gulp.task('vendors:css', function(callback) {
//     runSequence('copy:vendorsCSS', 'minify:vendorsCSS', 'clean:vendorsCSS', callback);
// });
//
// gulp.task('copy:vendorsJS', function() {
//   return gulp.src(vendorsJS)
//   .pipe(gulp.dest(paths.vendors + '/app/'));
// });
//
// gulp.task('minify:vendorsJS', function() {
//   return gulp.src([paths.vendors + '/app/*.app', '!' + paths.vendors + '/app/*.min.app'])
//   .pipe(gulp.dest(paths.vendors + '/app/'))
//   .pipe(uglify())
//   .pipe(rename({ suffix: '.min' }))
//   .pipe(gulp.dest(paths.vendors+'/app/'));
// });
//
// gulp.task('clean:vendorsJS', function () {
//     return del([paths.vendors + '/app/*.app', '!' + paths.vendors + '/app/*.min.app']);
// });
//
// gulp.task('vendors:app', function(callback) {
//     runSequence('copy:vendorsJS', 'minify:vendorsJS', 'clean:vendorsJS', callback);
// });
//
// gulp.task('copy:vendorsFonts', function() {
//   return gulp.src(vendorsFonts)
//   .pipe(gulp.dest(paths.vendors + '/fonts/'));
// });
//
// gulp.task('replace:node_modules', function(){
//   return gulp.src([
//       './dist/*.html',
//       './dist/**/*.app',
//     ], {base: './'})
//     .pipe(replace(/node_modules+.+(\/[a-z0-9][^/]*\.app+(\'|\"))/ig, 'vendors/app$1'))
//     .pipe(replace(/vendors\/app\/(.*).app/ig, 'vendors/app/$1.min.app'))
//     .pipe(replace(/..\/..\/vendors\/app\/(.*).app/ig, '../../vendors/app/$1.min.app'))
//     .pipe(replace('.min.min.app', '.min.app'))
//     .pipe(replace(/node_modules+.+(\/[a-z0-9][^/]*\.css+(\'|\"))/ig, 'vendors/css$1'))
//     .pipe(replace(/vendors\/css\/(.*).css/ig, 'vendors/css/$1.min.css'))
//     .pipe(replace(/..\/..\/vendors\/css\/(.*).css/ig, '../../vendors/css/$1.min.css'))
//     .pipe(replace('.min.min.css', '.min.css'))
//     .pipe(gulp.dest('./'));
// });
//
// gulp.task('vendors', function(callback) {
//     runSequence('vendors:css', 'vendors:app', 'copy:vendorsFonts', 'replace:node_modules', callback);
// });
//
// gulp.task('clean:dist', function () {
//     return del(paths.dist);
// });
//
// gulp.task('copy:css', function() {
//    return gulp.src('./css/**/*')
//    .pipe(gulp.dest(paths.dist+'/css'));
// });
//
// gulp.task('copy:img', function() {
//    return gulp.src('./img/**/*')
//    .pipe(gulp.dest(paths.dist+'/img'));
// });
//
// gulp.task('copy:app', function() {
//    return gulp.src('./app/**/*')
//    .pipe(gulp.dest(paths.dist+'/app'));
// });
//
// gulp.task('copy:views', function() {
//    return gulp.src('./views/**/*')
//    .pipe(gulp.dest(paths.dist+'/views'));
// });
//
// gulp.task('copy:html', function() {
//    return gulp.src('index.html')
//    .pipe(gulp.dest(paths.dist+'/'));
// });
//
// gulp.task('build:dist', function(callback) {
//     runSequence('clean:dist', 'copy:css', 'copy:img', 'copy:app', 'copy:views', 'copy:html', 'vendors', callback);
// });

'use strict';

let gulp = require('gulp');
let uglify = require('gulp-uglify');
let cssmin = require('gulp-cssmin');
let rename = require('gulp-rename');
let del = require('del');
let runSequence = require('run-sequence');
let replace = require('gulp-replace');

let paths = gulp.paths;

gulp.vendors = require('./../vendors.json');
let vendors = gulp.vendors;

gulp.task('copy:vendorsCSS', function() {
  return gulp.src(vendors.css)
  .pipe(gulp.dest(paths.vendors + 'css/'));
});

gulp.task('minify:vendorsCSS', function() {
  return gulp.src([
    paths.vendors + 'css/*.css',
    '!' + paths.vendors + 'css/*.min.css'
  ])
  .pipe(cssmin())
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest(paths.vendors + 'css/'));
});

gulp.task('clean:vendorsCSS', function () {
    return del([
      paths.vendors + 'css/*.css',
      '!' + paths.vendors + 'css/*.min.css'
    ]);
});

gulp.task('vendors:css', function(callback) {
    runSequence('copy:vendorsCSS', 'minify:vendorsCSS', 'clean:vendorsCSS', callback);
});

gulp.task('copy:vendorsJS', function() {
  return gulp.src(vendors.js)
  .pipe(gulp.dest(paths.vendors + 'app/'));
});

gulp.task('minify:vendorsJS', function() {
  return gulp.src([
    paths.vendors + 'app/*.js',
    '!' + paths.vendors + 'app/*.min.js'
  ])
  .pipe(gulp.dest(paths.vendors + 'app/'))
  .pipe(uglify())
  .pipe(rename({ suffix: '.min' }))
  .pipe(gulp.dest(paths.vendors+'app/'));
});

gulp.task('clean:vendorsJS', function () {
    return del([
      paths.vendors + 'app/*.js',
      '!' + paths.vendors + 'app/*.min.js']);
});

gulp.task('vendors:app', function(callback) {
    runSequence('copy:vendorsJS', 'minify:vendorsJS', 'clean:vendorsJS', callback);
});

gulp.task('copy:vendorsFonts', function() {
  return gulp.src(vendors.fonts)
  .pipe(gulp.dest(paths.vendors + 'fonts/'));
});

gulp.task('replace:node_modules', function(){
  return gulp.src([
      paths.dist + '**/*.html',
      paths.dist + '**/*.js',
    ], {base: './'})
    .pipe(replace(/node_modules+.+(\/[a-z0-9][^/]*\.js+(\'|\"))/ig, 'vendors/app$1'))
    .pipe(replace(/vendors\/js\/(.*).js/ig, 'vendors/app/$1.min.js'))
    .pipe(replace(/..\/..\/vendors\/js\/(.*).js/ig, 'vendors/app/$1.min.js'))
    .pipe(replace('.min.min.js', '.min.js'))
    .pipe(replace(/node_modules+.+(\/[a-z0-9][^/]*\.css+(\'|\"))/ig, 'vendors/css$1'))
    .pipe(replace(/vendors\/css\/(.*).css/ig, 'vendors/css/$1.min.css'))
    .pipe(replace(/..\/..\/vendors\/css\/(.*).css/ig, 'vendors/css/$1.min.css'))
    .pipe(replace('.min.min.css', '.min.css'))
    .pipe(gulp.dest('./'));
});
gulp.task('replace:bower_components', function(){
  return gulp.src([
      paths.dist + '**/*.html',
      paths.dist + '**/*.js',
    ], {base: './'})
    .pipe(replace(/bower_components+.+(\/[a-z0-9][^/]*\.js+(\'|\"))/ig, 'vendors/app$1'))
    .pipe(replace(/vendors\/js\/(.*).js/ig, 'vendors/app/$1.min.js'))
    .pipe(replace(/..\/..\/vendors\/js\/(.*).js/ig, 'vendors/app/$1.min.js'))
    .pipe(replace('.min.min.js', '.min.js'))
    .pipe(replace(/bower_components+.+(\/[a-z0-9][^/]*\.css+(\'|\"))/ig, 'vendors/css$1'))
    .pipe(replace(/vendors\/css\/(.*).css/ig, 'vendors/css/$1.min.css'))
    .pipe(replace(/..\/..\/vendors\/css\/(.*).css/ig, 'vendors/css/$1.min.css'))
    .pipe(replace('.min.min.css', '.min.css'))
    .pipe(gulp.dest('./'));
});

gulp.task('vendors', function(callback) {
    runSequence('vendors:css', 'vendors:app', 'copy:vendorsFonts', 'replace:node_modules', 'replace:bower_components', callback);
});

gulp.task('clean:dist', function () {
    return del(paths.dist);
});

gulp.task('copy:css', function() {
   return gulp.src(paths.src + 'css/**/*')
   .pipe(gulp.dest(paths.dist + 'css'));
});

gulp.task('copy:img', function() {
   return gulp.src(paths.src + 'img/**/*')
   .pipe(gulp.dest(paths.dist + 'img'));
});

gulp.task('copy:app', function() {
   return gulp.src(paths.src + 'app/**/*')
   .pipe(gulp.dest(paths.dist + 'app'));    //devenir 'app' au lieu de 'js'
});

gulp.task('copy:assets', function() {
   return gulp.src(paths.src + 'assets/**/*')
   .pipe(gulp.dest(paths.dist + 'assets'));
});

gulp.task('copy:views', function() {
   return gulp.src(paths.src + 'views/**/*')
   .pipe(gulp.dest(paths.dist + 'views'));
});

gulp.task('copy:html', function() {
   return gulp.src(paths.src + 'index.html')
   .pipe(gulp.dest(paths.dist));
});

gulp.task('copy:vendors', function() {
   return gulp.src(paths.src + 'vendors/**/*')
   .pipe(gulp.dest(paths.dist + 'vendors/'));
});

gulp.task('build:dist', function(callback) {
    runSequence('clean:dist', 'copy:css', 'copy:img', 'copy:app', 'copy:assets', 'copy:views', 'copy:html', 'copy:vendors', 'vendors', callback);
});
