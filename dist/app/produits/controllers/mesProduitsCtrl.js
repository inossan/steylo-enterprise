(function () {
    'use strict';

    angular.module('app.produits')
        .controller('mesProduitsCtrl', mesProduitsCtrl);

    mesProduitsCtrl.$inject = ['$rootScope', '$scope', '$state', 'ProduitService', 'CategoriesService', 'SousCategoriesService', 'MarqueService', 'notificationService', 'toastr', 'AclService'];
    function mesProduitsCtrl($rootScope, $scope, $state, ProduitService, CategoriesService, SousCategoriesService, MarqueService, notificationService, toastr, AclService) {

        $scope.can = AclService.can;
        $rootScope.getStatusProduit = getStatusProduit;
        $scope.searchProduits = searchProduits;
        $rootScope.getProduits = getProduits;
        getProduits();  //Pour charger la fonction par défaut
        $rootScope.enableDisableProduit = enableDisableProduit;
        $rootScope.deleteProduit = deleteProduit;
        $scope.showImage = showImage;
        $scope.exporterXlsx = exporterXlsx;

        $scope.currentPage = 1;
        $scope.currentPageS = 1;
        $scope.sizeProduit = 6;
        $scope.max_size = 3;
        $scope.subviewproduit = 'viewTable'; //On choisit l'affichage table par défaut

        $scope.searching = false;   //Pour spécifier si nous sommes en train de faire une recherche ou pas
        const modalAutreImage = $('#autre-image-produit');

        //Configuration du tabs-----------------------------
        $rootScope.tabs = {
            active: 0
        };
        $rootScope.pages = {
            active: 0
        };
        $rootScope.horizontalTabs = {
            active: 0
        };
        $rootScope.pages = [
            {
                id: 1,
                title: 'Détails',
                badge: true //Pour afficher le nombre de demande  dans un badge uniquement pour cet onglet
            },
            {
                id: 2,
                title: 'Image(s) du produit',
                content: 'ok'
            }
        ];
        //Fin de la configuration du tabs-------------------

        function getStatusProduit(produit) {
            switch (produit.status){
                case 1:
                    return {
                        badge: 'badge-success',
                        text: 'Activé',
                        icon: 'icon-check',
                        state: 'success'
                    };
                case 0:
                    return {
                        badge: 'badge-secondary',
                        text: 'Désactivé',
                        icon: 'icon-close',
                        state: 'danger'
                    };
            }
        }

        function showImage(p) {
            console.log('image id', p.id);
            ProduitService.getImagesProduit(p.id)
                .then(function (res) {
                    console.log('res imageProduit', res);
                    if (!res.data.hasError && res.data.count > 0){
                        $rootScope.autresImages = p;
                        $rootScope.autresImages.listImageProduit = res.data.itemsImageProduit;
                        modalAutreImage.modal('show');
                    }else {
                        toastr.info('Aucune image trouvée', 'Information');
                    }
                }, function (err) {
                    console.log('err imageProduit', err);
                });
        }

        function getProduits(index, size) {
            $scope.criteria = {}; //On réinitialise tous les critères de recherche
            ProduitService.getProduits(index, size)
                .then(function (res) {
                    console.log('res produits', res);
                    if (!res.data.hasError){
                        $scope.produits = res.data.itemsProduit;
                        $scope.totalProduit = res.data.count;
                        $rootScope.getCategories();
                        console.log('totalProduit', res.data.count);
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err produits', err);
                });
        }

        function enableDisableProduit(produit, choix) {
            let p = {};
            p.id = produit.id;
            p.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;
            switch (choix){
                case 'activer':
                    p.status = 1;
                    break;
                case 'desactiver':
                    p.status = 0;
                    break;
            }
            ProduitService.enableDisableProduit(p)
                .then(function (res) {
                    console.log('res update produit', res);
                    if (!res.data.hasError){
                        if (res.data.itemsProduit[0].status === 1){
                            $scope.etatProduit = 'desactiver';
                            notificationService.flatty.success('<b><i class="fa fa-check"></i>&nbsp;Votre produit a bien été activé !</b>');
                        }
                        if (res.data.itemsProduit[0].status === 0){
                            $scope.etatProduit = 'activer';
                            notificationService.flatty.error('<b><i class="fa fa-ban"></i>&nbsp;Votre produit a bien été désactivé !</b>');
                        }
                        getProduits();
                        $rootScope.getImportProduits();
                        console.log('res enableOrDisable produit', res);
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err update produit', err);
                });
        }

        function deleteProduit(produit) {
            let p = {};
            p.id = produit.id;
            p.trash = 1;
            ProduitService.deleteProduit(p)
                .then(function (res) {
                    if (!res.data.hasError){
                        notificationService.flatty.error('<b><i class="fa fa-close"></i>&nbsp;Votre produit a bien été supprimé !</b>');
                        getProduits();
                        $rootScope.getImportProduits();
                        console.log('res delete produit', res);
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err delete produit', err);
                });
        }

        function searchProduits(criteria, index, size, searchClick) {
            console.log('criteria', criteria);
            $scope.searching = true;

            if (searchClick === 'searchClick'){
                if (!criteria.hasOwnProperty('idCategorie') && !criteria.hasOwnProperty('idSousCategorie') && !criteria.hasOwnProperty('idMarque') && !criteria.hasOwnProperty('couleur')){
                    toastr.info('Veuillez sélectionner au moins un critère de recherche !', 'Information');
                    return;
                }
            }

            criteria.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;
            criteria.trash = 0;
            ProduitService.searchProduits(criteria, index, size) //revenir pour paginer dans le cas d'une recherche - OK
                .then(function (res) {
                    console.log('res search produits', res);
                    if (!res.data.hasError){
                        $scope.produits = res.data.itemsProduit;
                        $scope.totalProduit = res.data.count;
                        if (res.data.count === 0 && !criteria.libelle){
                            toastr.info('Aucun produit trouvé pour cette recherche !', 'Information');
                        }
                        console.log('totalProduit', res.data.count);
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err search produits', err);
                });
        }

        function exporterXlsx() {
            ProduitService.exportToXLSX()
                .then(function (res) {
                    console.log('res export produits', res);
                    if (!res.data.hasError){
                        let file = new Blob([res.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
                        if (file.size === 0){
                            toastr.info('Aucune donnée à exporter !', 'Information');
                            return;
                        }
                        saveAs(file, 'Liste-produits');
                        toastr.success('Exportation effectuée avec succès!', 'Succès');
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err search produits', err);
                });
        }
    }
})();