(function () {
    'use strict';

    angular.module('app.commons')
        .factory('ProduitService', ProduitService);

    ProduitService.$inject = ['$rootScope', '$http', 'Upload'];
    function ProduitService($rootScope, $http, Upload) {
        const chanelService = {};
        chanelService.createProduit = createProduit;
        chanelService.updateProduit = updateProduit;
        chanelService.enableDisableProduit = enableDisableProduit;
        chanelService.deleteProduit = deleteProduit;
        chanelService.getProduits = getProduits;
        chanelService.searchProduits = searchProduits;
        chanelService.getImagesProduit = getImagesProduit;
        chanelService.exportToPDF = exportToPDF;
        chanelService.exportToXLSX = exportToXLSX;
        chanelService.importProduits = importProduits;
        chanelService.getImportProduits = getImportProduits;

        return chanelService;

        function createProduit(p) {
            let request = {
                datasProduit: [p]
            };
            let URL = BASE_URL + '/produit/create';
            return $http.post(URL, request, {cache: true});
        }

        function updateProduit(p) {
            let request = {
                datasProduit: [p]
            };
            let URL = BASE_URL + '/produit/update';
            return $http.post(URL, request, {cache: true});
        }

        function enableDisableProduit(p) {
            let request = {
                datasProduit: [p]
            };
            let URL = BASE_URL + '/produit/update';
            return $http.post(URL, request, {cache: true});
        }

        function deleteProduit(p) {
            let request = {
                datasProduit: [p]
            };
            let URL = BASE_URL + '/produit/update';
            return $http.post(URL, request, {cache: true});
        }

        function getProduits(index, size) {
            let request = {
                dataProduit:{
                    idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel,
                    trash: 0      //On recupère que les produits non supprimés
                    //status: 1   //On recupère que les produits actifs
                },
                index: index || 0,
                size: size || 6
            };
            let URL = BASE_URL + '/produit/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }

        function getImportProduits(index, size) {
            let request = {
                dataProduit:{
                    idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel,
                    trash: 0,      //On recupère que les produits non supprimés
                    byUpload: true   //On recupère que les produits uplodés
                },
                index: index || 0,
                size: size || 5
            };
            let URL = BASE_URL + '/produit/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }

        function searchProduits(criteria, index, size) {
            let request = {
                dataProduit: criteria,
                index: index || 0,
                size: size || 6
            };
            let URL = BASE_URL + '/produit/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }

        function getImagesProduit(idProduit) {
            let request = {
                dataImageProduit: {
                    idProduit: idProduit
                }
            };
            let URL = BASE_URL + '/imageProduit/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }

        function exportToPDF() {
            let request = {

            };
            let URL = BASE_URL + '/produit/';
            return $http.post(URL, request, {cache: true});
        }

        function exportToXLSX() {
            let request = {
                dataProduit: {
                    idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel,
                    trash: 0    //On récupère les produits non supprimés
                }
            };
            let URL = BASE_URL + '/produit/getProduitReport';
            return $http.post(URL, request,
                {
                    cache: true,
                    responseType: 'arraybuffer'
                });
        }

        function importProduits(file, criteria) {
            let URL = BASE_URL + '/produit/importProduits?'
                + 'chanel=' + $rootScope.globals.currentUser.chanelUserInfos.idChanel
                + '&marque=' + criteria.idMarque
                + '&categorie=' + criteria.idCategorie
                + '&sousCategorie=' + criteria.idSousCategorie;

            return Upload.upload({
                url: URL,
                data: {file: file}
            });
        }
    }
})();