(function () {
    'use strict';

    angular.module('app.profil')
        .controller('profilCtrl', profilCtrl);

    profilCtrl.$inject = ['$rootScope', '$scope', '$state', 'ConnexionService', 'toastr', '$interval', 'notificationService'];
    function profilCtrl($rootScope, $scope, $state, ConnexionService, toastr, $interval, notificationService) {

        $scope.updateChanelUser = updateChanelUser;
        $scope.showModalEditPass = showModalEditPass;
        $scope.changePass = changePass;
        //On récupère les infos stockées lors de la connexion (A revoir si c'est bien la bonne façon de procéder)
        $scope.userProfil = angular.copy($rootScope.globals.currentUser.chanelUserInfos);

        let modalEditPass = $('#modifPass');

        function updateChanelUser(u) {
            let user = {};
            user.id = u.id;
            user.nom = u.nom;
            user.prenoms = u.prenoms;
            user.login = u.login;
            user.email = u.email;

            ConnexionService.updateChanelUser(user)
                .then(function (res) {
                    console.log('res update chanelUser', res);
                    if (!res.data.hasError){
                        notificationService.flatty.success('<b><i class="fa fa-check"></i>&nbsp;Les informations ont été modifiés !</b>');
                    }
                }, function (err) {
                    console.log('err update chanelUser', err);
                });
        }

        function showModalEditPass() {
            modalEditPass.modal('show');
        }
        
        function changePass(user) {
            // Vérifier la confirmation du pass - Vérifier l'ancien mot de passe

            //Vérification des champs obligatoires
            if (!user){
                user = {};
            }
            if (!user.hasOwnProperty('oldPass') || user.oldPass === ''){
                toastr.error('Veuillez renseigner l\'ancien mot de passe !', 'Erreur');
                return;
            }
            if (!user.hasOwnProperty('newPass') || user.newPass === ''){
                toastr.error('Veuillez renseigner le nouveau mot de passe !', 'Erreur');
                return;
            }
            if (!user.hasOwnProperty('confPass') || user.confPass === ''){
                toastr.error('Veuillez renseigner la confirmation du mot de passe !', 'Erreur');
                return;
            }
            if (user.confPass !== user.newPass){
                toastr.error('Le nouveau mot de passe et sa confirmation ne concordent pas !', 'Erreur');
                return;
            }
            user.oldPassword = user.oldPass;
            user.password = user.newPass;
            user.login = $rootScope.globals.currentUser.chanelUserInfos.login;
            user.id = $rootScope.globals.currentUser.chanelUserInfos.id;
            console.log('user', user);
            ConnexionService.updatePassChanelUser(user)
                .then(function (res) {
                    console.log('res chanelUser', res);
                    if (!res.data.hasError){
                        modalEditPass.modal('hide');
                        toastr.success('Le mot de passe a été modifié !', 'Succès');
                        ConnexionService.clearCredentials();

                        //On stoppe la notification
                        $interval.cancel($rootScope.animateNombreDemande);
                        $interval.cancel($rootScope.callDemandeurNotificationAtInterval);
                        //--------------------------------------------------------------

                        $state.go('connexion.login');
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err chanelUser', err);
                });
        }
    }
})();