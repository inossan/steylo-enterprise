(function () {
    'use strict';

    angular.module('app.espace-publicitaire')
        .controller('publicationsCtrl', publicationsCtrl);

    publicationsCtrl.$inject = ['$rootScope', '$scope', '$state', 'Upload', 'EspacePubService', 'toastr'];
    function publicationsCtrl($rootScope, $scope, $state, Upload, EspacePubService, toastr) {

        $scope.getPrice = getPrice;
        $scope.converteOctet2Ko = converteOctet2Ko;
        $scope.getGlobalPrice = getGlobalPrice;
        $scope.getGlobalSize = getGlobalSize;
        $scope.showDialogText = showDialogText;
        $scope.dialogSendPublication = showDialogSendPublication;
        $rootScope.getTexte = getTexte;
        $rootScope.sendRangeDate = sendRangeDate;
        $rootScope.sendPublication = sendPublication;
        $rootScope.sendModifiyingPublication = sendModifiyingPublication;
        $scope.getPublications = getPublications();
        $scope.searchPublications = searchPublications;
        $rootScope.modifyPub = modifyPub;
        //$scope.getDatePickerOptions = getDatePickerOptions;

        $scope.currentPage = 1;
        $scope.currentPageS = 1;
        $scope.sizePublication = 5;
        $scope.max_size = 3;

        $rootScope.publication = {};
        $scope.tabs = {
            active: 0
        };
        $scope.pages = {
            active: 0
        };
        $rootScope.horizontalTabs = {
            active: 0
        };

        const dialogText = $('#dialog-text');
        const dialogSendPublication = $('#dialog-send-publication');

        // Configurations date-range-picker-----------------------
        $scope.datePicker = {
            startDate: moment().subtract(1, "days"),
            endDate: moment()
        };

        $rootScope.startDateTimePicker = {
            startDate: moment(),
            //endDate: moment()
        };

        $rootScope.endDateTimePicker = {
            //startDate: moment(),
            endDate: moment()
        };

        $scope.options = {
            applyClass: 'btn-success',
            cancelClass: 'btn-secondary',
            opens: 'left',
            //timePicker: true,
            //timePicker24Hour: true,
            autoApply: true,
            //timePickerSeconds: true,
            locale: {
                applyLabel: "Choisir",
                fromLabel: "De",
                //format: "YYYY-MM-DD", //will give you 2017-01-06
                //format: "D-MMM-YY", //will give you 6-Jan-17
                format: "DD MMMM YYYY", //will give you 6 January 2017
                toLabel: "à",
                cancelLabel: 'Annuler',
                customRangeLabel: 'Définir intervalle'
            },
            ranges: {
                'Aujourd\'hui': [moment(), moment()],
                'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 derniers jours': [moment().subtract(6, 'days'), moment()],
                '30 derniers jours': [moment().subtract(29, 'days'), moment()],
                'Ce mois': [moment().startOf('month'), moment().endOf('month')],
                'Le mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };

        //console.log('startDate', $scope.startDateTimePicker.startDate.format('DD-MM-YYYY hh:mm'));
        //console.log('endDate', $scope.endDateTimePicker.endDate.format('DD-MM-YYYY hh:mm'));

        $rootScope.optionsTimeDatePicker = {
            applyClass: 'btn-success',
            cancelClass: 'btn-secondary',
            opens: 'center',
            timePicker: true,
            timePicker24Hour: true,
            timePickerSeconds: false,
            autoApply: false,
            singleDatePicker: true,
            locale: {
                applyLabel: "Choisir",
                cancelLabel: 'Annuler',
                format: "DD MMMM YYYY hh:mm",
            }
        };
        // Fin de la configuration--------------------------------

        $scope.searching = false;

        function searchPublications(criteria, index, size) {
            console.log('startDate', criteria.startDate.format('DD-MM-YYYY'));
            console.log('endDate', criteria.endDate.format('DD-MM-YYYY'));
            $scope.searching = true;

            let dataCriteria = {};

            dataCriteria.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;
            //dataCriteria.isValid = 0;
            dataCriteria.startDate = criteria.startDate.format('DD-MM-YYYY');
            dataCriteria.endDate = criteria.endDate.format('DD-MM-YYYY');

            EspacePubService.searchPublications(dataCriteria, index, size)
                .then(function (res) {
                    console.log('res search publications', res);
                    if (!res.data.hasError){
                        $scope.publications = res.data.itemsPublication;
                        $scope.totalPublications = res.data.count;
                        if (res.data.count === 0 && !criteria.libelle){
                            toastr.info('Aucune publication trouvée pour cette recherche !', 'Information');
                        }
                        console.log('totalPublication', res.data.count);
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err search publications', err);
                });
        }

        function getPublications(index, size) {

            EspacePubService.getFullPublications(index, size)
                .then(function (res) {
                    console.log('getPublications res', res);
                    if (!res.data.hasError){
                        $scope.publications = _.filter(res.data.itemsPublication, function (pv) {
                            return pv.isValid !== undefined;
                        });
                        $scope.totalPublications = $scope.publications.length;
                        console.log('$scope.publications', $scope.publications);
                    }else {
                        console.log('Erreur', res.data.status.message);
                    }
                }, function (err) {
                    console.log('getPublications err', err);
                });
            //dialogSendPublication.modal('hide')
        }

        function sendPublication(publication) {
            console.log('publication', publication);

            let filesToSend = [];   //Attention l'ordre des fichiers compte pour permettre au backend de reconnaitre chaque type de fichier
            filesToSend.push(publication.video);
            filesToSend.push(publication.image);
            filesToSend.push(publication.audio);
            filesToSend.push($scope.texte);
            filesToSend.push(publication.texte.size);

            EspacePubService.importPublication(filesToSend)
                .then(function (res) {
                    console.log('upload res', res);
                    if (!res.data.hasError){
                        toastr.success("Soumission effectuée avec succès !", 'Succès');
                        $rootScope.publication = undefined;
                        $state.reload()
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('upload err', err);
                }, function (evt) {
                    console.log('upload progress', evt);
                });

        }


        function sendModifiyingPublication(publication) {
            console.log('modif publication', publication);

            return;

            let filesToSend = [];   //Attention l'ordre des fichiers compte pour permettre au backend de reconnaitre chaque type de fichier
            filesToSend.push(publication.video);
            filesToSend.push(publication.image);
            filesToSend.push(publication.audio);
            filesToSend.push($scope.texte);
            filesToSend.push(publication.texte.size);

            EspacePubService.importPublication(filesToSend)
                .then(function (res) {
                    console.log('upload res', res);
                    if (!res.data.hasError){
                        toastr.success("Soumission effectuée avec succès !", 'Succès');
                        $rootScope.publication = undefined;
                        $state.reload()
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('upload err', err);
                }, function (evt) {
                    console.log('upload progress', evt);
                });

        }


        function getTexte(text) {
            $scope.texte = text;
            $rootScope.publication.texte = new Blob([text]);
            console.log('texteSize', $scope.publication.texte.size);
            dialogText.modal('hide')
        }

        function sendRangeDate(p, startDate, endDate) {
            console.log('publication modifiée', p);
            console.log('startDate', $rootScope.startDateTimePicker.startDate.format('DD-MM-YYYY HH:mm'));
            console.log('endDate', $rootScope.endDateTimePicker.endDate.format('DD-MM-YYYY HH:mm'));

            let pub = {
                id: p.id,
                dateDebutPub: startDate.format('DD-MM-YYYY HH:mm'),
                dateFinPub: endDate.format('DD-MM-YYYY HH:mm'),
                idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel
            };

            console.log('pubToSend', pub);

            //return;

            EspacePubService.updatePublication(pub)
                .then(function (res) {
                    console.log('upload res', res);
                    if (!res.data.hasError){
                        toastr.success("Soumission effectuée avec succès !", 'Succès');
                        $rootScope.publication = undefined;
                        getPublications();
                        dialogSendPublication.modal('hide');
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('upload err', err);
                });
        }

        function showDialogText() {
            dialogText.modal('show')
        }

        function getPriceForModal (p) {

            let ts = 0;
            let tp;
            _.forEach(p.typePublicationDtos, function (s) {
                console.log('s.taille=====', s.taille);
                ts += parseInt(s.taille);
            });
            tp = getPrice(ts);
            ts = converteOctet2Ko(ts);

            return {
                totalSize: ts,
                totalPrice: tp
            };
        }

        function showDialogSendPublication(p) {
            $rootScope.publicationModal = angular.copy(p);
            $rootScope.publicationPrice = getPriceForModal(p);
            console.log('$rootScope.publicationPrice', $rootScope.publicationPrice);
            dialogSendPublication.modal('show')
        }

        function converteOctet2Ko(n) {
            return (n/1024) <= 1 ? 1 : Math.floor(n/1024)   //Pour le texte si la convertion en Ko est < 1 on met 1Ko
        }

        function getPrice(sizeFileToKo) {
            return (converteOctet2Ko(sizeFileToKo) * FILE_PRICE_BY_OCTET)
        }

        function getGlobalSize() {
            let video = !$scope.video || 0;
            let filename = !$scope.filename || 0;
            let audio = !$scope.audio || 0;
            let texte = !$scope.texte || 0; //0 pour la taille du texte pour l'instant
            console.log('ici 1');

            return converteOctet2Ko(video) + converteOctet2Ko(filename) + converteOctet2Ko(audio) + converteOctet2Ko(texte)
        }

        function getGlobalPrice() {
            let totalSize;
            let totalAmount;

            totalSize = getGlobalSize();
            totalAmount = getPrice(totalSize);

            console.log('ici 2');

            return totalAmount;
        }

        function modifyPub(p) {
            $rootScope.publication = {}; //Réinitialise au cas où la validation des types pub n'a pas été finalisée
            $rootScope.modifyingPub = p;
            console.log('$scope.modifyingPub', $rootScope.modifyingPub);
        }

        //new Blob(["😀"]).size

        //console.log('blob size', new Blob(["~"]).size)

    }
})();