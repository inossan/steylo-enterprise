(function () {
    'use strict';

    angular.module('app.espace-publicitaire')
        .controller('historiqueCtrl', historiqueCtrl);

    historiqueCtrl.$inject = ['$rootScope', '$scope', 'EspacePubService', 'toastr'];
    function historiqueCtrl($rootScope, $scope, EspacePubService, toastr) {

        $scope.getPublications = getPublications();
        $scope.searchPublications = searchPublications;


        $scope.currentPage = 1;
        $scope.currentPageS = 1;
        $scope.sizePublication = 5;
        $scope.max_size = 3;

        // Configurations date-range-picker-----------------------
        $scope.datePicker = {
            startDate: moment().subtract(1, "days"),
            endDate: moment()
        };

        $scope.options = {
            applyClass: 'btn-success',
            cancelClass: 'btn-secondary',
            opens: 'left',
            //timePicker: true,
            //timePicker24Hour: true,
            autoApply: true,
            //timePickerSeconds: true,
            locale: {
                applyLabel: "Choisir",
                fromLabel: "De",
                //format: "YYYY-MM-DD", //will give you 2017-01-06
                //format: "D-MMM-YY", //will give you 6-Jan-17
                format: "DD MMMM YYYY", //will give you 6 January 2017
                toLabel: "à",
                cancelLabel: 'Annuler',
                customRangeLabel: 'Définir intervalle'
            },
            ranges: {
                'Aujourd\'hui': [moment(), moment()],
                'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 derniers jours': [moment().subtract(6, 'days'), moment()],
                '30 derniers jours': [moment().subtract(29, 'days'), moment()],
                'Ce mois': [moment().startOf('month'), moment().endOf('month')],
                'Le mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };
        // Fin de la configuration--------------------------------

        $scope.searching = false;

        function searchPublications(criteria, index, size) {
            console.log('startDate', criteria.startDate.format('DD-MM-YYYY'));
            console.log('endDate', criteria.endDate.format('DD-MM-YYYY'));
            $scope.searching = true;

            let dataCriteria = {};

            dataCriteria.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;
            dataCriteria.startDate = criteria.startDate.format('DD-MM-YYYY');
            dataCriteria.endDate = criteria.endDate.format('DD-MM-YYYY');
            dataCriteria.isReady = 1;

            EspacePubService.searchPublications(dataCriteria, index, size)
                .then(function (res) {
                    console.log('res search publications', res);
                    if (!res.data.hasError){
                        $scope.publications = res.data.itemsPublication;
                        $scope.totalPublications = res.data.count;
                        if (res.data.count === 0 && !criteria.libelle){
                            toastr.info('Aucune publication trouvée pour cette recherche !', 'Information');
                        }
                        console.log('$scope.totalPublications', $scope.totalPublications);
                        console.log('totalPublication', res.data.count);
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err search publications', err);
                });
        }

        function getPublications(index, size) {

            let isReady = 1;      //On recupère que les publications qui ne sont pas encore prêtes

            EspacePubService.getFullPublications(isReady, index, size)
                .then(function (res) {
                    console.log('getPublications res', res);
                    if (!res.data.hasError){
                        $scope.publications = res.data.itemsPublication;
                        $scope.totalPublications = res.data.count;
                        console.log('$scope.totalPublications', $scope.totalPublications);
                        console.log('$scope.publications', $scope.publications);
                    }else {
                        console.log('Erreur', res.data.status.message);
                    }
                }, function (err) {
                    console.log('getPublications err', err);
                });
        }
    }
})();