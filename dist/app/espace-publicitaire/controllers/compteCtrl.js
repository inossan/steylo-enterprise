(function () {
    'use strict';

    angular.module('app.espace-publicitaire')
        .controller('compteCtrl', compteCtrl);

    compteCtrl.$inject = ['$scope', 'EspacePubService'];
    function compteCtrl($scope, EspacePubService) {

        $scope.exporterCsv = exporterCsv;
        $scope.getHistoAchatCredit = getHistoAchatCredit;
        $scope.showModalAchatCredit = showModalAchatCredit;

        $scope.currentPage = 1;
        $scope.sizeProduit = 6;
        $scope.max_size = 3;

        const achatCredit = $('#achat-credit');

        // Configurations date-range-picker-----------------------
        $scope.datePicker = {
            startDate: moment().subtract(1, "days"),
            endDate: moment()
        };

        $scope.options = {
            applyClass: 'btn-success',
            cancelClass: 'btn-secondary',
            opens: 'center',
            //timePicker: true,
            //timePicker24Hour: true,
            autoApply: false,
            //timePickerSeconds: true,
            locale: {
                applyLabel: "Choisir",
                fromLabel: "De",
                //format: "YYYY-MM-DD", //will give you 2017-01-06
                //format: "D-MMM-YY", //will give you 6-Jan-17
                format: "DD MMMM YYYY", //will give you 6 January 2017
                toLabel: "à",
                cancelLabel: 'Annuler',
                customRangeLabel: 'Définir intervalle'
            },
            ranges: {
                'Aujourd\'hui': [moment(), moment()],
                'Hier': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '7 derniers jours': [moment().subtract(6, 'days'), moment()],
                '30 derniers jours': [moment().subtract(29, 'days'), moment()],
                'Ce mois': [moment().startOf('month'), moment().endOf('month')],
                'Le mois dernier': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };
        // Fin de la configuration--------------------------------



        // ------------------------------------Les fonctions------------------------------------

        function exporterCsv() {

            console.log('exporterCsv');

            return;

            EspacePubService.exporterCSV()
                .then(function (res) {
                    console.log('res achat export csv', res);
                    if (!res.data.hasError){
                        let file = new Blob([res.data], { type: 'text/csv' });
                        if (file.size === 0){
                            toastr.info('Aucune donnée à exporter !', 'Information');
                            return;
                        }
                        saveAs(file, 'Historique-Achat');
                        toastr.success('Exportation effectuée avec succès!', 'Succès');
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err achat export csv', err);
                });
        }

        function getHistoAchatCredit() {
            console.log('getHistoAchatCredit');
        }

        function showModalAchatCredit() {
            achatCredit.modal('show');
        }
    }
})();