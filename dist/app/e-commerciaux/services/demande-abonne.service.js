(function () {
    'use strict';

    angular.module('app.ecommerciaux')
        .factory('demandeAbonneService', demandeAbonneService);

    demandeAbonneService.$inject = ['$rootScope', '$http'];
    function demandeAbonneService($rootScope, $http){
        let demandeAbonneService = {};

        demandeAbonneService.getAbonneDemande = getAbonneDemande;
        demandeAbonneService.getAbonneLocation = getAbonneLocation;
        demandeAbonneService.updateAbonneDemande = updateAbonneDemande;
        demandeAbonneService.getAbonne = getAbonne;
        demandeAbonneService.searchDemandeur = searchDemandeur;

        return demandeAbonneService;

        //Voir comment faire pour recupérer les localisations dans backend
        function getAbonneLocation(demandeState) {
            let req = {
                dataAbonneDemande: {
                    idChannel: $rootScope.globals.currentUser.chanelUserInfos.idChanel,
                    demandeState: demandeState
                }
            };
            let URL = BASE_URL + '/abonneDemande/getByCriteria';
            return $http.post(URL, req, {cache: true});
        }

        function getAbonneDemande(demandeState, index, size, showLoadingBar) {
            let slb = (showLoadingBar === 'no-loading-bar');
            let req = {
                dataAbonneDemande: {
                    idChannel: $rootScope.globals.currentUser.chanelUserInfos.idChanel,
                    demandeState: demandeState
                },
                index: index,
                size: size
            };
            let URL = BASE_URL + '/abonneDemande/getByCriteria';
            return $http.post(URL, req, {
                ignoreLoadingBar: slb,
                cache: true
            });
        }

        function updateAbonneDemande(demandeAbonne) {
            let req = {
                datasAbonneDemande: [demandeAbonne]
            };
            let URL = BASE_URL + '/abonneDemande/update';
            return $http.post(URL, req, {cache: true});
        }

        function getAbonne(id) {
            let req = {
                dataAbonne: {
                    id: id
                }
            };
            let URL = BASE_URL + '/abonne/getByCriteria';
            return $http.post(URL, req, {cache: true});
        }
        
        function searchDemandeur(criteria, index, size) {
            let req = {
                dataAbonneDemande: criteria,
                index: index || 0,
                size: size || 5
            };
            let URL = BASE_URL + '/abonneDemande/getByCriteria';
            return $http.post(URL, req, {
                cache: true
            });
        }
    }
})();