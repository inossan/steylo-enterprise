(function () {
    'use strict';

    angular.module('app.ecommerciaux')
        .controller('ecomDetailsCtrl', ecomDetailsCtrl);

    ecomDetailsCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'demandeAbonneService', 'notificationService', 'toastr', 'AclService'];
    function ecomDetailsCtrl($rootScope, $scope, $state, $stateParams, demandeAbonneService, notificationService, toastr, AclService) {

        $scope.can = AclService.can;
        $scope.getDetailsAbonne = getDetailsAbonne($stateParams.idAbonne);
        $rootScope.decisionDemand = decisionDemand;
        $scope.showPopupMotifRejet = showPopupMotifRejet;

        console.log('$stateParams', $stateParams);
        //$stateParams = {};

        let modalMotifRejet = $('#motifRejet');
        function showPopupMotifRejet() {
            modalMotifRejet.modal('show');
        }

        function getDetailsAbonne(idDemandeur) {
            demandeAbonneService.getAbonne(idDemandeur)
                .then(function (res) {
                    if (!res.data.hasError){
                        let demande = {};
                        $rootScope.abonneDetails = res.data.itemsAbonne[0];
                        if ($rootScope.abonneDetails.photoIdentite){
                            $rootScope.abonneDetails.photoIdentite = getImageFromBase64($scope.abonneDetails)
                        }
                        demande.id = $stateParams.idDemande; //on injecte l'id de sa demande
                        demande.demandeState = 0;
                        demande.loadSpinAccept = false;
                        $rootScope.abonneDetails.demande = demande;
                        getMap($rootScope.abonneDetails);
                        console.log('infoAbonné', $rootScope.abonneDetails);
                    }
                });
        }

        function getMap(abonneDetails) {
            console.log('abonneDetails.latitude', abonneDetails.latitude);
            console.log('abonneDetails.longitude', abonneDetails.longitude);
            let marker;
            let center = new google.maps.LatLng(parseFloat(abonneDetails.latitude), parseFloat(abonneDetails.longitude));
            let map = new google.maps.Map(document.getElementById('mapDetails'), {
                zoom: 15,
                center: center,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                //styles: [{"stylers": [{ "saturation": 100 }]}],
                /*/
                zoomControl: boolean,
                mapTypeControl: boolean,
                scaleControl: boolean,
                rotateControl: boolean,//*/
                streetViewControl: false,
                fullscreenControl: false

            });

            marker = new google.maps.Marker({
                map: map,
                //draggable: true,
                animation: google.maps.Animation.DROP,
                position: center,
                //icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
                //icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
                //icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
                //position: {lat: parseFloat(latitude), lng: parseFloat(longitude)}
            });
            marker.addListener('click', toggleBounce);

            function toggleBounce() {
                if (marker.getAnimation() !== null) {
                    marker.setAnimation(null);
                } else {
                    marker.setAnimation(google.maps.Animation.BOUNCE);
                }
                //infowindow.open(map, marker);
            }

            console.log('ok');
            //google.maps.event.trigger(map, "resize");
            //return map.setCenter(center);
        }

        function getImageFromBase64(img) {
            switch (img.extPhotoIdentite){
                case 'png':
                    return 'data:image/png;base64,' + img.photoIdentite;
                case 'jpeg':
                    return 'data:image/jpeg;base64,' + img.photoIdentite;
                case 'jpg':
                    return 'data:image/jpg;base64,' + img.photoIdentite;
                case 'gif':
                    return 'data:image/gif;base64,' + img.photoIdentite;
            }
        }

        function decisionDemand(demande, decision) {
            console.log('demande', demande);
            console.log('decision', decision);

            switch (decision){
                case 'accept':
                    demande.loadSpinAccept = true;
                    demande.demandeState = 1;   //Validé
                    demande.dateDecision = moment(new Date()).format('DD-MM-YYYY HH:mm:ss');
                    demande.statut = 1;
                    break;
                case 'reject':
                    if (!demande.hasOwnProperty('motifRejet')){
                        toastr.error('Veuillez indiquer un motif pour le rejet svp !');
                        return;
                    }
                    if (demande.motifRejet === ''){
                        toastr.error('Le motif n\'est pas renseigné !', 'Erreur', {
                            positionClass: 'toast-top-right'
                        });
                        return;
                    }
                    demande.loadSpinReject = true;
                    demande.demandeState = 3;   //Rejeté
                    demande.dateDecision = moment(new Date()).format('DD-MM-YYYY HH:mm:ss');
                    demande.statut = 2;
                    break;
            }
            demandeAbonneService.updateAbonneDemande(demande)
                .then(function (res) {
                    if (!res.data.hasError){
                        console.log('res demande Abonné', res);
                        if (res.data.itemsAbonneDemande[0]){
                            if (res.data.itemsAbonneDemande[0].demandeState === 1){
                                notificationService.flatty.success('La demande a été validée !');
                            }
                            if (res.data.itemsAbonneDemande[0].demandeState === 3){
                                notificationService.flatty.error('La demande a été rejetée !');
                            }
                        }
                        $rootScope.$stateParams = {};
                        $('[data-toggle="tooltip"]').tooltip('dispose');
                        $state.go('app.ecommerciaux.demandeurs');
                        //$state.reload();
                        modalMotifRejet.modal('hide');
                        $rootScope.modalInfoAbonne.modal('hide');
                        $rootScope.getDemandeAbonne();
                    }else {
                        notificationService.flatty.error('Erreur interne\nLa validation a échoué !');
                    }
                }, function (err) {
                    console.log('err demande Abonné', err);
                });
        }
    }
})();