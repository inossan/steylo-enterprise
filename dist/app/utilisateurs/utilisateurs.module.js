(function () {
    'use strict';

    angular.module('app.utilisateurs', [
        'app.commons'
    ])
        .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {

            $ocLazyLoadProvider.config({
                // Set to true if you want to see what and when is dynamically loaded
                debug: true
            });

            $urlRouterProvider.otherwise('/login');
            $stateProvider
                .state('app.utilisateurs', {
                    url: '/users',
                    templateUrl: 'app/utilisateurs/views/utilisateurs.html',
                    ncyBreadcrumb: {
                        label: 'Utilisateurs'
                    },
                    controller: 'usersCtrl',
                    resolve: {
                        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                            // you can lazy load controllers
                            return $ocLazyLoad.load({
                                files: [
                                    'app/utilisateurs/services/profil.service.js',
                                    'app/utilisateurs/services/utilisateurs.service.js',
                                    'app/utilisateurs/services/sous-fonctionnalite.service.js',
                                    'app/utilisateurs/controllers/utilisateursCtrl.js',
                                    //'app/utilisateurs/controllers/profilsCtrl.js',
                                    'app/utilisateurs/controllers/utilisateursCtrl.js',
                                    'app/utilisateurs/controllers/fonctionnalitesCtrl.js',
                                ]
                            });
                        }]
                    },
                    redirectTo: 'app.utilisateurs.users',  //permet de faire une redirection à la sous vue
                })
                .state('app.utilisateurs.users', {
                    url: '/manage-users',
                    ncyBreadcrumb: {
                        label: 'Gérer les utilisateurs'
                    },
                    views: {
                        'ec_sub_menu': {
                            templateUrl: 'app/utilisateurs/views/manage-users.html',
                            controller: 'usersCtrl'
                        }
                    },

                })
                .state('app.utilisateurs.profils', {
                    url: '/profils',
                    ncyBreadcrumb: {
                        label: 'Gérer les profils'
                    },
                    views: {
                        'ec_sub_menu': {
                            templateUrl: 'app/utilisateurs/views/manage-profils.html',
                            controller: 'profilsCtrl'
                        }
                    },

                })
                .state('app.utilisateurs.fonctionnalites', {
                    url: '/fonctionnalites',
                    ncyBreadcrumb: {
                        label: 'Gérer les fonctionnalites'
                    },
                    views: {
                        'ec_sub_menu': {
                            templateUrl: 'app/utilisateurs/views/manage-fonctionnalites.html',
                            controller: 'fonctionnalitesCtrl'
                        }
                    },

                })
        }])
})();