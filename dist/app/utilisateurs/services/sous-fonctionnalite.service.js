(function () {
    'use strict';

    angular.module('app.utilisateurs')
        .factory('SousFonctionnaliteService', SousFonctionnaliteService);

    SousFonctionnaliteService.$inject = ['$rootScope', '$http'];
    function SousFonctionnaliteService($rootScope, $http) {

        const sousFonctionnaliteService = {};

        sousFonctionnaliteService.createSousFonctionnalite = createSousFonctionnalite;
        sousFonctionnaliteService.updateSousFonctionnalite = updateSousFonctionnalite;
        sousFonctionnaliteService.deleteSousFonctionnalite = deleteSousFonctionnalite;
        sousFonctionnaliteService.getSousFonctionnalites = getSousFonctionnalites;

        return sousFonctionnaliteService;

        function createSousFonctionnalite(sf) {
            let request = {
                datasPermissionActions: [sf]
            };
            let URL = BASE_URL + '/permissionActions/create';
            return $http.post(URL, request, {cache: true});
        }

        function updateSousFonctionnalite(sf) {
            let request = {
                datasPermissionActions: [sf]
            };
            let URL = BASE_URL + '/permissionActions/update';
            return $http.post(URL, request, {cache: true});
        }

        function deleteSousFonctionnalite(sf) {
            let request = {
                datasPermissionActions: [sf]
            };
            let URL = BASE_URL + '/permissionActions/delete';
            return $http.post(URL, request, {cache: true});
        }

        function getSousFonctionnalites(index, size) {
            let request = {
                dataPermissionActions:{

                },
                index: index || 0,
                size: size || 5
            };
            let URL = BASE_URL + '/permissionActions/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }
    }
})();