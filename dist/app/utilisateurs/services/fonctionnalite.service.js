(function () {
    'use strict';

    angular.module('app.utilisateurs')
        .factory('FonctionnaliteService', FonctionnaliteService);

    FonctionnaliteService.$inject = ['$rootScope', '$http'];
    function FonctionnaliteService($rootScope, $http) {

        const fonctionnaliteService = {};

        //Lier a permissionsActions
        fonctionnaliteService.createFonctionnalite = createFonctionnalite;
        fonctionnaliteService.updateFonctionnalite = updateFonctionnalite;
        fonctionnaliteService.deleteFonctionnalite = deleteFonctionnalite;
        fonctionnaliteService.getFonctionnalite = getFonctionnalites;
        fonctionnaliteService.getAllFonctionnalites = getAllFonctionnalites;

        return fonctionnaliteService;

        function createFonctionnalite(p) {

        }

        function updateFonctionnalite(p) {

        }

        function deleteFonctionnalite(p) {

        }

        function getFonctionnalites(index, size) {
            let request = {
                dataChanelPermissions:{

                },
                index: index || 0,
                size: size || 10
            };
            let URL = BASE_URL + '/chanelPermissions/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }

        function getAllFonctionnalites() {
            let request = {
                dataChanelPermissions:{

                }
            };
            let URL = BASE_URL + '/chanelPermissions/getAll';
            return $http.post(URL, request, {cache: true});
        }
    }
})();