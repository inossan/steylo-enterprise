(function () {
    'use strict';

    angular.module('app.utilisateurs')
        .controller('profilsCtrl', profilsCtrl);

    profilsCtrl.$inject = ['$rootScope', '$scope', 'ProfilService', 'FonctionnaliteService', 'toastr', 'AclService'];
    function profilsCtrl($rootScope, $scope, ProfilService,FonctionnaliteService, toastr, AclService) {

        $scope.can = AclService.can;
        $rootScope.getProfiles = getProfiles();
        $scope.loadPermissions = loadPermissions;
        $scope.createProfil = createProfil;
        $scope.updateProfil = updateProfil;
        $scope.deleteProfil = deleteProfil;
        $scope.getFonctionnalites = getFonctionnalites();
        $scope.showAddProfil = showAddProfil;
        $scope.showEditProfil = showEditProfil;
        $scope.closeModalAdd = closeModalAdd;
        $scope.closeModalEdit = closeModalEdit;
        $scope.readyCB = readyCB;

        let modalAddProfil = $('#add-profil');
        let modalEditProfil = $('#edit-profil');

        let treeInstance = angular.element('#jstreeContainer');
        let treeInstanceUpdate = angular.element('#jstreeContainerUpdate');

        $scope.currentPageP = 1;
        $scope.sizePermission = 5;
        $scope.max_size = 1;

        //Pagination côté front
        $scope.$watch("currentPageP", function() {
            setPagingData($scope.currentPageP);
        });

        function setPagingData(page) {

            if ($scope.listFonctionnalities){

                let pagedData = $scope.listFonctionnalities.slice(
                    (page - 1) * $scope.sizePermission,
                    page * $scope.sizePermission
                );

                $scope.listF = pagedData;
            }
        }

        function closeModalAdd() {
            $scope.profil = {};
            if (modalAddProfil.modal('show')) {
                treeInstance.jstree(true).deselect_all();
                treeInstance.jstree(true).close_all();
                modalAddProfil.modal('hide');
            }
        }

        function closeModalEdit() {
            $scope.profil = {};
            if (modalEditProfil.modal('show')) {
                treeInstanceUpdate.jstree(true).deselect_all();
                treeInstanceUpdate.jstree(true).close_all();
                modalEditProfil.modal('hide');
            }
        }

        function showAddProfil() {
            modalAddProfil.modal('show');
        }

        function showEditProfil(p) {

            console.log('p update', p);
            //return;
            $rootScope.editingProfil = angular.copy(p);
            $rootScope.editingProfilTitle = p.libelle;

            //loadPermissions(p);
            //on charge les fonctionnalités
            readyCB(p.listFonctionnalities, 'forUpdate');

            modalEditProfil.modal('show');
        }

        function loadPermissions(profil, selected) {
            $scope.selectedProfil = selected;
            $scope.listFonctionnalities = profil.listFonctionnalities;
            $scope.totalListFonctionnalities = profil.listFonctionnalities.length;
            setPagingData(1) //A l'initialisation
        }

        function getFonctionnalites() {
            FonctionnaliteService.getAllFonctionnalites()
                .then(function (res) {
                    console.log('getFonctionnalites res', res);
                    if (!res.data.hasError){
                        $scope.allFonctionnalites = res.data.itemsChanelPermissions;
                        jsTree($scope.allFonctionnalites);
                    }else {
                        console.log('Erreur getFonctionnalites', res.data.status.message);
                    }
                }, function (err) {
                    console.log('getFonctionnalites err', err);
                });

        }

        function readyCB(p, where) {

            if (where === 'forUpdate'){
                console.log('$scope.listFonctionnalities', p);

                let group = _.groupBy(p, function (g) {
                    return g.libelleChanelPermission
                });
                console.log('group', group);

                //return;

                let children = [];

                _.forOwn(group, function (value, key) { //Pour boucler sur les proprietés d'un objet

                    if (value.length > 0){   //For children
                        _.forEach(value, function (f) {
                            children.push({
                                id: f.idPermissionAction,
                                parent: key,
                                text: f.libellePermissionAction,
                                state: {
                                    opened: true
                                },
                                type: 'cog'
                            });
                        });
                    }
                });

                treeInstanceUpdate.jstree(true).select_node(children);
                //treeInstanceUpdate.jstree(true).close_all(); //A commenter si scrollbar
                console.log('children', children);
            }
        }

        function jsTree(fonctionnaites) {
            //Pour JSTREE - Ne jamais utiliser les entiers pour constituer les 'id' et 'parent'

            $rootScope.treeConfig = {
                core: {
                    multiple: true, //Pour permettre la selection multiple
                    animation: true,
                    error: function(error) {
                        console.log('treeCtrl: error from js tree - ' + angular.toJson(error));
                    },
                    check_callback: true,
                    worker: true
                },
                types: {
                    default: {
                        icon: 'fa fa-snowflake-o'
                    },
                    bolt: {
                        icon: 'fa fa-bolt'
                    },
                    cogs: {
                        icon: 'fa fa-cogs'
                    },
                    cog: {
                        icon: 'fa-spin fa fa-cog'
                    }
                },
                version: 1,
                plugins: ['types', 'checkbox']
            };
            let parents = [];
            //let children = [];

            _.forEach(fonctionnaites, function (af) {
                parents.push({  //For parent
                    id: af.libelle,
                    text: af.libelle,
                    state: {
                        opened: false
                    },
                    type: 'cogs',
                    children: _.map(af.listPermissionActions, function (f) {
                        return {
                            id: f.id,
                            parent: af.libelle,
                            text: f.libelle,
                            state: {
                                opened: true
                            },
                            type: 'cog'
                        }
                    })
                });
                /*
                if (af.listPermissionActions.length > 0){   //For children
                    _.forEach(af.listPermissionActions, function (f) {
                        children.push({
                            id: f.id,
                            parent: af.libelle,
                            text: f.libelle,
                            state: {
                                opened: true
                            },
                            type: 'cog'
                        });
                    });
                }
                */
            });

            $rootScope.treeData = parents;//.concat(children);
            console.log('$scope.treeData', $scope.treeData);
        }

        function getProfiles() {
            ProfilService.getProfils()
                .then(function (res) {
                    console.log('profils', res);
                    if (!res.data.hasError){
                        $rootScope.profils = res.data.itemsPermissionProfil;
                        $rootScope.totalProfils = res.data.count;
                    }
                });
        }

        function createProfil(p) {

            if (!p){
                p = {};
            }
            if (!p.hasOwnProperty('libelle') || p.libelle === ''){
                toastr.error('Veuillez renseigner le libellé du profil !', 'Erreur');
                return;
            }
            if (treeInstance.jstree(true).get_selected(true).length < 1){
                toastr.error('Veuillez attribuer des permissions !', 'Erreur');
                return;
            }

            //console.log('treeInstance.jstree(true).get_selected()', treeInstance.jstree(true).get_selected(true));
            //return;

            //Pour recupérer les noeuds selectionnés
            let listFonctionnalities;

            listFonctionnalities = _.map(treeInstance.jstree(true).get_selected(true), function (node) {
                console.log('node.original.parent', node.original.parent);
                return {
                    libelleChanelPermission: node.original.parent,  //Parent
                    idPermissionAction: node.original.id    //Child
                }
            });

            listFonctionnalities = _.filter(listFonctionnalities, function (lf) {
                return lf.libelleChanelPermission;
            });

            console.log('selected_nodes', listFonctionnalities);

            //return;
/*

            let group = _.groupBy(listFonctionnalities, function (g) {
                return g.parent
            });
            console.log('group', group);
*/

            //Données à envoyer
            let profil = {};
            profil.libelle = p.libelle;
            profil.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;
            profil.listFonctionnalities = listFonctionnalities;

            //return;
            ProfilService.createProfil(profil)
                .then(function (res) {
                    console.log('create profils res', res);
                    if (!res.data.hasError){
                        toastr.success('Enregistrement effectué avec succès !', 'Succès');
                        $scope.profil = {};
                        treeInstance.jstree(true).deselect_all();
                        treeInstance.jstree(true).close_all();
                        getProfiles();
                        modalAddProfil.modal('hide');
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('create profils err', err);
                });
        }

        function updateProfil(p) {

            console.log('p test', p);

            $('[data-toggle="tooltip"]').tooltip('hide');

            if (!p){
                p = {};
            }
            if (!p.hasOwnProperty('libelle') || p.libelle === ''){
                toastr.error('Veuillez renseigner le libellé du profil !', 'Erreur');
                return;
            }
            if (treeInstanceUpdate.jstree(true).get_selected(true).length < 1){
                toastr.error('Veuillez attribuer des permissions !', 'Erreur');
                return;
            }

            //console.log('treeInstance.jstree(true).get_selected()', treeInstance.jstree(true).get_selected(true));
            //return;

            //Pour recupérer les noeuds selectionnés
            let listFonctionnalities;

            listFonctionnalities = _.map(treeInstanceUpdate.jstree(true).get_selected(true), function (node) {
                console.log('node.original.parent', node.original.parent);
                return {
                    libelleChanelPermission: node.original.parent,  //Parent
                    idPermissionAction: node.original.id    //Child
                }
            });

            listFonctionnalities = _.filter(listFonctionnalities, function (lf) {
                return lf.libelleChanelPermission;
            });

            console.log('selected_nodes', listFonctionnalities);

            //Données à envoyer
            let profil = {};
            profil.id = p.id;   //idProfil à update
            profil.libelle = p.libelle;
            profil.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;
            profil.listFonctionnalities = listFonctionnalities;

            //return;
            ProfilService.updateProfil(profil)
                .then(function (res) {
                    console.log('update profils res', res);
                    if (!res.data.hasError){
                        treeInstanceUpdate.jstree(true).deselect_all();
                        treeInstanceUpdate.jstree(true).close_all();
                        getProfiles();
                        toastr.success('Modification effectuée avec succès !', 'Succès');

                        modalEditProfil.modal('hide');
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('update profils err', err);
                });
        }

        function deleteProfil(p) {

            $('[data-toggle="tooltip"]').tooltip('hide');

            ProfilService.deleteProfil(p)
                .then(function (res) {
                    console.log('delete profils res', res);
                    if (!res.data.hasError) {
                        toastr.success('Suppression effectuée avec succès !', 'Succès');
                        getProfiles();
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('delete profils err', err);
                });
        }

    }
})();