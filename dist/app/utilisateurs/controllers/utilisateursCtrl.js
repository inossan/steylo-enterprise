(function () {
    'use strict';

    angular.module('app.utilisateurs')
        .controller('usersCtrl', usersCtrl);

    usersCtrl.$inject = ['$rootScope', '$scope', 'ProfilService', 'UtilisateurService', 'commonUtilities', 'toastr', 'notificationService', AclService];
    function usersCtrl($rootScope, $scope, ProfilService, UtilisateurService, commonUtilities, toastr, notificationService, AclService) {

        $scope.can = AclService.can;
        $scope.getUsers = getUsers;
        getUsers();
        $scope.enableOrDisableUser = enableOrDisableUser;
        $scope.createUser = createUser;
        $rootScope.updateUser = updateUser;
        $scope.deleteUser = deleteUser;
        $scope.searchUsers = searchUsers;
        $scope.showModalEditUser = showModalEditUser;

        $scope.windowHeightP = $(window).innerHeight() - 260;

        $scope.currentPage = 1;
        $scope.sizeUser = 5;
        $scope.max_size = 3;

        $scope.searching = false;

        let modalEditUser = $('#edit-user');
        //$rootScope.deleteTooltipIntrusive();

        function showModalEditUser(u) {
            $rootScope.editingUser = angular.copy(u);
            $rootScope.editingUserTitle = u.login;
            modalEditUser.modal('show');
        }

        function searchUsers(criteria, index, size) {
            console.log('criteria', criteria);
            $scope.searching = true;

            criteria.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;
            UtilisateurService.searchUsers(criteria, index, size)
                .then(function (res) {
                    console.log('res search users', res);
                    if (!res.data.hasError){
                        $scope.users = res.data.itemsChanelUsers;
                        $scope.totalUsers = res.data.count || 0;

                        console.log('totalUsers', res.data.count);
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err search users', err);
                });
        }

        function getUsers(index, size) {
            UtilisateurService.getUsers(index, size)
                .then(function (res) {
                    console.log('getUsers res', res);
                    if (!res.data.hasError){
                        //$scope.users = res.data.itemsChanelUsers;
                        $scope.users = _.filter(res.data.itemsChanelUsers, function (u) {
                            return u.id !== $rootScope.globals.currentUser.chanelUserInfos.id
                        });
                        $scope.totalUsers = (res.data.count - 1) || 0;
                        console.log('$scope.users res', $scope.users);

                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('getUsers err', err);
                });
        }
        
        function enableOrDisableUser(user, choix) {
            let p = {};
            p.id = user.id;

            switch (choix){
                case 'activer':
                    p.statut = 1;
                    break;
                case 'desactiver':
                    p.statut = 0;
                    break;
            }
            UtilisateurService.enableDisableUser(p)
                .then(function (res) {
                    console.log('res update enableOrDisableUser', res);
                    if (!res.data.hasError){
                        if (res.data.itemsChanelUsers[0].statut === 1){
                            $scope.etatUser = 'desactiver';
                            notificationService.flatty.success('<b><i class="fa fa-check"></i>&nbsp;L\'utilisateur a bien été activé !</b>');
                        }
                        if (res.data.itemsChanelUsers[0].statut === 0){
                            $scope.etatUser = 'activer';
                            notificationService.flatty.error('<b><i class="fa fa-ban"></i>&nbsp;L\'utilisateur a bien été désactivé !</b>');
                        }
                        getUsers();

                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err update enableOrDisableUser', err);
                });
        }


        function createUser(user) {

            console.log('user', user);

            if (!user){
                user = {};
            }
            if (!user.hasOwnProperty('login') || user.login === ''){
                toastr.error('Veuillez renseigner l\'identifiant !', 'Erreur');
                return;
            }
            if (!user.hasOwnProperty('idProfil') || user.idProfil === ''){
                toastr.error('Veuillez renseigner le profil !', 'Erreur');
                return;
            }
            if (!user.hasOwnProperty('password') || user.password === ''){
                toastr.error('Veuillez renseigner le mot de passe !', 'Erreur');
                return;
            }
            if (!user.hasOwnProperty('passwordConf') || user.passwordConf === ''){
                toastr.error('Veuillez renseigner la confirmation du mot de passe !', 'Erreur');
                return;
            }
            if (user.password !== user.passwordConf){
                toastr.error('La confirmation du mot de passe est incorrecte !', 'Erreur');
                return;
            }
            if (!user.hasOwnProperty('email') || user.email === ''){
                toastr.error('Veuillez renseigner l\'email !', 'Erreur');
                return;
            }
            if (!commonUtilities.validateEmail(user.email)){
                toastr.error('L\'email est incorrect !', 'Erreur');
                return;
            }

            user.createdBy = $rootScope.globals.currentUser.chanelUserInfos.id;
            user.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;

            UtilisateurService.createUser(user)
                .then(function (res) {
                    console.log('createUser res', res);
                    if (!res.data.hasError){
                        $scope.userFormSubmited = true;
                        $scope.user = {};
                        getUsers();
                        toastr.success('Enregistrement effectué avec succès !', 'Succès');
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('createUser err', err);
                });
        }

        function updateUser(user) {

            $('[data-toggle="tooltip"]').tooltip('hide');

            console.log('editingUser', user);

            //return;

            if (!user){
                user = {};
            }
            if (!user.hasOwnProperty('login') || user.login === ''){
                toastr.error('Veuillez renseigner l\'identifiant !', 'Erreur');
                return;
            }
            if (!user.hasOwnProperty('email') || user.email === ''){
                toastr.error('Veuillez renseigner l\'email !', 'Erreur');
                return;
            }
            if (!commonUtilities.validateEmail(user.email)){
                toastr.error('L\'email est incorrect !', 'Erreur');
                return;
            }

            UtilisateurService.updateUser(user)
                .then(function (res) {
                    console.log('updateUser res', res);
                    if (!res.data.hasError){
                        getUsers();
                        toastr.success('Modification effectuée avec succès !', 'Succès');
                        modalEditUser.modal('hide');
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('updateUser err', err);
                });
        }

        function deleteUser(user) {

            $('[data-toggle="tooltip"]').tooltip('hide');

            UtilisateurService.deleteUser(user)
                .then(function (res) {
                    console.log('deleteUser res', res);
                    if (!res.data.hasError){
                        getUsers();
                        toastr.success('Suppression effectuée avec succès !', 'Succès');
                    }else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('deleteUser err', err);
                });
        }

    }
})();