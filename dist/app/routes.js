angular
.module('app')
.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {

  $urlRouterProvider.otherwise('/login');

  $ocLazyLoadProvider.config({
      // Set to true if you want to see what and when is dynamically loaded
      debug: true
  });

  $breadcrumbProvider.setOptions({
      prefixStateName: 'app.main',
      includeAbstract: true,
      template: '<li class="breadcrumb-item small" ng-repeat="step in steps" ng-class="{active: $last}" ng-switch="$last || !!step.abstract">' +
      '<a ng-switch-when="false" href="{{step.ncyBreadcrumbLink}}">{{step.ncyBreadcrumbLabel}}</a>' +
      '<span ng-switch-when="true">{{step.ncyBreadcrumbLabel}}</span>' +
      '</li>'
  });

  $stateProvider
      .state('app', {
          abstract: true,
          templateUrl: 'app/views/common/layouts/full.html',
          //page title goes here
          ncyBreadcrumb: {
              label: 'Root',
              skip: true
          },
          resolve: {
              loadCSS: ['$ocLazyLoad', function($ocLazyLoad) {
                  // you can lazy load CSS files
                  return $ocLazyLoad.load([{
                      serie: true,
                      name: 'Font Awesome',
                      //files: ['vendors/css/font-awesome.min.css']
                      files: ['vendors/css/font-awesome.min.css']
                  },{
                      serie: true,
                      name: 'Simple Line Icons',
                      //files: ['vendors/css/simple-line-icons.min.css']
                      files: ['vendors/css/simple-line-icons.min.css']
                  }]);
              }],
          }
      })
      .state('app.main', {
          url: '/dashboard',
          templateUrl: 'app/dashboard/main.html',
          controller: 'dashboardCtrl',
          //page title goes here
          ncyBreadcrumb: {
              label: 'Accueil',
          },
          //page subtitle goes here
          //params: { subtitle: 'Welcome to ROOT powerfull Bootstrap & AngularJS UI Kit' },
          resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  // you can lazy load controllers
                  return $ocLazyLoad.load({
                      files: [
                          'app/dashboard/main.js',
                          'app/connexion/services/connexion.service.js',
                          'app/connexion/controllers/logoutCtrl.js',
                          'app/commons/utils/modal.service.js',
                      ]
                  });
              }]
          }
      })
      .state('connexion', {
        abstract: true,
        templateUrl: 'app/views/common/layouts/simple.html',
        resolve: {
            loadCSS: ['$ocLazyLoad', function($ocLazyLoad) {
                return $ocLazyLoad.load([{
                    serie: true,
                    name: 'Font Awesome',
                    files: ['vendors/css/font-awesome.min.css']
                    //files: ['vendors/css/font-awesome.min.css']
                },{
                    serie: true,
                    name: 'Simple Line Icons',
                    files: ['vendors/css/simple-line-icons.min.css']
                    //files: ['vendors/css/simple-line-icons.min.css']
                }]);
            }],
            loadService: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    files: ['app/connexion/services/connexion.service.js']
                });
            }]
          }
    })
      .state('connexion.login', {
          url: '/login',
          templateUrl: 'app/connexion/views/login.html',
          //page title goes here
          ncyBreadcrumb: {
              label: 'Login',
          },
          controller: 'loginCtrl',
          //page subtitle goes here
          //params: { subtitle: 'Welcome to ROOT powerfull Bootstrap & AngularJS UI Kit' },
          resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  // you can lazy load controllers
                  return $ocLazyLoad.load({
                      files: ['app/connexion/controllers/loginCtrl.js']
                  });
              }]
          }
      })
      .state('connexion.register', {
          url: '/register',
          templateUrl: 'app/connexion/views/register.html',
          //templateUrl: 'app/connexion/views/terms-and-conditions.html',
          //page title goes here
          ncyBreadcrumb: {
              label: 'Register',
          },
          controller: 'registerCtrl',
          //page subtitle goes here
          //params: { subtitle: 'Welcome to ROOT powerfull Bootstrap & AngularJS UI Kit' },
          resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  // you can lazy load controllers
                  return $ocLazyLoad.load({
                      files: ['app/connexion/controllers/registerCtrl.js']
                  });
              }]
          }
      })
      .state('connexion.terms&conditions', {
          url: '/terms-conditions',
          templateUrl: 'app/connexion/views/terms-and-conditions.html',
          ncyBreadcrumb: {
              label: 'Terms & Conditions',
          },
          controller: 'registerCtrl',
          resolve: {
              loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                  // you can lazy load controllers
                  return $ocLazyLoad.load({
                      files: ['app/connexion/controllers/registerCtrl.js']
                  });
              }]
          }
      })
  /*.state('appSimple.404', {
    url: '/404',
    templateUrl: 'views/pages/404.html'
  })
  .state('appSimple.500', {
    url: '/500',
    templateUrl: 'views/pages/500.html'
  })*/
}]);
