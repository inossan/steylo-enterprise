// Default colors
let brandPrimary =  '#20a8d8';
let brandSuccess =  '#4dbd74';
let brandInfo =     '#63c2de';
let brandWarning =  '#f8cb00';
let brandDanger =   '#f86c6b';

let grayDark =      '#2a2c36';
let gray =          '#55595c';
let grayLight =     '#818a91';
let grayLighter =   '#d1d4d7';
let grayLightest =  '#f8f9fa';

//let BASE_URL = 'http://213.246.56.33:8081/steylo-enterprise'; //A décommenter en prod
//let BASE_URL = 'http://novagy.net:8990/steylo-enterprise';
let BASE_URL = 'http://31.207.34.93:8080/steylo-enterprise';
//let BASE_URL = 'http://127.0.0.1:8081'; //A commenter en prod

let statut_demande = {
    EN_ATTENTE: 0,
    VALIDE: 1,
    REJETE: 3,
};
let statut_commande = {
    EN_ATTENTE: 'E_ATT',
    A_VALIDER: 'A_VAL',
    ANNULEE: 'ANN',
    EN_COURS: 'E_COU',
    ENVOYEE: 'ENV',
    REFUSEE: 'REFU',
    REJETEE: 'REJ',
    REMBOURSEE: 'REMB',
    RETOURNEE: 'RETOU',
    TERMINEE: 'TERM',
};

let FILE_PRICE_BY_OCTET = 100; //fcfa

angular.module('app', [
    // Module applicative
    'app.commons',
    'app.utilisateurs',
    'app.produits',
    'app.parametres',
    'app.ecommerciaux',
    'app.profil',
    'app.suivis',
    'app.espace-publicitaire',

    // Autres modules
    'ui.router',
    'oc.lazyLoad',
    'ncy-angular-breadcrumb',
    'angular-loading-bar',
    'ngAnimate',
    'ngSanitize',
    'mgo-angular-wizard',
    'ui.mask',
    'ngNotify',
    'ui.select',
    'ngLodash',
    'ngJsTree',
    'angular-ladda',

    'toastr',
    'ngMessages',
    'xeditable',/*/
    'ui.bootstrap',
    'ngCookies',
    'ng-fusioncharts',

    'daterangepicker'
    //*/
])
    .config(configuration)
    .run(runBlock);

configuration.$inject = ['cfpLoadingBarProvider', 'toastrConfig', 'laddaProvider', 'modalConfig', 'localStorageServiceProvider', 'AclServiceProvider'];
function configuration(cfpLoadingBarProvider, toastrConfig, laddaProvider, modalConfig, localStorageServiceProvider, AclServiceProvider) {
    cfpLoadingBarProvider.includeSpinner = true;
    cfpLoadingBarProvider.latencyThreshold = 1;
    modalConfig.containerSelector = '#container';

    AclServiceProvider.config({
        storage: 'localStorage'
    });

    localStorageServiceProvider
        .setPrefix('app')
        .setDefaultToCookie(false)
        .setNotify(true, true);

    angular.extend(toastrConfig, {
        extendedTimeOut: 1000,
        messageClass: 'toast-message',
        progressBar: true,
        tapToDismiss: true,
        //positionClass: 'toast-top-center',
        timeOut: 5000,
        titleClass: 'toast-title',
        toastClass: 'toast',
        containerId: 'toast-container',
        maxOpened: 0,
        newestOnTop: true,
        preventDuplicates: false,
        preventOpenDuplicates: false,
        allowHtml: true,
        closeButton: true,
        closeHtml: '<button>&times;</button>',
        showEasing: "swing",
        hideEasing: "linear",
        showMethod: "fadeIn",
        hideMethod: "fadeOut"
    });

    laddaProvider.setOption({ /* optional */
        style: 'expand-left',
        spinnerSize: 35,
        spinnerColor: '#dcef7f'
    });
}

runBlock.$inject = ['$rootScope', '$state', '$stateParams', '$http', '$cookieStore', 'localStorageService', 'ngNotify', 'editableOptions', 'editableThemes', 'AclService'];
function runBlock($rootScope, $state, $stateParams, $http, $cookieStore, localStorageService, ngNotify, editableOptions, editableThemes, AclService) {
    ngNotify.config({
        position: 'bottom',
        theme: 'pitchy',
        duration: 3000
    });

    //$rootScope.windowHeight = $(window).innerHeight();
    //console.log("windowHeight ="+$rootScope.windowHeight);

    //-------------Debut configuration xeditable
    editableThemes.bs3.inputClass = 'form-control form-control-sm';
    editableThemes.bs3.buttonsClass = 'btn btn-sm';
    editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
    editableThemes['bs3'].submitTpl = '<button class="btn btn-success" type="submit"><i class="fa fa-check"></i></button>';
    editableThemes['bs3'].cancelTpl = '<button class="btn btn-danger" type="reset"><i class="fa fa-close"></i></button>';
    //-------------Fin configuration xeditable

    // keep user logged in after page refresh
    $rootScope.globals = $cookieStore.get('globals-steylo') || {};

    $rootScope.globalsLogo = localStorageService.get('globals-steylo-logo') || {};  //Utilisation du module angular-localstorage pour stocker et recupérer le logo de l'entreprise (taille trop grande dans un cookie)

    if ($rootScope.globals.currentUser) {
        if ($rootScope.globalsLogo.currentLogo){
            console.log('cookies recupéré !');
            //console.log('currentUser', $rootScope.globals.currentUser);
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser; // jshint ignore:line
            //$http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentLogo; // jshint ignore:line

            // Attempt to load from web storage
            if (!AclService.resume()) {

                AclService.setAbilities($rootScope.globals.currentUser.chanelUserInfos.listFonctionnalites);
                AclService.attachRole('profil');
            }
        }
    }

    /*
    $rootScope.deleteTooltipIntrusive = function () {
        $('[data-toggle="tooltip"]').tooltip('hide');    //Supprime le tooltip qui persiste

    };*/

    $rootScope.annee = new Date();
    $rootScope.showCopyright = true;
    $rootScope.toggleSteyloText = function (s) {
        $rootScope.showCopyright = !s;
    };

    $rootScope.$on('$stateChangeSuccess',function(){
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    });
    $rootScope.$state = $state;
    //return $rootScope.$stateParams = $stateParams;
}