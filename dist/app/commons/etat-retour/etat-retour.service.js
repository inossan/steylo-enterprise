(function () {
    'use strict';

    angular.module('app.commons')
        .factory('EtatRetourService', EtatRetourService);

    EtatRetourService.$inject = ['$http'];
    function EtatRetourService($http) {
        const etatRetourService = {};
        etatRetourService.getEtatRetour = getEtatRetour;

        return etatRetourService;

        function getEtatRetour() {
            let request = {
                dataEtatRetour: {}
            };
            let URL = BASE_URL + '/etatRetour/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }
    }
})();