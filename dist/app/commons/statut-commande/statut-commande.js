(function () {
    'use strict';

    angular.module('app.commons')
        .factory('StatutCommandeService', StatutCommandeService);

    StatutCommandeService.$inject = ['$http'];
    function StatutCommandeService($http) {
        const statutCommandeService = {};
        statutCommandeService.getStatutCommande = getStatutCommande;

        return statutCommandeService;

        function getStatutCommande() {
            let request = {
                dataStatutCommand: {

                }
            };
            let URL = BASE_URL + '/statutCommand/getByCriteria';
            return $http.post(URL, request, {cache: true});
        }
    }
})();