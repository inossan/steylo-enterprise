(function () {
    'use strict';

    angular.module('app.commons')
        .factory('paysService', paysService);

    paysService.$inject = ['$http'];
    function paysService($http){
        let paysService = {};

        paysService.getPays = getPays;

        return paysService;
        
        function getPays() {
            let req = {
                dataCountry: {}
            };
            let URL = BASE_URL + '/country/getByCriteria';
            return $http.post(URL, req, {cache: true});
        }
    }
})();