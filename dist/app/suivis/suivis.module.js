(function () {
    'use strict';

    angular.module('app.suivis', [
        'ui.router',
        //*/
        'oc.lazyLoad',
        'ncy-angular-breadcrumb',
        'angular-loading-bar',
        //'ngMaterial',
        //'mdSteppers',

        //*/
    ])
        .config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', '$breadcrumbProvider', function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $breadcrumbProvider) {

            $ocLazyLoadProvider.config({
                // Set to true if you want to see what and when is dynamically loaded
                debug: true
            });

            $urlRouterProvider.otherwise('/login');
            $stateProvider
                .state('app.suivis', {
                    url: '/suivis',
                    templateUrl: 'app/suivis/views/suivis.html',
                    ncyBreadcrumb: {
                        label: 'Suivis'
                    },
                    controller: 'suivisCtrl',
                    resolve: {
                        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
                            // you can lazy load controllers
                            return $ocLazyLoad.load({
                                files: [
                                    'app/suivis/controllers/suivisCtrl.js',
                                    'app/suivis/controllers/commandesCtrl.js',
                                    'app/suivis/controllers/ventesCtrl.js',
                                    'app/suivis/controllers/details-commandeCtrl.js',
                                    'app/suivis/controllers/details-retourCtrl.js',
                                    'app/suivis/controllers/retoursCtrl.js',
                                    'app/suivis/services/commandes.service.js',
                                    'app/suivis/services/retours.service.js',
                                    'app/suivis/services/ventes.service.js',
                                    'app/commons/etat-retour/etat-retour.service.js'
                                ]
                            });
                        }]
                    },
                    redirectTo: 'app.suivis.commandes'
                })
                .state('app.suivis.commandes', {
                    url: '/commandes',
                    ncyBreadcrumb: {
                        label: 'Liste des commandes'
                    },
                    //abstract: true,
                    //templateUrl: '<ui-view></ui-view>',
                    views: {
                        'svi_sub_menu': {
                            templateUrl: 'app/suivis/views/commandes/commandes.html',
                            controller: 'commandesCtrl'
                        },
                    },
                })
                .state('app.suivis.details', {
                    url: '/J1RhaGEgQW1vdXNzb3UgUGhpbG9tw6huZSc=:id&:idChanel&J04nZ3Vlc3NhbiBJbm5vY2VudCc=',
                    ncyBreadcrumb: {
                        label: 'Détails de la commande'
                    },
                    views: {
                        'svi_sub_menu': {
                            templateUrl: 'app/suivis/views/commandes/details-commande.html',
                            controller: 'detailsCommandeCtrl',
                        },
                    },
                })
                .state('app.suivis.ventes', {
                    url: '/vente',
                    ncyBreadcrumb: {
                        label: 'Ventes'
                    },
                    views: {
                        'svi_sub_menu': {
                            templateUrl: 'app/suivis/views/ventes/vente.html',
                            controller: 'ventesCtrl'
                        },
                    },
                })
                .state('app.suivis.retour', {
                    url: '/retours',
                    ncyBreadcrumb: {
                        label: 'Retours'
                    },
                    views: {
                        'svi_sub_menu': {
                            templateUrl: 'app/suivis/views/retours/retour.html',
                            controller: 'retoursCtrl'
                        },
                    },
                })
                .state('app.suivis.retourDetails', {
                    url: '/J1RhaGEgQW1vdXNzb3UgUGhpbG9tw6huZSc=:id&:idChanel&J04nZ3Vlc3NhbiBJbm5vY2VudCc=',
                    ncyBreadcrumb: {
                        label: 'Détails du retour'
                    },
                    views: {
                        'svi_sub_menu': {
                            templateUrl: 'app/suivis/views/retours/details-retour.html',
                            controller: 'detailsRetourCtrl',
                        },
                    },
                })
        }])
})();