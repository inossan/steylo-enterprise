(function () {
    'use strict';

    angular.module('app.suivis')
        .factory('VenteService', VenteService);

    VenteService.$inject = ['$rootScope', '$http'];
    function VenteService($rootScope, $http) {
        const venteService = {};

        venteService.getVentes = getVentes;
        venteService.searchRetour = searchRetour;

        return venteService;

        function getVentes(index, size) {
            let request = {
                dataKitProduit: {
                    idChanel: $rootScope.globals.currentUser.chanelUserInfos.idChanel,
                },
                index: index || 0,
                size: size || 6
            };

            let URL = BASE_URL + '/kitProduit/getByCriteria';
            return $http.post(URL, request, { cache: true});
        }

        function searchRetour(criteria, index, size) {
            let request = {
                dataRetourCommande: criteria,
                index: index || 0,
                size: size || 6
            };
            let URL = BASE_URL + '/retourCommande/getByCriteria';
            return $http.post(URL, request, { cache: true });
        }
    }
})();