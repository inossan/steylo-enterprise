(function () {
    'use strict';

    angular.module('app.suivis')
        .controller('detailsCommandeCtrl', detailsCommandeCtrl);

    detailsCommandeCtrl.$inject = ['$rootScope', '$scope','$stateParams', '$state', 'CommandeService', 'StatutCommandeService', 'toastr', 'notificationService'];
    function detailsCommandeCtrl($rootScope, $scope,$stateParams, $state, CommandeService, StatutCommandeService, toastr, notificationService)  {

        $scope.showDetailsCommande = showDetailsCommande($stateParams.id);
        $scope.getTVA = getTVA;
        $scope.getTotalTVA = getTotalTVA;
        $scope.getStatutCommande = getStatutCommande();
        $scope.updateCommande = updateCommande;

        $scope.windowHeightP = $(window).innerHeight() - 260;

        //Configuration du tabs-----------------------------
        //*/
        $scope.tabs = {
            active: 0
        };
        $scope.pages = {
            active: 0
        };
        $scope.horizontalTabs = {
            active: 0
        };
        $scope.pages = [
            {
                id: 1,
                title: 'Détails de commande',
                badge: true //Pour afficher le nombre de demande  dans un badge uniquement pour cet onglet
            },
            {
                id: 2,
                title: 'Détails de paiement',
                content: ''
            },
            {
                id: 3,
                title: 'Détails de livraison',
                content: ''
            },
            {
                id: 4,
                title: 'Produits',
                content: ''
            },
            {
                id: 5,
                title: 'Historique',
                content: ''
            }
        ];
        //*/
        //Fin de la configuration du tabs-------------------
        console.log('$stateParams', $stateParams);

        function showDetailsCommande(idCommande) {

            $('[data-toggle="tooltip"]').tooltip('dispose');

            CommandeService.getDetailsCommandes(idCommande)
                .then(function (res) {
                    console.log('res getDetailCommandes', res);
                    if (!res.data.hasError) {
                        $scope.commande = res.data.itemsAbonneCommand[0];

                        _.forEach(res.data.itemsAbonneCommand, function (c) {
                            let tot = 0;
                            _.forEach(c.kitDto.kitProduitDtos, function (kp) {
                                tot += kp.quantite * kp.prix;
                                console.log('tot', tot);
                            });
                            c.total = tot;
                        });

                        console.log('commande', $scope.commande);
                    } else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err getDetailCommandes', err);
                });
        }

        function getTVA(total) {
            return _.ceil(total * 0.1);
        }

        function getTotalTVA(total) {
            return _.ceil(total + (total * 0.1));
        }

        function getStatutCommande() {
            StatutCommandeService.getStatutCommande()
                .then(function (res) {
                    console.log('res getStatutCommande', res);
                    if (!res.data.hasError) {
                        $scope.statutsCommande = res.data.itemsStatutCommand;
                        console.log('statutCommande', $scope.statutsCommande);
                    } else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err getStatutCommande', err);
                });
        }

        function updateCommande(commande) {

            let commandeInfo = {};

            console.log('updateCommande', commande);
            //Vérification des champs obligatoires
            if (!commande){
                commande = {};
                $scope.commande = {};
            }
            //console.log('produit', produit);
            if (!commande.hasOwnProperty('statutCommandeCode') || commande.statutCommandeCode === ''){
                toastr.error('Veuillez renseigner l\'état de la commande !', 'Erreur');
                return;
            }

            //Informations liées à l'historique
            commandeInfo.commentaire = commande.commentaire || '';
            commandeInfo.notifyCustomer = commande.notifyCustomer ? 1 : 0;

            console.log('commandeInfo', commandeInfo);

            CommandeService.updateCommande(commande.id, commande.statutCommandeCode, commandeInfo)
                .then(function (res) {
                    console.log('res updateCommande', res);
                    if (!res.data.hasError) {
                        notificationService.flatty.success('<b><i class="fa fa-check"></i>&nbsp;Votre commande a bien été modifiée !</b>');
                        $state.go("app.suivis.commandes")
                    } else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err updateCommande', err);
                });
        }
    }
})();