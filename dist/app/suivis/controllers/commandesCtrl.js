(function () {
    'use strict';

    angular.module('app.suivis')
        .controller('commandesCtrl', commandesCtrl);

    commandesCtrl.$inject = ['$rootScope', '$scope', '$state', 'CommandeService', 'toastr'];
    function commandesCtrl($rootScope, $scope, $state, CommandeService, toastr) {

        $scope.getCommandes = getCommandes;
        $scope.searchCommandes = searchCommandes;
        $scope.updateCommande = updateCommande;
        

        getCommandes();

        $scope.currentPage = 1;
        $scope.currentPageS = 1;
        $scope.sizeCommandes = 6;
        $scope.max_size = 3;

        $scope.searching = false;

        function getCommandes(statut, index, size) {
            CommandeService.getCommandes(statut, index, size)
                .then(function (res) {
                    console.log('res getCommandes', res);
                    if (!res.data.hasError) {
                        $scope.commandes = res.data.itemsAbonneCommand;

                        _.forEach($scope.commandes, function (c) {
                            let tot = 0;
                            _.forEach(c.kitDto.kitProduitDtos, function (kp) {
                                tot += kp.quantite * kp.prix;
                                console.log('tot', tot);
                            });
                            c.total = tot;
                        });

                        console.log('commandes', $scope.commandes);
                        $scope.totalCommandes = res.data.count;
                        console.log('totalCommande', res.data.count || 0);
                    } else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err getCommandes', err);
                });
        }

        function searchCommandes(criteria, index, size) {
            console.log('criteria', criteria);
            $scope.searching = true;

            criteria.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;
            
            CommandeService.searchCommandes(criteria, index, size) //revenir pour paginer dans le cas d'une recherche - OK
                .then(function (res) {
                    console.log('res searchCommandes', res);
                    if (!res.data.hasError) {
                        $scope.commandes = res.data.itemsAbonneCommand;

                        _.forEach($scope.commandes, function (c) {
                            let tot = 0;
                            _.forEach(c.kitDto.kitProduitDtos, function (kp) {
                                tot += kp.quantite * kp.prix;
                                console.log('tot', tot);
                            });
                            c.total = tot;
                        });

                        $scope.totalCommandes = res.data.count;
                        console.log('totalCommande', res.data.count);
                    } else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err searchCommandes', err);
                });
        }

        function updateCommande(id, statut) {

            $('[data-toggle="tooltip"]').tooltip('hide');
            
            CommandeService.updateCommande(id, statut)
                .then(function (res) {
                    console.log('res updateCommandes', res);
                    if (!res.data.hasError) {
                        getCommandes(statut_commande.EN_ATTENTE);
                    } else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err updateCommandes', err);
                });
        }

    }
})();