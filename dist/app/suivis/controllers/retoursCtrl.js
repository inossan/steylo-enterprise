(function () {
    'use strict';

    angular.module('app.suivis')
        .controller('retoursCtrl', retoursCtrl);

    retoursCtrl.$inject = ['$rootScope', '$scope', 'RetourService', 'EtatRetourService', 'toastr', 'notificationService'];
    function retoursCtrl($rootScope, $scope, RetourService, EtatRetourService, toastr, notificationService) {

        $scope.getRetoursCommande = getRetoursCommande;
        $scope.searchRetours = searchRetours;
        $scope.getEtatRetour = getEtatRetour;
        $scope.actionUpdateRetour = actionUpdateRetour;


        getRetoursCommande();
        getEtatRetour();

        $scope.currentPage = 1;
        $scope.currentPageS = 1;
        $scope.sizeRetours = 6;
        $scope.max_size = 3;

        $scope.searching = false;

        function getRetoursCommande(checkUpdate, index, size) {
            RetourService.getRetours(index, size)
                .then(function (res) {
                    console.log('res getRetoursCommande --> ', res);
                    if (!res.data.hasError) {
                        $scope.retours = res.data.itemsRetourCommande;

                        console.log('retours', $scope.retours);
                        $scope.totalRetours = res.data.count;

                        if (checkUpdate) {
                            notificationService.flatty.success('<b><i class="fa fa-check"></i>&nbsp;Votre modification a été prise en compte !</b>');
                        }

                    } else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err getRetoursCommande --> ' + err);
                });
        }

        function searchRetours(criteria, index, size) {
            console.log('criteria', criteria);
            $scope.searching = true;

            criteria.idChanel = $rootScope.globals.currentUser.chanelUserInfos.idChanel;

            RetourService.searchRetour(criteria, index, size)
                .then(function (res) {
                    console.log('res getRetoursCommande --> ', res);
                    if (!res.data.hasError) {
                        $scope.retours = res.data.itemsRetourCommande;
                        $scope.totalRetours = res.data.count;
                    } else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err searchRetours --> ', err);
                });
        }
        
        function getEtatRetour() {
            EtatRetourService.getEtatRetour()
                .then(function (res) {
                    console.log('res getEtatRetour --> ', res);
                    if (!res.data.hasError) {
                        $scope.etatRetours = res.data.itemsEtatRetour;
                        $scope.totalEtatRetours = res.data.count;

                    } else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err getEtatRetour --> ' + err);
                });
        }
        
        function actionUpdateRetour(idRetour, idEtatRetour) {

            let updateRetour = {
                id: idRetour,
                idEtatRetour: idEtatRetour
            };

            console.log('updateRetour --> ' + updateRetour);

            RetourService.updateRetour(updateRetour)
                .then(function (res) {
                    console.log('res actionUpdateRetour --> ', res);
                    if (!res.data.hasError) {

                        getRetoursCommande(true);

                    } else {
                        toastr.error(res.data.status.message, 'Erreur');
                    }
                }, function (err) {
                    console.log('err actionUpdateRetour --> ' + err);
                });
        }

    }
})();