angular.module("app").controller('WidgetDropzoneCtrl', ['$scope', 'commonUtilities', 'toastr',
function ($scope, commonUtilities, toastr) {

  let minSteps = 6,
  maxSteps = 60,
  timeBetweenSteps = 100,
  bytesPerStep = 100000,
  id = "#" + $scope.options.id;

  function checkFileName(str) {
    let bool = false;
    let fileName = null;
    let fileNames = str.split(".");
    if (commonUtilities.arrayContainItems(fileNames)) {
      if (fileNames.length !== 2) {
        fileNames.pop();
        fileName = angular.copy(fileNames.join("."));
      } else {
        fileName = angular.copy(fileNames[0]);
      }
    } else {
      fileName = angular.copy(str);
    }
    if (!commonUtilities.isNullOrEmptyValue(fileName)) {
      if ((/^[a-zA-Z0-9- _]*$/.test(fileName))) {
        bool = true;
      }
    }
    return bool;
  }

  //Dropzone Configuration
  Dropzone.autoDiscover = false;

  jQuery(document).ready(function () {
    setTimeout(function () {
      let dropzone = new Dropzone(id, {
        /*thumbnailWidth: 50,
          thumbnailHeight: 50,
          dictCancelUploadConfirmation:'Confirm cancel?',
          dictDefaultMessage:'Drop files here to upload',
          dictFallbackMessage:'Your browser does not support drag n drop file uploads',
          dictFallbackText:'Please use the fallback form below to upload your files like in the olden days',
          dictInvalidFileType:'File does not match the allowed file types',
          dictFileTooBig:'File is too big',*/
          dictCancelUpload:'Annuler',
        acceptedFiles: 'image/*',
        addRemoveLinks: true,
        parallelUploads: 2,
        maxFilesize: 1, //en MB
        maxFiles: 100,
        dictRemoveFile: "Retirer",
        accept: function (file, done) {
          let result = checkFileName(file.name);
          if (!result) {
            toastr.error('Nom de fichier invalide. Vérifier qu\'il n\'y a pas plus d\'un point dans le nom ' + file.name, 'Erreur');
            this.removeFile(file);
          }
          else {
            done();
          }
        },
        init: function () {
          this.on("maxfilesexceeded", function (file) {
              toastr.error('Vous ne pouvez pas joindre plus de ' + $scope.maxFiles + ' fichier(s)', 'Erreur');
            this.removeFile(file);
          });
          this.on("removedfile", function (file) {
            $scope.options.removeFile(file);
          });
          this.on("success", function (file, response) {
            let reader = new FileReader();
            reader.onload = handleReaderLoad;
            reader.readAsDataURL(file);
            function handleReaderLoad(evt) {
              $scope.options.addFile({ file: file, data: evt.target.result });
            }
          });
        }
      });

      dropzone.uploadFiles = function (files) {
        let self = this;

        for (let i = 0; i < files.length; i++) {

          let file = files[i],
          totalSteps = Math.round(Math.min(maxSteps, Math.max(minSteps, file.size / bytesPerStep)));

          for (let step = 0; step < totalSteps; step++) {
            let duration = timeBetweenSteps * (step + 1);
            setTimeout(function (file, totalSteps, step) {
              return function () {
                file.upload = {
                  progress: 100 * (step + 1) / totalSteps,
                  total: file.size,
                  bytesSent: (step + 1) * file.size / totalSteps
                };

                self.emit('uploadprogress', file, file.upload.progress, file.upload.bytesSent);
                if (file.upload.progress === 100) {
                  file.status = Dropzone.SUCCESS;
                  self.emit("success", file, 'success', null);
                  self.emit("complete", file);
                  self.processQueue();
                }
              };
            }(file, totalSteps, step), duration);
          }
        }
      }
    }, 700);
  });
}
]);
