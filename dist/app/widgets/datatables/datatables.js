angular.module("app").controller('WidgetDataTableCtrl', ['$scope', 'commonUtilities',
    function ($scope, commonUtilities) {

        $scope.isNull = function (value) {
            return commonUtilities.isNullOrEmptyValue(value);
        };

        $scope.item = {
          status : {}
        };

        $scope.status = [
        {
          name: "green",
          value: "BON"
        },
        {
          name: "yellow",
          value: "AVERTISSEMENT"
        },
        {
          name: "red",
          value: "MAUVAIS"
        }
      ];


        $scope.showObjectProperties = function (col, row) {
            let results = col.value.split('.');
            let variable = results[0];
            let attr = results[col.depth];
            return row[variable][attr];
        };

        $scope.pagination = {
            startIndex: $scope.options.index,
            lastIndex: $scope.options.size,
            sizePerPage: $scope.options.size,
            maxSize: 5,
            currentPage: 1,
            change: function () {
                this.startIndex = $scope.options.dataRows.length !== 0 ? ((this.currentPage * this.sizePerPage + 1) - this.sizePerPage) : 0;
                this.lastIndex = $scope.options.dataRows.length !== 0 ? (this.startIndex + (this.sizePerPage - 1)) : 0;
                $scope.options.index = angular.copy(this.startIndex);
                $scope.options.size = angular.copy(this.lastIndex);
            }
        };

        $(document).on('mouseenter', "td", function () {
            var $this = $(this);
            if (this.offsetWidth < this.scrollWidth && !$this.attr('title')) {
                $this.tooltip({
                    title: $this.text(),
                    placement: "bottom",
                    container: "body"
                });
                $this.tooltip('show');
            }
        });
    }
]);
